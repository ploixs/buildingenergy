---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)
# **Climate change**
stephane.ploix@grenoble-inp.fr

---

<br/><br/><br/>
# **Summary**

---

### General Circulation Models

Radiative forcing (+2.98W/m²) yields climate instability: it's mainly due to human activities. The doubling of CO2 in the next decade will lead to a radiative forcing (+3.71W/m2). There are gas like aerosol that yields negative forcing.

Water vapor is the most important GHG but it is not accounted because it has a short lifetime (days, weeks) and because it acts as a feed-back mechanism, it amplifies the radiative forcing caused by other GHG.

---

Climate models are like weather forecasting models but without observations. They rely on General Circulation Model. They take into account energy and water cycles (atmosphere, continental surface, ocean, sea ice, glaciers and polar ice caps) as well as the exchanges between these elements (heat exchange, evaporation, precipitation, run-off from rivers, glacier melt,...).

Today, models consider the atmosphere, oceans, soils and vegetation, sea ice, sulphate aerosols, and in some cases continental hydrology, the carbon cycle including the biosphere and marine ecosystems, and stratospheric ozone.

---

### Emission scenarios

The climate change depends on how socio-economic changes (economy, technology, lifestyle, public policies) evolve. The SRES scenarios are used as initial conditions to simulate recent climate change. These simulations cover the period 1860-2000.

The RCP scenarios correspond to the more or less efforts to reduce GHG emissions at global level. 4 a priori greenhouse gas emission scenarios have been established.
RCP8.5: increase of CO2 rejection
RCP6.0: stabilisation without exceed
RCP4.5: stabilisation without exceed
RCP2.6: peak then decline

---

Sociologists and economists are working on scenarios about socio-economic developments while the climatologists produce climate projections using these RCPs scenarios as input.

Compared with the RCPs, the new Shared Socioeconomic Pathway (SSP) scenarios provide economic and social reasons for socio-economic developments in relation to the different trajectories of greenhouse gas concentrations:
SSP1: forcing 2.6W/m2 (2100) sustainability
SSP2: forcing 4.5W/m2 (2100) middle of the road
SSP3: forcing 7W/m2 (2100) regional rivalry
SSP4: forcing ?W/m2 (2100) inequality
SSP5: forcing 8.5W/m2 (2100) fossil fuel development.

---

### Global warming level approach

Another approach consists considering a global warming level wrt 1850-1900: +1.5°C, +2°C and +4°C, no matter when it will be reached: the effect are similar.
- a global warming limited to +1.5°C: reached by the early 2030s, unless global emissions are drastically reduced immediately;
- a global warming of up to +2°C: if very large reductions in GHG emissions are made after 2030 and if global carbon neutrality is achieved in the second half of the 21st century:

---

- a global warming of up to +3°C: This level lies between the warming that would result from the commitments announced by governments (+2.8°C in 2100) and the warming that would result from the global policies in place at the end of 2020 (+3.2°C in 2100).

Using global climate projections, based on global circulation models (GCMs), the correspondence between the level of global warming and the associated impacts is immediate: individually for each global simulation, we determine the 20-year period during which the level of global warming relative to the pre-industrial period is reached.

---

### Regionalization

Impact studies generally require climate data simulated on a finer scale than the current resolution of large-scale climate simulation models (of the order of 50 km to 300km) or involve threshold phenomena, for which the biases of the models are prohibitive. It is therefore necessary to increase the resolution of the mesh. This is known as regionalization. Regionalization methods make it possible to go down to finer scales of the order of ten or so kilometers. The downscaling technique only makes sense if there is a link between the large scale and the small scale.

---

Applying the global warming level approach to regional climate projection involves establishing a correspondence between the regional projections and the global mean temperature. The reference warming trajectory for adaptation in France (TRACC) is doing the job in France. The relationship between warming in France and global warming is deduced from CMIP6 projections constrained by observations. Explore2-ADAMONT data are used to map high-resolution climate indicators over France for each global warming level set.

---

### Narrative approach

A narrative is defined as a physically coherent sequence of past or future events. This approach seeks to develop descriptive 'narratives' of possible future climates. Several narratives need to be considered in order to explore several possible futures.

The Explore2-Climate game is based on 17 EURO-CORDEX projections at 12 km resolution, which have been corrected for their biases using the ADAMONT and CDF-t methods. From these projections, four narratives were selected in co-construction with hydrologists, to illustrate possible climate futures in mainland France.

---

<br/><br/><br/>
# **Detailed explanations**
Daily simulations of climate change from 2006 to 2100 are available on the DRIAS website: https://www.drias-climat.fr

---

## How climate change is modeled?

A building __designed today__ should be __still performant in 2070__ and probably still __in 2100__. It supposes to be able to predict the ambient conditions in a mid/long-term taking into account __climate change and its uncertainties particularly due to how people way of life will evolve__. Let's investigate the modelling of the climate change.

In France, there is a web portal, named [DRIAS les futurs du climate](https://www.drias-climat.fr), which is publishing diverse simulation results about the possible climate evolution for the French territory.

---

__Climate models__ have many __similarities__ with __weather forecasting models__. Compared with forecasting models, an essential specificity of __climate models__ is that they __are not at all based on observations__. __The climate system evolves completely freely__. It __receives__ energy in the form of __solar radiation__ and __losses__ it in the form of __infrared radiation__ emitted into space. It corresponds to the so-called radiative forcing.

---

__Radiative forcing__ is used to quantify the __change in energy balance__ in Earth's atmosphere at the __top of the troposphere__ (between 10 and 16 km above sea level). Various factors contribute to this change, such as __concentrations of greenhouse gases and aerosols__, and changes in __surface albedo and solar irradiance__. In more technical terms, it is defined as "__the change__ in the net, __downward minus upward__, __radiative flux__ (expressed in W/m²) due to a change in an __external driver of climate change__.

---

The radiative forcing corresponds to the ratio of the 2 quantities pointed out by dashed circles:

![width:25cm](img/radiative_forcing.png)

---

The Intergovernmental Panel on Climate Change (__IPCC i.e. GIEC__) summarized the current scientific consensus about __radiative forcing__ changes: the global value is __2.98W/m²__ with a __human-caused part__, which is about __2.72W/m²__ in 2019 relative to 1750. It can be decomposed into:
- __greenhouse gases__ (GHGs): The __largest contributor__ to positive radiative forcing, with carbon dioxide (CO₂) alone accounting for about __2.17W/m²__ of the total increase​. Assuming no change in the emissions growth path, a __doubling of the CO₂__ concentrations within the next __several decades__ would correspond to a cumulative __radiative forcing change of +3.71 W/m²__ whereas the balance from steady temperature is __0 W/m²__.

---

- __aerosols and precursor gases__: These have a __net cooling effect__, estimated to reduce radiative forcing by about __-1.1 W/m²__ due to their __reflective properties and interaction with clouds__
- land use changes: These contribute to radiative forcing by __altering surface albedo__, though their overall effect is smaller compared to GHGs and aerosols​.
- other minor factors that include __black carbon on snow__, which has a positive forcing effect, and changes in solar irradiance, which have a relatively small impact​.

---

The figure below summarizes the main heat flows involved.

<img src="img/Energy_balance.jpg" width="50%"/>

Water (vapor) is the __most important GHG__ but it is not accounted because it has a __short lifetime__ (days, weeks) and because it acts as a __feed-back mechanism__, it __amplifies the radiative forcing caused by other GHG__, rather than initiating it.

---

The assessment of radiative forcing and climate sensitivity shows which physical variables are contributing to temperature changes:

<img src="img/Physical_Drivers_of_climate_change.svg.png" width="50%"/>

---

The simulated climate (wind, temperature, etc.) is the result of this adjustment between energy received and energy lost. The conservation of energy, and more generally __energy exchanges__, are therefore __fundamental__ to a __climate model__, and modelling them is the __primary concern of climatologists__.

In order to ensure this energy consistency, __climate models take into account__, with varying degrees of approximation, all the elements involved in the __energy and water cycles__ (atmosphere, continental surface, ocean, sea ice, glaciers and polar ice caps) as well as the exchanges between these elements (__heat exchange, evaporation, precipitation, run-off from rivers, glacier melt,...__).

---

__Climate variability differs from climate change__. Indeed, the __variability__ refers to changes in the climate in relation to a general trend for a __reference period__ (often three decades). Cyclical phenomena, whether __high-frequency__ (such as __seasonal droughts or floods__) or __low-frequency__ (such as the North Atlantic Oscillation of __El-Nino__), can only be considered as climate change if the change in their frequency extends over several decades. Climate variability can be due to intrinsic Earth processes, such as __volcanic eruptions__, to external influences such as __solar Milankovitch cycles__ (it's cyclical orbital movements of Earth cause variations of up to 25 percent in the amount of incoming insolation at Earth's mid-latitudes.), or to __anthropogenic forcing__, which can also be described as an __external process__ 

---

Today, __models consider__ the __atmosphere, oceans, soils and vegetation, sea ice, sulphate aerosols__, and in some cases __continental hydrology__, the __carbon cycle__ including the biosphere and marine ecosystems, and __stratospheric ozone__. 

To answer the questions raised by climate change and its impacts, the IPCC uses models of climate system and __economic and demographic studies__. 

---

Climate simulations are carried out using numerical models known as __General Circulation Model__ (GCM, Modèles Atmosphériques de Circulation Générale (MACG) in French). The __main difference__ compared with a numerical __weather forecasting model__ is the need to __force the model__ towards __monthly averages__, allowing the __annual seasonal cycle__ to be __taken into account__. 

__Atmospheric models__ are __coupled with models__ representing other parts of the Earth system: __the ocean, vegetation, rivers, marine biogeochemistry, atmospheric chemistry, polar ice caps and the carbon cycle__. 

---

### Emission scenarios

The implications of __climate change__ for the environment and society  __depends not only on__ the response of the Earth system to changes in __radiative forcing__, but also on how __socio-economic changes (economy, technology, lifestyle, public policies) evolve__.

 These scenarios __take into account changes in population, economy, industrial and agricultural development and, in fairly simplified terms, atmospheric chemistry and climate change__.

---

History of the IPCC scenarios:

- _SA90 scenarios, in the first assessment report published in 1990_
- _IS92 scenarios, in the 2nd and 3rd reports (1995 & 2002)_
- SRES scenarios (Special Report on Emissions Scenarios), by the fourth assessment report, presented in 2007
- RCP (Representative Concentration Pathways) scenarios, derived from the SRES scenarios, and used by Group I for the fifth assessment report (2013)
- The SSP (Shared Socioeconomic Pathways) scenarios used for the 5th and 6th assessment reports, published in 2007 and 2013 respectively.

---

#### The SRES scenarios

The reference scenarios correspond to __observed greenhouse gas and aerosol concentrations__. They are used as __initial conditions__ for numerical models, in order to simulate recent climate change. 
These simulations cover the __period 1860-2000__.

The simulations from 1860 to 2000 are __carried out taking into account only the forcing due to human activities__: the increase in __greenhouse gases and sulphate aerosols__. 

---

#### The RCP scenarios

For the __5th IPCC report, 4 RCP scenarios of greenhouse gases (GHG), ozone and aerosol precursors for the 21st century and  __correspond to greater or lesser efforts to reduce GHG emissions__ at global level. 

From these, __climatologists deduce the climatic conditions__ and associated impacts of climate change while __sociologists and economists__ are working on scenarios about __socio-economic developments__.

---

__Since the 2000s__, socio-economic determinants and knowledge of the climate system have evolved considerably. For example, __demographic projections have been revised downwards__, the __development of emerging countries__ has been __underestimated__, and __climate policies have been adopted on a global scale over the last fifteen years__. It was therefore necessary to update the tools used by the IPCC. In addition, unlike the SRES scenarios, the __new scenarios__ will not be created by the IPCC itself, but will be __provided by the scientific community__ for the IPCC's use. 

---

In addition to designing new scenarios, the scientific community has made a real methodological shift. Whereas the previous generation of scenarios was based on a sequential approach (cf. SRES scenarios), the new method now applies __a parallel approach__. Scientists have defined __4 a priori greenhouse gas emission scenarios__, known as RCPs (Representative Concentration Pathways). Based on these reference scenarios, the teams work simultaneously: the __climatologists produce climate projections__ using the RCPs as input, while the __socio-economists develop emission scenarios__ that they compare with the RCP scenarios.

---

![bg fit](img/Moss_RCP.png)

---

The RCP scenarios are 4 reference scenarios for the evolution of radiative forcing (see table below) over the period 2006-2300. 

![width:31cm](img/radiative_scenarios.png)

 The only scenario with no equivalent is RCP 2.6, which incorporates the effects of an emissions reduction policy likely to limit global warming to 2°C in 2100.

---

![bg fit](img/rcp.png)

---

#### SSP scenarios

__5 families of SSP scenarios__ have been defined: it enables __climatologists and economists__ to __work in parallel__.

Compared with the RCPs, the new __SSP scenarios__ illustrate __different socio-economic developments__ in relation to the different trajectories of __greenhouse gas concentrations__ in the atmosphere.


---

![height:13cm](img/scenariomip-ssps-within-the-scenario-matrix.jfif.jpg)

Unlike the RCP scenarios, the new SSP-based scenarios provide economic and social reasons for the assumed emissions trajectories. 


---

- SSP1: The __sustainable and "green" pathway describes an increasingly sustainable world__. The __global commons are preserved, and the limits of nature are respected__. The focus is on __human well-being rather than economic growth__.
  __Income inequalities between and within states are reduced__. Consumption is geared towards __minimizing the use of material resources and energy__.

  SSP1 2.6: This scenario with 2.6 W/m² by 2100 is a remake of the RCP2.6 low-emissions scenario and has been designed to simulate development __compatible with the 2°C objective__. This scenario includes the implementation of __strong measures to reduce GHG emissions__.

---

- SSP2: The "median" or __middle path extrapolates past and present global development into the future__. 
  __Income__ trends in different __countries diverge considerably__.
  There is __a little amount of cooperation between countries__. __Global population growth is moderate and stabilizing in the second half of the century__. 
  __Environmental systems are facing a degree of degradation.__

  SSP2 4.5: As an update of the RCP4.5 scenario, with an additional radiative forcing of 4.5 W/m² by 2100, represents the __average trajectory of future greenhouse gas emissions__. This scenario assumes that __climate protection measures are taken__.
---

- SSP3: __Regional rivalries__. 
  A __resurgence of nationalism and regional conflicts is pushing global issues into the background__.
   __Politicians are increasingly focusing on national and regional security issues__. 
   __Investment in education and technological development declines.__
   __Inequalities are increasing__. 
   __Some regions suffer considerable environmental damage__.

  SSP3 7.0: At 7W/m² by 2100, this scenario is in the upper middle of the full range of scenarios. It has been newly introduced after the RCP scenarios, bridging the gap between RCP6.0 and RCP8.5.
---

- SSP4: __Inequality__. 
  The __gap is widening between developed societies that cooperate globally and those that stagnate at a lower stage of development, with low incomes and low levels of education__. 
  __Environmental policies succeed in solving local problems in some regions, but not in others__.

---

- SSP5: __Development based on fossil fuels__. 
  Global markets are becoming increasingly integrated, leading to innovation and technological progress. However, __social and economic development is based on intensified exploitation of fossil fuel resources, with a high percentage of coal and energy-intensive lifestyles worldwide__. The global economy is growing rapidly and local environmental problems, such as air pollution, are being tackled successfully.

  SSP5 8.5: With an additional radiative forcing of 8.5W/m² by 2100, this scenario represents the upper limit of the range of scenarios. It can be understood as an update of the CMIP5 RCP8.5 scenario, now combined with socio-economic trajectories. 

---

![bg fit](img/radiative-forcing.jfif.jpg)

---

### Levels of global warming

The latest IPCC report (AR6; IPCC 2021) puts forward an approach aiming at __documenting the global climate for different levels of warming__. The aim is to describe the climate in a world at a given level of warming, rather than at a given timeframe and for a given emissions scenario. So, for example, we now talk about __the climate for a global warming level of +2°C__ compared with the pre-industrial period (1850-1900). 

---

This figure represents the change in the mean annual temperature (°C) wrt 1850-1900 for global warming levels +1.5°C, +2°C and +4°C.

![width:30cm](img/Figure5b_AR6_TRACC-GWL.JPG)
This approach is based on the assumption that, __at a given level of global warming, climate change, no matter when this level is reached__. 

---

### DRIAS model of the climate change

The reference warming trajectory for adaptation in __France (TRACC)__ defined in spring 2023 is based on a global warming level approach. 

In the light of the information provided by the IPCC, __3 levels of global warming__ have been selected:

---

- Global warming limited to __+1.5°C__: __reached by the early 2030s, unless global emissions are drastically reduced immediately__;
- Global warming of up to __+2°C__: if __very large reductions in GHG emissions are made after 2030__ and if __global carbon neutrality is achieved in the second half of the 21st century__;
- Global warming of up to __+3°C__: This level lies between the warming that would __result from the commitments announced by governments (+2.8°C in 2100)__ and the warming that would result __from the global policies in place at the end of 2020 (+3.2°C in 2100)__.

---

__The Explore2-ADAMONT set (RCP8.5)__

The data used correspond to the __17 projections__ in the Explore2-ADAMONT set (TRACC model, distributed on the DRIAS portal) and resulting from the __national Explore2 project__. They were chosen to meet the following three objectives:

---

- providing __high spatial resolution__, enabling detailed diagnoses to be made of __local meteorological phenomena__ and their future evolution;
- providing the best possible coverage of the uncertainties inherent in any projection;
- being __consistent with the analysis of hydrological impacts (surface water, groundwater)__ produced as part of the Explore2 project and with the expected changes over France described by broader ensembles such as CMIP6.

---

To ensure that the ensemble is made up of the same simulations whatever the targeted level of warming, __only__ the projections for the high emissions scenario (__RCP8.5__) are used in TRACC. This scenario __was chosen __ because it is the one for which __the largest number of simulations are available__, and __the only one that allows high levels of global warming to be dealt__ with (+3°C in particular).

---

__CMIP6 projections constrained by observations__

For the first time, in AR6, the IPCC decided to base its estimates on __statistical combinations of models and observations__ (a method known as "observational constraints").

The results obtained on mean global warming indicate a significant __reduction in uncertainty__.

---

### Method

Using global climate projections, based on global circulation models (GCMs), __the correspondence between the level of global warming and the associated impacts is immediate__: individually for each global simulation, we determine __the 20-year period during which the level of global warming relative to the pre-industrial period is reached__.  __The final results are usually presented in probabilistic form (multi-model medians, distribution products__).

Applying the global warming level approach to __regional climate projection__ involves establishing __a correspondence between the regional projections and the global mean temperature__.

---

Regarding TRACC, we need to:
a) __determine the warming in France corresponding to the global warming level set__,
b) __identify the year in which this warming in France is reached__,
c) __describe the climate in France over the 20-year period centred on that year__.

In this approach, the relationship between warming in France and global warming is deduced from CMIP6 projections constrained by observations. __Explore2-ADAMONT data are used to map high-resolution climate indicators over France for each global warming level set__.
 
---

Step a) This step is based on CMIP6 projections constrained by observations:
- the global mean temperature series, consistent with the IPCC estimate (IPCC 2021, Section 4.3.4), which covers the period 1850-2100.

---

- the mean temperature series for France, which covers the period 1850-2100.
  
They indicate that __global warming of +1.5°C, +2°C and +3°C__ relative to the pre-industrial period (1850-1900) corresponds to __warming in France of +2°C, +2.7°C and +4°C__ relative to the pre-industrial period (1900-1930, because of the availability of the observation data used to constrain the projections for France, the pre-industrial period for warming in France corresponds to 1900-1930.

---

In order to match the Explore2-ADAMONT data, as these __data are not available for the first half of the 20th century__, the __warming in France corresponding to each level of global warming is also expressed in relation to the 1976-2005__ reference period. We thus find that global warming of __+1.5°C, +2°C and +3°C__ relative to the pre-industrial period (1850-1900) corresponds to warming in __mainland France of +1.4°C, 2.1°C and 3.4°C__ relative to 1976-2005.

---

In the next table, in the first row appears the global warming levels compared with the pre-industrial period. Rows 2 and 3 correspond to the warming levels in mainland France compared with the pre-industrial period and compared with 1976-2005.

<img src=img/levels.png width=80%/>

---

The next two steps are based on the Explore2-ADAMONT projections.

Step b) For each Explore2-ADAMONT simulation, we produce a series of anomalies relative to 1976-2005 in the smoothed mean annual temperature for France. The year in which this series reaches the warming in France relative to 1976-2005 (e.g. +3.4°C) corresponding to the targeted global warming level (+3°C) is determined.

---

Step c) In each Explore2-ADAMONT simulation, the impacts on France over the 20-year period around the year determined in step (b) are calculated .

Thus, for each of the 17 Explore2-ADAMONT simulations used, we have maps for various climate indicators (temperature, precipitation, number of hot days, etc.) describing the climate of a France in a world that will have warmed up, for example by 3°C compared with the pre-industrial period.

These results are also presented in terms of changes (deviations or relative deviations) compared with the period 1976-2005, the reference period used on the Drias - Climate Futures portal.

---

To summarize the results, __multi-model distribution products are produced__: for each indicator, 3 maps are produced: __"median", "lower bound" and "upper bound"__, showing respectively __at each grid point the median__, the lowest value and the highest value of __all 17 Explore2-ADAMONT projections__. The next table represents the year at which the 1.5°C, 2°C and 3°C global warming levels are reached in each of the Explore2-ADAMONT-RCP8.5 projections aver a 20-year period over which the indicators are calculated.

---

![bg fit](img/aladin.png)

---


### Regionalization

Impact studies generally require climate data simulated on a __finer scale than__ the current resolution of large-scale climate simulation models (of the order of __50 km to 300km__) or involve threshold phenomena, for which the biases of the models are prohibitive. It is therefore necessary to __increase the resolution of the mesh__. This is known as regionalization. Regionalization methods make it possible to go down to __finer scales of the order of ten or so kilometers__.

---

![bg fit](img/Descente_Echelle_Schema.png)

---

For some impact studies, climate change is requested for specific points corresponding to the locations of measurement stations. However, the grid points in a model represent average values over the mesh, which means that there are sometimes significant differences with the point values measured. __Downscaling is therefore necessary, particularly for studying extremes of precipitation and for regions with complex topography__.

---
 
The downscaling technique __only makes sense if there is a link between the large scale and the small scale__. The downscaling technique must:
- __reflect sub-mesh processes__
- __take physical processes into account__
- __provide more than simple linear interpolation__.

---

Downscaling methods lead to an artificial increase in resolution. An uncertainty calculation is required to determine the confidence that can be placed in the results, and this calculation can be made over the "present climate" period. Impact studies in particular need to be based on several simulations (scenarios/models/descaling methods) in order to assess uncertainties.

---

There are two approaches to downscaling methods:
- the dynamic approach, which involves explicitly solving the physics and dynamics of the regional climate system;
- the statistical approach, which is based on the search for a statistical relationship between local variables and model predictors.

---

These two approaches can be used independently or in combination (statistical-dynamic methods). The dynamic approach is often more costly in terms of computing time. The statistical approach, on the other hand, requires finding a method that is suited to the type of impact we want to study (parameters, domain, etc.) and having access to historical data over a sufficiently long period (10-20 years). Using two downscaling methods for the same problem is a way of quantifying some of the uncertainty.

---


### Correction methods

Generally speaking, __regionalized projections cannot be used directly for impact studies on a local scale, because of two main problems: they are biased in relation to observations and their spatial scale is too coarse for certain applications__. Indeed, if we compare the average of climate simulations over a given period, in a given region and for a given parameter, with that of observations, we generally find __fairly good matching__. However, the matching is not perfect.

---

 Not only are there __systematic errors in the averages__, but __some extremes are also poorly reproduced__. A model can overestimate low temperatures and very slightly underestimate the highest temperatures. In order to close this gap between the information needed by the impact experts and the data available from the regional climate models, __bias correction and statistical downscaling methods are used to correct the model variables and make the statistical distribution of the daily data identical to the distribution observed at each point__. 

---

There are __different kinds of statistical downscaling and bias correction techniques__, with different characteristics to meet different end-user needs. One method that is neither necessary nor sufficient to establish the confidence in the potential of models to respond adequately to anthropogenic forcing is to __consider the degree of accuracy with which models reproduce measurable present or past climate and its variability.__

---

The most exhaustive way of analyzing behavior at a station is to __compare the probabilistic density functions (PDFs) for each season and variable__. The quantile-quantile diagram (figure on the right) is both an analysis tool and a correction method. This diagram represents the minimum temperatures in Paris in winter (°C). The quantiles of the model are on the x-axis and those of the observation are on the y-axis.

![bg right:20% width:100%](img/diagrammeQQ.png)

---

 Other correction methods exist like:
- The DSClim method is a combination of the time type approach and the analogues method. It uses only the large-scale characteristics of the models and is therefore applicable to a wide range of products, including IPCC simulations.
- The CDF-t method is a method to generate the distribution functions of a local climate variable in the future climate from the distribution functions of this same variable observed or pseudo-observed in the reference climate.

---

- The __ADAMONT__ method is a consolidation of the so-called 'quantile-quantile' statistical adjustment method developed by Michel Déqué in the early 2000s. It __uses the quantile-quantile approach and takes into account time types for bias adjustment__.

---

__Most statistical downscaling and correction methods are based on the assumption that the statistical relationship established when the model was built (historical period) will be verified when it is applied (future scenarios)__, i.e. the stationarity assumption. For the same observed series and the same model, the results may vary. Bias correction is therefore another source of uncertainty to be taken into account, in addition to those arising from the observed data, the models, the scenario and the impact model.

There are many sources of uncertainty in any modelling exercise, and it is essential always to bear in mind the limitations inherent in each modelling exercise, especially when it comes to future climate projections.

---


### Narrative approach

A narrative is defined as a physically coherent sequence of past or future events. This approach __seeks to develop descriptive 'narratives'__ of possible future climates. Several narratives need to be considered in order to explore several possible futures. In addition, it is important to describe how they have been selected and how they fit within the distribution of the set of models.

---

__Explore2 narratives__

The Explore2-Climate game is based on 17 EURO-CORDEX projections at 12 km resolution, which have been corrected for their biases using the ADAMONT and CDF-t methods. From these projections, four __narratives were selected in co-construction with hydrologists__, to illustrate possible climate futures in mainland France. The selection criterion relates to average changes in future temperature and precipitation: the four narratives must correspond to contrasting changes, to reflect the dispersion of the Explore2-Climat ensemble while remaining consistent with the confidence interval of the CMIP6 projections. They are intended to be used as inputs for hydro-climatic projections that will illustrate possible water futures in mainland France.

---

The selected narratives, identified by colours, are:
- __the orange narrative (EC-EARTH rcp85 HadREM3-GA7 ADAMONT): strong warming and strong drying in summer (and annually)__
- __yellow narrative (CNRM-CM5 rcp85 ALADIN63 ADAMONT): relatively little change in the future__
- __the purple narrative (HadGEM2-ES rcp85 CCLM4-8-17 ADAMONT): strong warming and strong seasonal contrasts in precipitation__
- __the green narrative (HadGEM2-ES rcp85 ALADIN63 ADAMONT): marked warming and increased precipitation.__

---

The figure below illustrates the changes in temperature and precipitation over France according to the 17 Explore2-2022 simulations with identification of the four narratives over the summer and winter seasons, compared with the 5\% and 95\% quantiles (Q5 and Q95) of the CMIP6 and CMIP6 ensemble constrained by observations for temperatures.

---

![bg fit](img/naratif.png)

---

__Future changes according to the 4 Explore2 narratives__

In terms of average temperature, the orange, purple and green narratives are very close in terms of annual warming: +4.6°C to +5°C at the scale of France between 1976-2005 and the end of the century, while the yellow narrative is limited to +3.7°C. This difference is repeated on a seasonal basis. For annual precipitation, yellow and green are close and project wetter end-of-century conditions for France as a whole. 

---

Orange and purple project drier end-of-centuries. The ranking of narratives is significantly different at seasonal time steps: in winter, the wettest narratives are purple and green. In summer, orange and violet are the driest. Whatever the narrative, evapotranspiration is increasing in response to warming. Orange stands out for the strongest change. Changes in the water balance result from the cumulative effects of changes in precipitation and evaporation/ warming. Whatever the narrative, this balance is decreasing (drying up). For orange and violet, the two effects are in the same direction, with orange being the more drying. For green and yellow, the drying is slightly mitigated by the increase in precipitation in winter.

---

Narratives can also be used to study inter-annual variability. Next figure, taken from the Climate 2024 report of the Explore2 project, shows the annual mean temperature and cumulative precipitation balances for the mid-century horizon, compared with Safran observations (in grey) over the period 1976 to 2022. The four narratives according to their colours for the mid-century horizon (2041-2070) are compared with the period 1976-2005. The year 2022 corresponds to the anomaly point +2.1°C in temperature and -25\% in precipitation.

---

![bg fit](img/naratif2.png)

---

The four narratives constructed to present contrasting futures consistent with the CMIP6 confidence interval show differences in terms of future climate trends in temperature and precipitation on an annual or seasonal scale. Despite their differences, they all show a marked warming, particularly in summer, leading to an increase in potential evapotranspiration and a decrease in the "precipitation - potential evapotranspiration" (P - PET) balance. It is important to specify that the choice of narratives is always subjective and that their selection must be adapted to the needs of the user.

---


### Selecting a model

One of the major difficulties in carrying out an impact assessment is selecting the climate projections that will be used to determine the adaptation measures. These climate projections correspond to different emission scenarios and different models (GCM/RCM pairs), which need to be chosen according to the objectives of the impact study.

---

__Choice of individual simulations__

The DRIAS portal offers a selection of climate simulations, chosen to optimize the dispersion of the climate signal, as well as a wide range of climate indices. For the DRIAS-2020 ensemble, no fewer than __30 simulations are available, including 8 models for RCP2.6, 10 models for RCP4.5 and 12 models for RCP8.5__.

---

For impact studies, particularly those requiring the use of an impact model (hydrological, nivological, urban, etc.), it is not always possible to process such a large amount of data, which requires significant computing resources. It is often preferable to identify a few judiciously chosen simulations that will enable the objectives of the study to be met: these could be extreme scenarios (drought, extreme rainfall, sharp rise or fall in temperature) in the case of a sizing problem.

---

The tools/graphs presented above will enable users to identify the simulations best suited to their study.

Choosing a probabilistic representation: the main sources of uncertainty associated with the climate projections for the far horizons of the century are those associated with climate models and emission scenarios. For this reason, we use a set of climate projections for each RCP scenario
each available RCP scenario, which is based on a selection of models. This multi-model approach makes it possible to represent the dispersion of the climate signals modelled and to obtain a better estimate of the future climate given the information available.

---

The probabilistic representation is used to describe the range of variation of the modelled values for a set of climate projections. In the following, we will use the most commonly used parameters of the distribution - the IPCC AR5 in particular - the median, which represents the vast majority of the data (50\% higher values and 50\% lower values), and the 5th and 95th percentiles, which represent some of the extremes of the ensemble, corresponding respectively to the thresholds below which 5\% and 95\% of the values lie. 

---

In the next figure, uncertainty representation shows individual simulation values (dots), the multi-model median value (bold line) and the estimated 90\% percentile range (coloured bar).

<img src=img/Boxplot_Infographie_SelectionModels.png width=17%/>

---

To facilitate the selection of climate models for the DRIAS-2020 ensemble, we analyzed two climate indicators representative of model behavior:

  ΔT: deviation of mean temperature (over 30 years) from a reference period (1976-2005)
  ΔP: relative deviation of precipitation (over 30 years) compared with a reference period (1976-2005).

We calculated these indicators for each model individually, at annual and seasonal frequency, and for the 3 periods covered in DRIAS: 2021-2050, 2041-2070, 2071-2100.

---

Secondly, in order to assess the uncertainty associated with each of these indicators, we applied the method described above to calculate the distribution parameters (the 5th, 50th and 95th percentiles) from all the models.

---

For each season, we present below a classification of the models by variable (temperature and precipitation) in the form of a table and a 2D view (delta T/ delta P) enabling us to characterize the models for both temperature and precipitation.

The 2D diagram (ΔT/ΔP) shows the dispersion of the individual simulations of the DRIAS-2020 ensemble according to the forecast changes in precipitation (abscissa) and temperature (ordinate). And this for a given season at the end of the century according to the RCP8.5 scenario. The symbol used is representative of the global climate model used as forcing, and the color is a function of the regional climate model used. The 5th and 95th percentiles of the ensemble, as well as the median, are represented by dotted lines. 

---

The next figure illustrates the dispersion of the individual simulations of the DRIAS-2020 ensemble according to the forecast changes in precipitation (x-axis) and temperature (y-axis) for the summer seasons at the end of the century according to the RCP8.5 scenario. 

---

![bg fit](img/select1.png)

---

__Ranking models by variable__

The next figure shows a ranking of temperature differences (left-hand table) and precipitation differences (right-hand table) for the end-of-century summer seasons (2071-2100) compared with the historical period 1976-2005.

---

![bg fit](img/ranking.png)

---

For the end-of-century summer season, the two regional climate models (CCLM4-8-17 and RegCM4-6) forced by the HadGEM2 model are those modelling the warmest climate, with up to +6.4°C compared with the 1976-2005 baseline. The models with the RCM RCA4 and the HadGEM2 / CCLM4-8-17 model pair predict the greatest drying of the climate, beyond -40%.
The IPSL-CM5A / WRF381P model pair forecasts the smallest increase in temperature (+2.1°C) and models the largest increase in precipitation (+2\%) in the summer period at the end of the century, according to the RCP8.5 emission scenario.

---

![bg fit](figs/cops.png)

---

![bg fit](figs/tipping.png)

---

<br/><br/><br/>
# Open and solve the questions of the workshop02_climate.ipynb
