---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **Dynamic behavior & inertia**
stephane.ploix@grenoble-inp.fr

---

## Intuitive approach of thermal dynamics

Let's consider an empty wooden house under a controlled outdoor temperature and a controlled solar irradiance: for the sake of clarity, each day, the weather is the same.

The house is 120m$^2$ well insulated with a 30cm external insulation in glass foam, and the windows are directed to the South with a surface of 18m$^2$.

---

![bg fit](figs/sweedish_house.jpg)

---

The state model that has been identified is given by:

$$
\frac{d}{dt}
\begin{bmatrix}
T_{i}(t) \\ T_{w}(t)
\end{bmatrix}
 =
\begin{bmatrix}
\frac{-1}{r_i C_m} & \frac{1}{r_i C_m}\\
\frac{-1}{r_i C_i} & \frac{-1}{r_m C_i}+\frac{-1}{r_i C_i}
\end{bmatrix}
\begin{bmatrix}
T_{i}(t)\\T_{w}(t)
\end{bmatrix}
\begin{bmatrix}
0 & 0 & 0 \\ 
\frac{1}{r_mC_i} & \frac{1}{C_i} & \frac{A_w}{r_mC_i}
\end{bmatrix}
\begin{bmatrix}
T_{o}(t)\\\Phi_r(t)\\\Phi_s(t)
\end{bmatrix}
$$
- $T_w$, the wall temperature (in °C)
- $T_i$, the internal air temperature (in °C)
- $T_o$, the outdoor temperature (in °C)
- $\Phi_r$, the solar irradiance (in W/m$^2$)
- $\Phi_s$, the solar irradiance absorbed by the wall (in W/m$^2$)
$C_i=11.837kWh/°C$, $C_m=3.987kWh/°C$, $r_i=0.4788°C/kW$, $r_m=29.38°C/kW$ and $A_w=2.845m^2$

---

Here is the day used for the simulation:

![](figs/typical_day.png)

---

Here is a 30 days reference simulation:

![](figs/reference.png)

Note that the average indoor temperature is higher that the outdoor temperature despite the absence of heating system. It's due to solar gain. The indoor temperature variation is about 1°C. The steady state is reached after 300 hours.

---

The insulation has been divided by 2:

![](figs/insulation2.png)

The indoor temperature variation is still about 1°C but the average indoor temperature is lower than with the reference simulation because less insulation increases the heat loss with outdoor. The steady state is reached after 200 hours. Inertia is less but variations of the indoor temperature are quite similar to the reference simulation.

---

The capacity has been multiplied by 10 (60 days instead of 30):
![](figs/capacity10.png)

It's now a very heavy house. Steady state is reached after 1400 hours. The indoor temperature variation is null: it is behaving like a cave. No HVAC system is required to keep the indoor temperature constant. Living permanently in this house is interesting but for temporary occupations, it's not recommended at all.

---

## Inertia in ThBAT (French standard to check a building performance)

- inertia is intervening in the [Th-BCE](doc/ThBAT/RT2012_Th-BCE.pdf) performances, in the CEP and TIC.
- there are different kinds of inertia [Th-I](doc/ThBAT/RT2012_Th-I.pdf):
  - hourly inertia (in the Th-C), to compute HVAC intermittence
  - daily inertia (in Th-C and Th-E), absorption of the daily wave
  - sequential inertia (typ. 12 days in Th-E), absorption of the daily wave for summer
3 approaches are proposed:
  - a simplified approach
  - an approach based on points
  - an approach based on calculations (ISO13786)

---

## Diffusivity, thickness and inertia

Let's return to the heat equation applied to a wall (1D):
$$
\frac{d^2 T(X, t)}{dx^2}  = \frac{\rho c_p}{\lambda} \frac{\partial T(X,t)}{\partial t} 
$$

with $\rho$, the  density (kg/m$^3$), $\lambda$ the conductivity (W/m.K), $c_p$ the specific heat capacity (J/kg.K), T(X,t) the temperature (in K or °C) at position $X$ and time $t$ and $\varphi(X,t)$ the heat power. 

---

$\alpha=\frac{\lambda}{\rho c_p}$ is the __thermal diffusivity__: the lower, the slower the heat will diffuse and therefore the heat will move slowly in the material and impact room temperature with delay.  

The __volumetric heat storage capacity__ is given by: $\rho c_p$. 

The __inertia__ is due to materials having:
- __high values of heat storage capacity__ 
- __low values of thermal diffusivity__.

Let's recall a couple of values for diffusivity

---

<style scoped>
table {
  font-size: 20px;
}
</style>

|material|conductivity (W/m.K)|	specific heat capacity (J/kg.K)|	density (kg/m3)|	emmisivity coefficient|	thermal diffusivity (mm$^2$/s)	|volumetric heat capacity (J/m3/K)|
|--------|--------------------|---------------------------------|----------------|-----------------------------------------|---------------------------------|---|
|*immobile CO2*	|0.01	|1,005	|1.56	|0	|8.92	|1570|
|*immobile air*	|0.02	|1,005	|1.26	|0	|19.03	|1261|
|polyurethane foam	|0.03	|1,000	|30	|0.82	|0.97	|30000|
|dry glass wool	|0.05	|840	|25	|0.97	|2.38	|21000|
|__asbestos (amiante)__	|0.15	|816	|2,400	|0.93	|__0.08__	|__1958400__|
|lightweight wood	|0.04	|1,300	|400	|0.95	|0.08	|520000|
|__heavyweight wood__	|0.12	|2,400	|890	|0.95	|__0.06__	|__2136000__|
|__plaster__	|0.35	|1,090	|2,300	|0.98	|__0.14__|__2507000__|
|__water__	|0.60	|4,180|	997|	0.95|	__0.14__	|__4167460__|
|__glass__	|1	|753	|2,600	|0.94	|__0.51__	|__1957800__|
|__full brick__	|1	|840	|2,000	|0.93	|__0.60__	|__1680000__|
|__concrete__	|0.5	|880	|2,000	|0.85|	__0.28__	|__1760000__|

---

<style scoped>
table {
  font-size: 20px;
}
</style>

|material|conductivity (W/m.K)|	specific heat capacity (J/kg.K)|	density (kg/m3)|	emmisivity coefficient |	thermal diffusivity (mm$^2$/s)	|volumetric heat capacity (J/m3/K)|
|--------|--------------------|---------------------------------|----------------|-----------------------------------------|---------------------------------|---|
|graphite	|5	|780	|2,500|	0.96	|2.56	|1950000|
|stainless steel	|15	|440	|7,820|	0.07|	4.36|	3440800|
|lead	|35	|129|	11,350	|0.43	|23.90	|1464150|
|iron	|57	|449|	7,200|	0.44	|17.63	|3232800|
|aluminium	|203	|897	|2,700	|0.07	|83.82	|2421900|
|copper	|380	|385|	8,790|	0.03	|112.29|	3384150|
|silver	|410	|235|	10,500	|0.03	|166.16|	2467500|
|__wood__	|0.20	|1,300|	700|	0.95	|__0.22__	|__910000__|
|__PVC__	|0.25	|1,046.00	|1,395.00|	0.92	|__0.17__	|__1459170__|

For more materials, see ```/buildingenergy/data/propertiesDB.xslx```

---

The depth of penetration of a temperature signal into a solid, such as a concrete wall, can be analyzed using principles of heat conduction. When the temperature signal is sinusoidal, it is governed by Fourier's law and can be described using the concept of thermal diffusivity.

For a temperature signal varying sinusoidally with time:
$T(x,t)=T_0 e^{-x/\delta} \sin\left(2\pi \frac{t}{\tau}\right)$

The temperature wave will penetrate the material to a certain depth, with its amplitude decreasing exponentially. The penetration depth $d_p$ is given by: $d_p = \sqrt{\frac{\alpha \tau}{\pi}}$.

---
- __Shallow Penetration:__ Layers with smaller penetration depths (e.g., due to low diffusivity) contribute most to thermal inertia because they absorb and store heat near the surface.

- __Deep Penetration:__ Layers with higher penetration depths contribute less because they allow heat to dissipate farther into the structure, reducing their ability to buffer surface temperature fluctuations.

---

The active wall layer thickness is given by: $d_{a}=min(d_{wall},d_p)=min(d_{wall},\sqrt{\frac{\alpha \tau}{\pi}})=min(d_{wall},\sqrt{\frac{\lambda \tau}{\pi \rho c_p}})$.

Only the material within the penetration depth can absorb and release heat efficiently during a given time period.

Let's consider the 24h sine wave, that corresponds to the main frequency of the daily temperature variation, with a period$\tau=24\times 3600$s related to a 13cm concrete wall having $\lambda=1.7$ W/m.K, $\rho=2400$ kg/m$^3$ and $c_p=880$ J/kg.K.

The resulting penetration depth is $d_p=15$cm but the wall is 13cm thick. Therefore, the active wall layer thickness is $d_{a}=13$cm.

---
We can calculate the thermal resistance and heat storage capacity of a fraction $r_a$ of the active thickness of the wall i.e.: $R=r_a\frac{d_a}{\lambda S}$ and $C=r_a\rho c_p d_a S$ and, assimilating it to a first order system, the time constant is $RC=\frac{\rho c_p r_a^2 d_a^2}{\lambda}$.

$r_a\approx 0.9$ is a good approximation for a daily wave.

However, this approach is rather limited because $r_a$ has still to be guessed and only a single wall with a unique homogeneous layer can be considered.

This approach is interesting for understanding the layer effect on inertia.

Let's now decompose an homogenous wall into $n$ layers to discretize the space with $n\rightarrow\infty$ in order to analyze the effect of the thickness of the wall on inertia.

---

![bg fit](figs/wall_layers.png)

---

Each layer $i$ has:
- a temperature $T_i$, a density $\rho$, a specific heat $c_p$, a conductivity $\lambda$
- a thickness $d/n$
- a surface $S$
- a mass $\frac{\rho dS}{n}$
- a stored energy $E_i=\frac{\rho c_p dS}{n}T_i$
- a heat flow (variation of energy) $\varphi = \frac{\rho c_p dS}{n}\frac{d T_i}{dt}$
- $\forall i/ 1\le i \le n-2, T_{i-1}-T_i = \frac{d}{\lambda S}\varphi$
- for extreme layer, $T_{sin}-T_0=\frac{d}{2\lambda S}\varphi$ and $T_{n-1}-T_{sout}=\frac{d}{2\lambda S}\varphi$

---
## State space model


A state space model can represent any linear set of ordinary differential equations with a set of matrices:

$$
\frac{d}{dt}X(t) = A X(t) + B U(t)
$$
$$
Y(t)=CX(t)+DU(t)
$$
with $X(t)\in \mathbb R^{n\times 1}$, the state vector, $U(t)\in \mathbb R^{m\times 1}$, the vector of inputs and $Y(t)\in \mathbb R^{p\times 1}$, the vector of outputs, $A\in \mathbb R^{n\times n}, B\in \mathbb R^{n\times m}$, $C\in \mathbb R^{p\times n}$ and $D\in \mathbb R^{p\times m}$.

---

Let's apply this to a wall with n layers, the state space model can be written as:

Let $X(t) = [T_0(t), T_1(t), ..., T_{n-1}(t)]^T$ be the state vector of temperatures
Let $U(t) = [T_{sin}(t), T_{sout}(t)]^T$ be the input vector of boundary temperatures

---

The state matrix A is tridiagonal:
$$
A = \frac{n\lambda}{{\rho c_p d^2}}
\begin{bmatrix} 
-2 & 1 & 0 & \cdots & 0 \\
1 & -2 & 1 & \cdots & 0 \\
0 & 1 & -2 & \ddots & 0 \\
\vdots & \vdots & \ddots & \ddots & 1 \\
0 & 0 & \cdots & 1 & -2
\end{bmatrix}
$$

---
The input matrix B is:
$$
B = \frac{2n\lambda}{{\rho c_p d^2}}
\begin{bmatrix}
1 & 0 \\
0 & 0 \\
\vdots & \vdots \\
0 & 0 \\
0 & 1
\end{bmatrix}
$$
---

For output, if we want to observe all layer temperatures:
$$
C = I_{n\times n} \text{ and } D = 0_{n\times 2}
$$

This model represents the heat diffusion through the wall layers, where:
- The diagonal terms in A represent heat loss to adjacent layers
- The off-diagonal terms in A represent heat gain from adjacent layers
- The B matrix connects the boundary temperatures to the first and last layers
- The scaling factor $\frac{n\lambda}{{\rho c_p d^2}}$ comes from the thermal diffusivity

---

A state model can be easily simulated, discretized in time and analyzed.
If the input values are composed of samples, constant during each time slot having a duration $\Delta$ each, the state space model can be discretized as:

$$
X(t+\Delta) = \Phi X(t) + \Gamma U(t)
$$

with $\Phi = e^{A\Delta}$ and $\Gamma = \int_0^\Delta e^{A\tau}B d\tau$= $(e^{A\Delta}-I)A^{-1}B$

Therefore, with such a recurrent model, simulating a wall amounts to iterate from time step to time step.

---
With Python, a recurrent state model can easily be obtained with:

```python
from scipy.signal import cont2discrete

A = ...
B = ...
C = ...
D = ...
sample_time = ...

Phi, Gamma, C, D = cont2discrete((A, B, C, D), sample_time, method='zoh')
```
---

If $U(t)=U_k+(U_{k+1}-U_k)\frac{t-k\Delta}{dt}$, the recurrent state model is obtained by using 'foh' (first order hold) instead of 'zoh' (zero order hold).

If the matrices of the state space model are  slightly time varying, like for a time-varying ventilation, the state matrices has to recompute at each time step. A buffer can be used to store the matrices avoiding to recompute them at each time step.

---

## Eigenvalues to analyze the dynamic behavior

A state space model is not unique: it can be modeled in different state spaces. Let's call $P$ the projection matrix into the so-called canonical eigenvalues' model: $X=PX^\prime$ such as $P^{-1}AP=\Lambda$ is a diagonal matrix. The state space model is then:

$$
\frac{d}{dt}X^\prime(t) = \Lambda X^\prime(t) + P^{-1}B U(t)
$$
$$
Y(t)=CPX^\prime(t)+DU(t)
$$

---

The first equation can be developed as:
$$
\forall i, \frac{d x_i^\prime(t)}{dt} = \lambda_i x_i^\prime(t) + b_i^T U(t)
$$
The free regime is obtained for $U(t)=0$ and the eigenvalues $\lambda_i$ are the eigenvalues of $A$:
$$
x_i^\prime(t) = e^{\lambda_i t} x_i^\prime(0)
$$
For convergence, all $\lambda_i$ have to be negative. The mode of $x_i^\prime(t)$ will be attenuated by 99% after $t=\frac{ln(0.99)}{\lambda_i}=0.01\tau_i$ with $\tau_i=-\frac{1}{\lambda_i}$ the time constant of the mode $i$.

---

## Reduced 2nd/3rd order model of the thermal behavior of office H358

We are going to model the dynamic behavior of H358 office with reduced 2nd/3rd order state space model.

Here is a 3 capacitances model of the office. A state space model is going to be deduced as an illustrative example of the state space modeling.
There are 3 temperature nodes $T_{cor}$, $T_{in}$ and $T_{out}$ and 3 heat flows $\varphi_{cor}$, $\varphi_{in}$ and $\varphi_{out}$.

---

![bg fit](img/RCmodel.png)

---

Let's model the material storage capabilities by capacitances $C=\rho c_p V$, with $\rho$ the density, $c_p$ the specific heat and $V$ the contributing wall volume. We will have to check wether this simplified way of modeling is an acceptable or not. The model with 3 thermal capacitances (2 for the dynamics of the wall and one for the air) is given by:

$$
\varphi_{cor}+\varphi_{in}=\varphi_{i1}+\varphi_{i2}+\varphi_{air}+\varphi_{out}$$
$$
T_{cor}-T_{in}=R_{cor}\varphi_{cor}
$$
$$
T_{in}-T_{out}=R_{out}\varphi_{out}
$$

$$
T_{in}-\tau_{1}=R_{i1}\varphi_{i1}
$$
$$
T_{in}-\tau_{2}=R_{i2}\varphi_{i2}
$$

---

$$
C_{i1}\frac{d\tau_1}{dt}=\varphi_{i1}
$$
$$
C_{i2}\frac{d\tau_2}{dt}=\varphi_{i2}
$$
$$
C_{air}\frac{dT_{in}}{dt}=\varphi_{air}\ 
$$

---

It can be reformulated as:

$$
C_{i1}\frac{d\tau_1}{dt}=-\frac{\tau_{1}}{R_{i1}}+\frac{T_{in}}{R_{i1}}
$$
$$
C_{i2}\frac{d\tau_2}{dt}=-\frac{\tau_{2}}{R_{i2}}+\frac{T_{in}}{R_{i2}}
$$
$$
C_{air}\frac{dT_{in}}{dt}=-\frac{1}{R}T_{in}+\frac{\tau_{1}}{R_{i1}}+\frac{\tau_{2}}{R_{i2}}+\frac{T_{out}}{R_{out}}+\frac{T_{cor}}{R_{cor}}+\varphi_{in}
$$
$$
\frac{1}{R}=\frac{1}{R_{cor}}+\frac{1}{R_{i1}}+\frac{1}{R_{i2}}+\frac{1}{R_{out}}
$$

and the resulting state space model is given by:

---

$$
\frac{d}{dt}\begin{bmatrix} \tau_1\\ \tau_2 \\ T_{in} \end{bmatrix}=
\begin{bmatrix} -\frac{1}{R_{i1}C_{i1}} & 0 & \frac{1}{R_{i1}C_{i1}}\\
0 & -\frac{1}{R_{i2}C_{i2}} & \frac{1}{R_{i2}C_{i2}}\\
\frac{1}{R_{i1}C_{air}}& \frac{1}{R_{i2}C_{air}}& -\frac{1}{RC_{air}}\\
\end{bmatrix}
\begin{bmatrix} \tau_1\\ \tau_2 \\ T_{in} \end{bmatrix}+
\begin{bmatrix} 0 & 0 & 0 \\ 0 & 0 & 0 \\ \frac{1}{R_{cor}C_{air}} & \frac{1}{R_{out}C_{air}} & \frac{1}{C_{air}}\end{bmatrix}
\begin{bmatrix}T_{cor}\\T_{out}\\\varphi_{in} \end{bmatrix}
$$

However, the air capacitance yields fast dynamics which are much faster than the 1 hour sample time, that is commonly used for building simulation and for generating energy strategies. Let's neglect the air capacitance and re-calculate the model. We consider that $RC_{air}$ is small and consequently that $\frac{dT_{in}}{dt}=0$. It leads to a 2nd order model (2 derivatives):

---

$$C_{i1}\frac{d\tau_1}{dt}=-\frac{\tau_{1}}{R_{i1}}+\frac{T_{in}}{R_{i1}}$$
$$C_{i2}\frac{d\tau_2}{dt}=-\frac{\tau_{2}}{R_{i2}}+\frac{T_{in}}{R_{i2}}$$
$$T_{in}=\frac{R}{R_{i1}}\tau_{1}+\frac{R}{R_{i2}}\tau_{2}+\frac{R}{R_{cor}}T_{cor}+\frac{R}{R_{out}}T_{out}+R\varphi_{in}$$

that can be rewritten as:

---

$$C_{i1}\frac{d\tau_1}{dt}=\frac{R-R_{i1}}{R_{i1}^2}\tau_{1}+\frac{R}{R_{i1}R_{i2}}\tau_{2}+\frac{R}{R_{i1}R_{cor}}T_{cor}+\frac{R}{R_{i1}R_{out}}T_{out}+\frac{R}{R_{i1}}\varphi_{in}$$
$$C_{i2}\frac{d\tau_2}{dt}=\frac{R}{R_{i1}R_{i2}}\tau_{1}+\frac{R-R_{i2}}{R_{i2}^2}\tau_{2}+\frac{R}{R_{i2}R_{cor}}T_{cor}+\frac{R}{R_{i2}R_{out}}T_{out}+\frac{R}{R_{i2}}\varphi_{in}$$
$$T_{in}=\frac{R}{R_{i1}}\tau_{1}+\frac{R}{R_{i2}}\tau_{2}+\frac{R}{R_{cor}}T_{cor}+\frac{R}{R_{out}}T_{out}+R\varphi_{in}$$

and reformulated as a state space model:

---

$$\frac{d}{dt}\begin{bmatrix} \tau_1\\ \tau_2 \end{bmatrix}=
\begin{bmatrix} \frac{R-R_{i1}}{R_{i1}^2C_{i1}} & \frac{R}{R_{i1}R_{i2}C_{i1}} \\
\frac{R}{R_{i1}R_{i2}C_{i2}} & \frac{R-R_{i2}}{R_{i2}^2C_{i2}}\\
\end{bmatrix}
\begin{bmatrix} \tau_1\\ \tau_2 \end{bmatrix}+
\begin{bmatrix} \frac{R}{R_{i1}R_{cor}C_{i1}} & \frac{R}{R_{i1}R_{out}C_{i1}} & \frac{R}{R_{i1}C_{i1}} \\ \frac{R}{R_{i2}R_{cor}C_{i2}} & \frac{R}{R_{i2}R_{out}C_{i2}} & \frac{R}{R_{i2}C_{i2}} \end{bmatrix}\begin{bmatrix}T_{cor}\\T_{out}\\\varphi_{in} \end{bmatrix}$$
$$T_{in}=\begin{bmatrix} \frac{R}{R_{i1}} & \frac{R}{R_{i2}}\end{bmatrix}
\begin{bmatrix} \tau_1\\ \tau_2 \end{bmatrix}+
\begin{bmatrix}\frac{R}{R_{cor}} & \frac{R}{R_{out}} & R\end{bmatrix}
\begin{bmatrix}T_{cor}\\T_{out}\\\varphi_{in} \end{bmatrix}$$

---

## Handling time-varying state models

The natural ventilation represents an advection phenomena i.e. a transportation of heat with the air. The state space model has to be time-varying.

---

## Model reduction with balanced truncature

Most common approach for model reduction is named Balanced Truncature, which inherits from the Moore approach (see [](doc/modelreduction.pdf)). The principle is to truncate a balanced state representation, which is a projection of the state space representation where state variables are ordered according to their impact on controllability and observability following a descending order.

---

It relies on 2 steps:
- computation of an equivalent state space representation where state variables are ordered according to their impact on controllability and observability following the descending order using a base change
- truncate the state space representation in order to keep the $r$  first state variables, where $r$ is the reduction order (the pymor library can be used in Python). 
