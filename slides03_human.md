---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **Inhabitants and buildings**
stephane.ploix@grenoble-inp.fr

---

## Air characteristics

- Reminder: for an ideal gas composed of $n_i$ moles (1 mol~$6.022\ 10^{23}$ molecules) of component $i$, we have (R~8.314 J/mol.K):
$$
n=\sum_i n_i=\frac{PV}{RT}\rightarrow \eta_i=\frac{PV_i}{RT}=\frac{P_i V}{RT}
$$
with $\eta_i$, the mole fraction: $\sum_i \eta_i=1$, $V_i=\eta_i V$ (partial volume) or $P_i=\eta_i P$ (partial pressure)

---

| gas | mole mass (g/mol)|
|-----|------------|
| argon (Ar) |	39.94|
| carbon dioxide (CO2) |	44.01 |
| carbon monoxide (CO) | 28.01 |
| hydrogen (H2) | 2.02 |
| nitrogen (N2) | 28.02 |
| oxygen (O2) |	32 |
| dry air |	28,97 |
| water vapor (H2O)	|	18.01 |

---

Density of dry air at 20°C: 1.204kg/m$^3$ (41.4 $mol/m^3$) at standard atmospheric pressure: 101325 Pa

The dry air composition in mole/volume fractions $\eta_i$ and mass fraction $\mu_i$:

- nitrogen: $\eta_{N_2}$ = 78.08%, $\mu_{N_2}$=75.51%
- oxygen: $\eta_{O_2}$ = 20.95%, $\mu_{O_2}$ = 23.14%
- argon: $\eta_{Ar}$ = 0.93%, $\mu_{Ar}$ = 1.28%
- carbon dioxide: $\eta_{CO₂}$ = 0.04%, $\mu_{CO₂}$ = 0.04%
  
plus other traces less than $\eta_i<0.01%$ each

Additionally, wet air can contain up to 4% of the volume of water vapor.

---

Note: Calling $M_i$, the mole mass of a gas component $i$, we have:

$$
\mu_i = \frac{M_i\eta_i}{\sum_i M_i\eta_i}
$$

 The mole/volume and mass ratios of the wet air are given by:

$$
 \forall i, \eta'_i = \frac{\eta_i}{1 + AH / 752};\ 
 \eta'_{H_2O} = \frac{1}{1 + 752 / AH}
 $$
$$
\mu'_{i}=\frac{M_i\eta_i}{\frac{28.97}{1+\frac{AH}{752}}+\frac{18.1}{1+\frac{752}{AH}}};\mu'_{H_2O}=\frac{1}{1+1.6\frac{1+\frac{752}{AH}}{1+\frac{AH}{752}}}
$$

---


For instance, let's consider that AH=$8g$ of water vapor per m$^3$$. The new fractions $\eta'_i$ and $\mu'_i$ for AH=8g/m$^3$ are given by: 

- nitrogen: $\eta'_{N_2}=77.26\%, \mu'_{N_2}=75.82\%$
- oxygen: $\eta'_{O_2} = 20.73\%, \mu'_{O_2} = 23.23\%$,
- argon: $\eta'_{Ar} = 0.92\%, \mu'_{Ar} = 1.29\%$, 
- carbon dioxide: $\eta'_{CO_2} = 0.0396\%, \mu'_{CO_2} = 0.06\%$
- water vapor: $\eta'_{H_2O} = 1.052632\%, \mu'_{H_2O} = 0.6\%$

---

## Relationship between absolute (AH) and relative humidity (RH)

The relative humidity is defined by:
$$
RH = \frac{P_{H_2O}}{\hat P_{H_2O}}
$$
where $P_{H_2O}(T)$ and $\hat P_{H_2O}(T)$ are the vapor pressure and the saturation vapor pressure.

---

$\hat P_{H_2O}$ is given in Pascal (and T in Celsius) by the Magnus formula: 
$$\hat P_{H_2O}=611.2 e^{\frac{17.67T}{243.12+T}}$$

The vapor pressure is deduced from the ideal gas law:
$$
P_{H_2O}=\frac{nRT_K}{V}=AH\frac{RT_K}{M_{H_2O}}=\frac{273.15+T}{2.167}\times AH
$$

We've got the relationship between $AH$ and $RH$:
$$
RH(AH, T)=\frac{273.15+T}{1324.5 e^{\frac{17.67T}{243.12+T}}}\times AH(g/m^3))
$$

---

The unit used for absolute humidity in g/kg (g of water vapor per kg of dry air). The ideal gas law applied to $1m^3$ of dry air leads to:
$$
1m^3\times P_{atm} = n R T_K \rightarrow n = \frac{P_{atm}}{R T_K}  \rightarrow m_\text{dry air} = \frac{M_\text{dry air}P_{atm}}{R T_K}
$$
Because the volume considered is $1m^3$, it comes:
$$
\rho=\frac{P_{atm}M_\text{dry air}}{R (273.15+T)}\text{ and then AH in g/kg}
$$
$$
RH(AH, T, P_{atm})=\frac{P_{atm}}{380114 e^{\frac{17.67T}{243.12+T}}} AH\approx 0.267AH e^{-\frac{17.67T}{243.12+T}}
$$

---

## Impact of breath

During respiration, mainly glucose is consumed: 
$$
C_6H_{12}O_6+6\times 0_2 \rightarrow 6\times CO_2+6\times H_2O + \text{energy}
$$
Therefore, 1 molecule of $O_2$ is transformed into 1 molecule of $CO_2$ and 1 molecule of $H_2O$ (vapor)

The exhaled air from breaths is at 38000ppm of $CO_2$ and 100% of relative humidity at 37°C.

---

### production of CO2
At rest, for $n$ persons, $Q_{breath\ m^3/s}=n\frac{6l/min}{60}=n\times 10^{-4}m^3/s$, therefore the $CO_2$ production is $n\times38000ppm\times 10^{-4} m^3/s=$ $3.8\times n\ ppm.m^3/s$.

Similarly, at normal activity, production is about $10 \times n\ ppm.m^3/s$.

At moderate exercise, production is about $13 \times n\ ppm.m^3/s$.

At stress exercise, production of $CO_2$ is about $22\times n\  ppm.m^3/s$.

(see table on next slide)

---

$Q_{breath}$ (in l/min, divide by $60000$ for m$^3$/s) is given by the next table:

<img src="figs/breath.png" width="100%"/>

---

### Production of moisture

The saturation vapor pressure is given by the Magnus formula: $\hat P_{H_2O}=611 e^{\frac{17.67T}{243.12+T}}$ in Pascal. For T=37°C, $\hat P_{H_2O}\approx 6305$​Pa.

The exhaled volume of air through breath at normal activity is $16 l/min$

As seen before, for the ideal gas law, we have the mass of water vapor $m_{H_2O}$ per unit of volume (in kg/m$^3$): $m_{H_2O}=\frac{\hat P_{H_2O}}{R(273+T)}\times M_{H_2O}$with $M_{H_2O}=0.018kg/mol$, $R=8.61J/mol.K$. 

Therefore, $m_{H_20}=43 mg/m^3$

---

For any $Q_\text{breath}$, we have:
$$
\Pi_{H_20}=n\times m_{H_2O}\times Q_\text{breath}
$$

---

The production of $CO_2$ and $H_2O$ is denoted respectively $\Pi_{CO_2}$ and $\Pi_{H_2O}$. The variation of $CO_2$ (and resp. $H_2O$) in a room (volume $V$, ventilation $Q$)is:
$$
V \frac{d C_{CO_2,in}}{dt} = Q_{m^3/s} \left(C_{CO_2, out} - C_{CO_2,in}\right) + \Pi_{CO_2}
$$
or,
$$
\frac{d C_{CO_2,in}}{dt} = \frac{Q_{vol/h}}{3600} \left(C_{CO_2, out} - C_{CO_2,in}\right) + \frac{\Pi_{CO_2}}{V}
$$


---

Similarly, for moisture:
$$
\frac{d C_{H_2O,in}}{dt} = \frac{Q_{vol/h}}{3600} \left(C_{H_2O, out} - C_{H_2O,in}\right) + \frac{\Pi_{H_2O}}{V}
$$
steady state leads to: 
$$
Q_{vol/h} = \frac{3600\Pi_{H_2O}}{V\left(C_{H_2O,in} - C_{H_2O, out}\right)}
$$
where $\Pi_{H_2O}$ in $g/m^3.s$

---

## A containment indicator: ICONE

Breath yields $CO_2$ (and smells) and vapor. High values must be avoided because
- even if $CO_2$ is __not a pollutant__ at low concentration, it reveals __bad smells__ and at higher levels, it causes __headache__.
- __ICONE__ indicator measures the __containment level__. i.e. exposure time to high concentration of $CO_2$ (see hereafter).

---

| $C_{CO_2}$ | effect on human |
|----------- | --------------- |
| 400-1000ppm | comfortable     |
| 1700ppm    | considered as significant confinement |
| 6000ppm    | cause headache after 30min exposure |
| 30000ppm   | cause increase of breath rate and beat rate |
| 100000ppm  | cause vomiting |
| >200000ppm | black-out and death |

---

### Impact of $CO_2$

Let $k$ be an instant of a regularly sampled time dataset $(0, T_s, 2T_s,\dots, (n-1)T_s)$. 
- Let $O_k$ be the number of occupants in a zone.
- Let $C_k$ be the CO2 concentration in the zone at time $k$.
- Let $level(C_k, O_k) \in \{-1, 0, 1, 2\}$ such as:

---

- $O_k=0 \rightarrow level(C_k, O_k) = -1$ (no exposure) 
- $O_k>0$ and $C_k<1000ppm \rightarrow level(C_k, O_k) = 0$ (normal exposure) 
- $O_k>0$ and 1000ppm<$C_k<1700ppm \rightarrow level(C_k, O_k) = 1$ (medium exposure) 
- $O_k>0$ and $C_k>1700ppm \rightarrow level(C_k, O_k) = 2$ (high exposure) 

---

Let's define the occurrence frequency of a level=$L_i$:
$$
f_i = \frac{\mid\{k/ level(C_k, O_k) = L_i\}\mid}{\mid \{k/ level(C_k, O_k) \ge 0\}\mid}
$$

Because of an admitted logarithmic relationship between stimuli and olfactive sensation, the IC0NE indicator of containment (confinement in French) is defined by:
$$
ICONE = 8.3 log_{10}(1+f_1+3f_2)
$$

Bur once again, $CO_2$ is not a pollutant but formaldehydes, Ozone, Radon are...

---

The following scale is admitted:
| ICONE | level of containment |
|-------|----------------------|
|   0   |   no containment     |
|   1   |  low containment     |
|   2   | average containment  |
|   3   |  high containment    |
|   4   | very high containment|
|   5   |  extreme containment |

---

## Impact of moisture

  High humidity might causes mould, degradation whereas low humidity might cause skin and respiratory issues

---

| Room	| Ideal Relative Humidity (%) |	Recommended Humidity Levels |
|-------|--------------------|------------------------------|
|Living Room|	40-50%	|Comfortable for most people; prevents static electricity and damage to furniture|
|Bedroom|	30-50%	|Improves sleep quality; prevents dry skin and respiratory problems|
|Kitchen	|40-50%|	Prevents mould and mildew growth due to cooking steam|
|Bathroom	|40-60%|	Common high humidity from showers; manage to avoid mould|

---

| Room	| Ideal Humidity (%) |	Recommended Humidity Levels |
|-------|--------------------|------------------------------|
|Basement	|30-50%|	Prevents dampness, mould, and mildew|
|Home Office	|30-50%|	Comfortable for working; protects electronics from moisture damage|

Note that 100% of humidity means the air is fully saturated in vapor and dew starts to appear. It depends on the air temperature (see Givoni chart)

---
### Impact of body metabolism
- human body regulate its interior temperature by adjusting
  - its perspiration that take advantage of latent heat
    - steam from breath (see respiration formula)
    - sweat through skin: evaporation is endothermic 
  <img src="figs/Vapor_pressure.png" width="40%"/>

---

$$P_\text{absorbed}=\tau L_\text{water}$$ 
where: 
- $P_\text{absorbed}$ is the absorbed power in Watt
- $L_\text{water}$ is the latent heat of water vaporization ($2.45\times 10^6$J/kg) 
- 𝛕 is the rate of transpiration in kg/s
  
For instance, $\tau$=0.5 l/day (from 0.1 to 8l/day) leads to 10W of absorbed power (from 2W to 227W). 

If the relative humidity is 100% and ambient temperature is over 35°C, the death can occur within few hours (one of the issue of the climate change at equatorial regions).

---
$$
P_{rest} = \frac{31°C-T_\text{ambient}}{0.155R_{clo}}S_\text{skin}
$$
with $T_\text{ambient}$ in °C, $R_{clo}$ in clo 
One clo is the amount of insulation that allows a person at rest to maintain thermal equilibrium in an environment at 21°C in a normally ventilated room with 0.1 m/s air movement: 1 clo = 0.155 K·$m^2$/$

---

|clothes description	| Icl (clo) |
|--------------------|----------|
|Walking shorts, short-sleeved shirt	| 0.36 |
|Trousers, short-sleeved shirt |	0.57 |
| Trousers, long-sleeved shirt	| 0.61 |
| Same as above, plus suit jacket	| 0.96 |
| Same as above, plus vest and T-shirt	| 0.96 |
| Trousers, long-sleeved shirt, long-sleeved sweater, T-shirt |	1.01 |
| Same as above, plus suit jacket and long underwear bottoms |	1.30 |

---

|clothes description	| Icl (clo) |
|--------------------|----------|
| Sweat pants, sweat shirt |	0.74 |
| Long-sleeved pajama top, long pajama trousers, short 3/4 sleeved robe, slippers (no socks) |	0.96 |
| Knee-length skirt, short-sleeved shirt, panty hose, sandals	| 0.54 |
| Knee-length skirt, long-sleeved shirt, full slip, panty hose |	0.67 |
| Knee-length skirt, long-sleeved shirt, half slip, panty hose, long-sleeved sweater	| 1.10 |

---

|clothes description	| Icl (clo) |
|--------------------|----------|
| Knee-length skirt, long-sleeved shirt, half slip, panty hose, suit jacket	| 1.04 |
| Ankle-length skirt, long-sleeved shirt, suit jacket, panty hose |	1.10 |
| Long-sleeved coveralls, T-shirt |	0.72 |
| Overalls, long-sleeved shirt, T-shirt	| 0.89 |
| Insulated coveralls, long-sleeved thermal underwear, long underwear bottoms |	1.37 |


___

* its sensitive heat whose exchange with the environment depends on:
  *+* the surface of the body i.e. the skin surface: $S_{skin}\approx 0.202 m^{0.425} h^{0.725}$ where $m$ is in kg and $m$ in meters
  *+* the produced work $P_\text{body}=P_\text{met} P_\text{rest}$ (see table hereafter; 1 met = 58.1W/m$^2$)
  *+* the clothes

---

One considers that the energy produced by human body is spread as:
- 24% with perspiration (removed by ventilation)
- 35% with convection (skin-air exchanges, important part of metabolism)
- 35% with radiation (body radiates power in the infrared range, important part of metabolism)
- 6% with food (which has a different temperature)
- 1% with conduction (feet)
---

<img src="figs/met.png" width="60%"/>

---

# **A dwelling: a system of services**

## Maintaining comfortable indoor living conditions

Temperature, humidity, air speed, good air quality,...

---

###  Comfort assessment with the psychrometric (or Givoni) chart  (for indoor or outdoor environment)

One of the major applications of the Psychrometric Chart is in air conditioning, and we find that most humans feel comfortable about when the temperature is between 20°C and 27°C, and the relative humidity is between 20% and 80%.

---

![bg fit](figs/psychrometric_chart.jpg)

---
####  Comfort assessment with the predictive mean vote (PMV, is for indoor environment)

For Fänger (1982), comfort depends on __air temperature__ $T_\text{air}$, __radiant temperature__, __relative air velocity__, __clothing__ and __base metabolism__ (without considering human work). 

__Discomfort__ is considered as __the metabolic heat flow $L$ required to maintain__ the comfortable __skin temperature__ and the latent energy corresponding to __comfortable sweat__.

---

The __comfortable skin__ temperature during comfort is given by:
 $$T_\text{skin} = 35.7 - 0.0276 M$$
 where $M$ is the metabolic rate per skin surface in $W/m^2$ and the comfortable sweat secretion for comfort: 
 $$P_\text{sweat} = 0.42(M-58)$$

In order to characterize the relationship between the thermal sensation and the metabolic heat flow $L$, lots of people have been asked to assess their level of comfort under different conditions accordingly to the next table. The resulting relationship is given by: 
$$PMV=3.155 (0.303 e^{-0.144M+0.028})L$$

---

| value | thermal sensation |
|----|-------------------|
| +3 | hot               |
| +2 | warm              |
| +1 | slightly warm     |
|  0 | neutral           |
| -1 | slightly cool     |
| -2  | cool              |
| -3 | cold              |

---

The __predicted mean vote__ is the __average response__ of a large number of people. Given the subjective nature of comfort, there will actually be a distribution of satisfaction among a large group of people. 

Next figure shows an empirical relationship between the __percentage of people dissatisfied (PPD)__ with a thermal environment as a function of the PMV: 
$$\text{PPD} = 1 - 0.95 e^{-0.03353 \text{PMV}^4 - 0.2179 \text{PMV}^2}$$

An example of implementation of PMV/PPD computation is available at ```buildingenergy/comfort.py```

---
<span style="font-size:0.8em;">

| input data | output data |
|-----------|--------------|
|air_temperature_C: 21| PMV: -0.6|
|relative_humidity_percent: 50|P_metabolism_W_m2: 58.15|
|radiant_temperature_C: 21|P_perspiration_sweat_W_m2: 12.45|
|Icl_CLO: 1|P_perspiration_diffusion_W_m2: 0|
|metabolism_MET: 1|P_breath_latent_W_m2: 4.57|
|air_speed_km_h: 0.1|P_breath_sensitive_W_m2: 1.06|
| | P_rad_W_m2: 21.5624 |
| | P_conv_W_m2: 17.8693 |
| | Tclothes_C: 26.4247 |
| | Pvapor_Pa: 376.071 |

</span>

---

![bg fill](figs/pmv_ppd.png)

---

### Adaptive comfort

The __PMV__ provides a __very strict__ definition of what is thermal comfort but it does not take into account that __people are not passive__. They can:
- adapt __physiologically__ to warmer or colder temperatures by gradually becoming accustomed to these conditions (at home, 21°C during winter does not lead to the same feeling that 21°C during summer even with the same clothes)
- adapt their expectations of thermal comfort because of capabilities to __adjust their environment__ (opening/closing windows,...). 

---

The __adaptive comfort__ model differs from traditional models such as the PMV in that it allows a __wider range of acceptable indoor temperatures__, depending on outdoor temperatures. 

This model is particularly __applicable to naturally ventilated buildings, where occupants have more control over their environment, unlike air-conditioned buildings where thermal conditions are often rigid and controlled by heating and cooling systems__.

---

For example, according to the standards established by __ASHRAE__ (American Society of Heating, Refrigerating and Air-Conditioning Engineers), the acceptable __indoor temperature in a naturally ventilated building can vary between 18°C and 30°C__, depending on outside temperatures.

---

There are different models for adaptive comfort but all of them rely on a 7-day to 30-day outdoor temperature average of the past, a linear relation giving the indoor comfort temperature and a level of tolerance around this comfort temperature.

For instance, ASHRAE 55 Standard proposes the model:
$$
T_\text{comfort} = 0.31 T_{out, moy} + 17.8
$$
if $T_{out, moy} \in[10°C, 35.5°C]$

The EN 15251/ISO 17772-1 standard proposes the model:
$$
T_\text{comfort} = 0.33 T_{out, moy} + 18.8
$$

---

with the following tolerances:
- **Catégorie I** : $T_\text{comfort} \pm 2°C$
- **Catégorie II** : $T_\text{comfort} \pm 3°C$
- **Catégorie III** : $T_\text{comfort} \pm 4°C$

---

#### outdoor apparent temperature (température ressentie)

<img src="figs/feels_like.png" width="80%"/>

---

#### TIC (Temperature Intérieure Conventionnelle from RT2005)

The indoor __operative temperature of the reference hot day__ is conventionally the __coldest of the five hottest days of a year__.

This coefficient represents the hourly value of the __operating temperature__ during the period of occupation. (__50% air temperature + 50% wall temperature__)

---

## other home services

The system of services also includes home services like:
    - cleaning dishes or clothes service
    - service for conserving food
    - ...
  - energy services to collect energy from the environment or the grid

---

### What is the difference between efficiency (efficacité) and sufficiency ($\approx$ sobriété) ?

Efficiency deals with services: it's the ratio between the quality of a service divided by a needed energy. Improving the efficiency is a matter of product design: for instance, a building envelope or a HVAC system.

- Sufficiency with how people use the services. For instance, the temperature set-point they set, how they use a washing machine,...

--- 

## Handling measurement data

Most sensors do not provide data regularly sampled. It is all the more true with self-powered and low consumption sensors using radio communication where each data sent has an energy cost. An sensor usually sends a data when a significant change has been detected (x% depending on the sensitivity) and a minimum time lap has passed.

The way of getting usable sampled data depends on the type of measured variable. In the next, the red arrows stand for received measurement data, and horizontal lines illustrate the resulting sampled values.

---

Regarding motion detectors, one have to count the number of detected motion per sample time. For power, we have to compute the average power per sample time.
![](figs/sampling1.png)

---

Energy data are processed in the same way than power whereas for a contact sensor, one has to compute the ratio of time it remains in the "detection" state. A contact sensor used with a door or a window leads to values between 0 and 1: 0 means always closed during the considered time period (1 hour here to match the weather data precision), 1 means always opened and x, x means it has been opened x% of the time period.

---

![](figs/sampling2.png)

---

<br/><br/><br/>
# Open and solve workshop03_human.ipynb
