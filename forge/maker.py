
from abc import ABC
from buildingenergy.data import DataProvider
from buildingenergy.thermal import ThermalNetwork, CAUSALITY
from typing import Any
from enum import Enum


class AirflowNetwork:

    def CCO2(name):
        pass
    
    
class Model:
    
    def __init__(self, dp: DataProvider, tnet: ThermalNetwork, anet: AirflowNetwork) -> None:
        super().__init__()
        self.dp: DataProvider = dp
        self.tnet: ThermalNetwork = tnet
        self.anet: AirflowNetwork = anet
    



class RoomZone:
    
    def __init__(self, model: Model, name: str, heated: bool, ventilated: bool, reference_temperature: float, ) -> None:
        super().__init__(model)
        self.name: str = name
        self.kind = type(self).__name__
        self.model = model
        self.heated = heated
        self.ventilated = ventilated
        self.reference_temperature = reference_temperature
        




class WallMaker(Maker):
    
    def __init__(self, dp: DataProvider, tnet: ThermalNetwork) -> None:
        super().__init__(dp, tnet)
        
    def set_zone_name