---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

## __**Design of building and dwelling**__


stephane.ploix@grenoble-inp.fr

---


## IV - Solution to consider during the design of dwellings

1. Low emission transportation accessible? can they park?
2. Are there energy source(s) nearby? local renewable energy production?
3. Re-use of materials, components,...? Heavy or light structure?
4. Bioclimatism or fitting into the environment
   1. solar radiation? natural solar protection ? passive or active solar protections?
   2. shape of the building?
   3. room usage and locations,...)?
5. Resilient (under control) or adaptive (reconfigurable) to climate variability or change?
6. Indoor or outdoor insulation? level of insulation? Inertia to avoid air conditioning?
7. Heat pump with large indoor emitter or pulse air? biomass boiler? heating system?
8.  Urban grids/micro-grid? Warm/cold water? Electricity?
9.  Heat recovery (dual flow ventilation, grey water, data center)?
10. Taking advantage of the ground? Canadian/Provencal well? ground water? geothermal energy?
11. Taking advantage of the sky? Radiative cooling?
12. Trombe wall?
13. Relevant controls and automation or Involvement of inhabitants in everyday management
14. Energy storage?
15. Rain water recovery?

