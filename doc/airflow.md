# diffusion of CO2

As an assumption, we assume that the diffusion of $CO_2$ is in most situations relatively more important that transportation of air mass. It means that when a volume $\Delta_V$  with a  $CO_2$  concentration $C_A$ is moving from a room A to a room B, there is a equivalent volume with a concentration $C_B$ moving from room B to A. Air flows passing through the building are not considered i.e. the ones coming from outdoor into room A then moving to room B before returning to outdoor. Air flows are always considered as bi-directional.

The assumption is going to be validated with recorded measurements.

Let's consider 2 rooms A and B with respectives $CO_2$ concentration $C_A$ and $C_B$ and outdoor at concentration $C_{O}$. It comes:
$$
V_A\frac{dC_A}{dt}=Q_{A,B}(C_B-C_A)+Q_{A,O}(C_O-C_A)+\Pi_A\\
V_B\frac{dC_b}{dt}=Q_{A,B}(C_A-C_B)+Q_{B,O}(C_O-C_B)+\Pi_B\\
$$
where  $\Pi_A$ and $\Pi_B$ are $CO_2$ breath productions 

with $Q_{A,B}=Q_{B,A}$.

Generally speaking, a simulated room $i$ connected to a set of simulated rooms $\mathbb J$ and to a set of known concentration areas $\mathbb{K}$  leads to:
$$
\forall i, V_i \frac{dC_i}{dt}=-\left(\sum_{j\in\mathbb J\cup \mathbb K}Q_{i,j}\right)C_i + \sum_{j\in\mathbb J\cup \mathbb K}Q_{i,j}C_j +\Pi_i
$$
