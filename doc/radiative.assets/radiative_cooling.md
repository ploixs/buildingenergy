# Radiative cooling



## Radiance for a flat surface directed to the sky zenith
$$
L_{flat}=\epsilon_{flat} \sigma T_{g}^4
$$

### Total electromagnetic radiated power transfer for the sky zenith to a flat surface (including the radiance $L_{flat}$​)

According to (Berdhal et al, 1982), the clear sky emissivity is given by $\epsilon_{sky}=\alpha+0.006T_{dp}$ with $\alpha=0.727$ and $\beta=0.0060$ during the day and $\alpha=0.741$ and $\beta=0.0062$ during the night. $T_{dp}$ stands for the dew point temperature in degrees close to the ground.

The clear sky emittance is given by: $L_{clear}=\sigma\epsilon_{clear}T_{weather}^4$ where $T_{clear}$​ is in Kelvin.

According to (Swinbank, 1963), the cloud temperature can be approximated by $T_{cloud}=T_{weather}-5$  and the cloud emissivity is $\epsilon_{cloud}=0.96$. Therefore, $L_{cloud}=\sigma\epsilon_{cloud}(T_{weather}-5)^4$ with $T_{weather}$ in Kelvin.
$$
L_{clear}= 0.711+0.56\frac{T_{dp}}{100}+0.73\left(\frac{T_{dp}}{100}\right)^2 \\
L_{cloud}=\epsilon_{cloud} \sigma T_{cloud}^4 \\
L_{sky}=(1-\zeta)L_{clear} + \zeta L_{cloud}\\
\epsilon_{cloud}=0.96\\
T_{cloud}=T_{weather}-5\\
\rightarrow  L_{sky}=9.36e^{-6}(1-\zeta)\sigma T_{weather}^6+ \zeta \epsilon_{cloud} \sigma (T_{weather}-5)^4\\
$$

where $\zeta\in [0, 100\%]$, is the nebulosity or the cloudiness and "clear sky" $\leftrightarrow \zeta=0$ and "sky totally hidden" by clouds covered sky $\leftrightarrow \zeta=1$​.

The global sky emittance is given by: $L_{sky}=(1-\zeta)L_{clear}+\zeta L_{cloud}$ where $\zeta\in[0,1]$ stands for the cloudiness. Developing the latter expression yields:
$$
L_{sky}=(1-\zeta)\sigma\epsilon_{clear}T_{weather}^4+\zeta\sigma\epsilon_{cloud}(T_{weather}-5)^4\\
\dots\approx (1-\zeta)\sigma\epsilon_{clear}\bar T^4+\zeta\sigma\epsilon_{cloud}(\bar T-5)^4 +\dots\\ 
\dots +4\sigma \left((1-\zeta)\epsilon_{clear}\bar T^3(T_{weather}-\bar T)+\zeta\epsilon_{cloud} (\bar T-5)^3\right)(T_{weather}-\bar T)
$$
Let's now consider another facing flat surface $L_{flat}=\epsilon_{flat}\sigma T_{flat}^4$,  which can be approximated by:
$$
L_{flat}\approx \epsilon_{flat}\sigma \bar T^4 + 4\epsilon_{flat}\sigma\bar T^3(T_{flat}-\bar T)
$$
It leads to:
$$
\Delta L_{sky-flat}=(1-\zeta)\sigma\epsilon_{clear}\bar T^4+\zeta\sigma\epsilon_{cloud}(\bar T-5)^4 -\epsilon_{flat}\sigma \bar T^4+\dots\\ 
\dots +4\sigma \left((1-\zeta)\epsilon_{clear}\bar T^3+\zeta\epsilon_{cloud} (\bar T-5)^3 - \epsilon_{flat}\bar T^3\right)(T_{weather}-\bar T)\\ 
+ 4\epsilon_{flat}\sigma\bar T^3(T_{weather}-T_{flat})
$$
The power exchanged $P_{sky-flat}$ depends on clear sky emissivity, which varies according to day and night. It is given by:
$$
P_{sky-flat}= \sigma S\left( (1-\zeta)\epsilon_{clear}\bar T^4+\zeta\epsilon_{cloud}(\bar T-5)^4 -\epsilon_{flat} \bar T^4\right)+ \dots \\
\dots+4\sigma S \left((1-\zeta)\epsilon_{clear}\bar T^3+\zeta\epsilon_{cloud} (\bar T-5)^3 - \epsilon_{flat}\bar T^3\right)(T_{weather}-\bar T)
$$
The resistance between $T_{weather}$ and $T_{flat}$:
$$
R_{weather-flat}=\frac{1}{4\epsilon_{flat}\sigma S \bar T^3}
$$
It can be implemented by an equivalent electric circuit:

![image-20240423105731111](radiative_cooling.assets/image-20240423105731111.png)

## Convective transfer 

According to the official Th-Bat calculation method, for a wind speed $v$, the convective transfer between a surface and the outdoor temperature is equal to:
$$
R_{conv}=\frac{1}{11.4+5.7v}
$$
In France, the average wind is 2.4m/s.

## Underground temperature

At around 10m deep, the underground temperature is worth about the average air outdoor temperature ($\approx 13°$). For the sake of simplicity, we will assume that the other side of the slab is at the average air outdoor temperature.


