$$
S_{living}=100\\
S_{floor}=\frac{S_{living}}{n_{floor}}\\
L_{NS}=\sqrt{S_{floor}f_{shape}}=\sqrt{\frac{S_{living}f_{shape}}{n_{floor}}}\\
L_{EW}=\sqrt{\frac{S_{floor}}{f_{shape}}}=\sqrt{\frac{S_{living}}{n_{floor}f_{shape}}}\\
S_{NS}=h_{floor}n_{floor}\sqrt{S_{floor}f_{shape}}\\
S_{EW}=h_{floor}n_{floor}\sqrt{\frac{S_{floor}}{f_{shape}}}\\

S^w_N=h_{floor}n_{floor}\rho_N\sqrt{S_{floor}f_{shape}}=h_{floor}n_{floor}\rho_N\sqrt{\frac{S_{living}f_{shape}}{n_{floor}}}\\

S^w_S=h_{floor}n_{floor}\rho_S\sqrt{S_{floor}f_{shape}}=h_{floor}n_{floor}\rho_S\sqrt{\frac{S_{living}f_{shape}}{n_{floor}}}\\

S^w_{E}=h_{floor}n_{floor}\rho_E\sqrt{\frac{S_{floor}}{f_{shape}}}=h_{floor}n_{floor}\rho_E\sqrt{\frac{S_{living}}{f_{shape}n_{floor}}}\\

S^w_{W}=h_{floor}n_{floor}\rho_W\sqrt{\frac{S_{floor}}{f_{shape}}}\\

S_N=h_{floor}n_{floor}\sqrt{S_{floor}f_{shape}}(1-\rho_N)\\
S_S=h_{floor}n_{floor}\sqrt{S_{floor}f_{shape}}(1-\rho_S)\\
S_{E}=h_{floor}n_{floor}\sqrt{\frac{S_{floor}}{f_{shape}}}(1-\rho_E)\\
S_{W}=h_{floor}n_{floor}\sqrt{\frac{S_{floor}}{f_{shape}}}(1-\rho_W)\\
S_{wall}=h_{floor}n_{floor}\left(\left(\sqrt{S_{floor}f_{shape}}(2-\rho_N-\rho_S)\right)+\left(\sqrt{\frac{S_{floor}}{f_{shape}}}(2-\rho_E-\rho_W)\right) \right)
$$

$$
S^w_N=h_{floor}\rho_N\sqrt{n_{floor}S_{living}f_{shape}}\\

S^w_S=h_{floor}\rho_S\sqrt{n_{floor}S_{living}f_{shape}}\\

S^w_{E}=h_{floor}\rho_E\sqrt{\frac{n_{floor}S_{living}}{f_{shape}}}\\

S^w_{W}=h_{floor}\rho_W\sqrt{\frac{n_{floor}S_{living}}{f_{shape}}}\\
$$

$$
S_{wall}=h_{floor}\sqrt{n_{floor}S_{living}f_{shape}}(2-\rho_N-\rho_S)+h_{floor}\sqrt{\frac{n_{floor}S_{living}}{f_{shape}}}(2-\rho_E-\rho_W)
$$

$$
S^w_N=h_{floor}n_{floor}\rho_N\sqrt{\frac{S_{living}f_{shape}}{n_{floor}}}\\

S^w_S=h_{floor}n_{floor}\rho_S\sqrt{\frac{S_{living}f_{shape}}{n_{floor}}}\\

S^w_{E}=h_{floor}n_{floor}\rho_E\sqrt{\frac{S_{living}}{f_{shape}n_{floor}}}\\

S^w_{W}=h_{floor}n_{floor}\rho_W\sqrt{\frac{S_{living}}{f_{shape}n_{floor}}}\\

S_*^w=h_{floor}\left(\rho_N+\rho_S\right)\sqrt{S_{living}f_{shape}n_{floor}}+h_{floor}(\rho_W+\rho_E)\sqrt{\frac{S_{living}n_{floor}}{f_{shape}}}
$$

