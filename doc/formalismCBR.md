# Case base reasoning for generating advices without explicit model

## Formalism

Let's write $\{K_i,\ \forall i\in \mathcal K\}=\mathbb K$ ,  a set of cases where $K_i$ is a tuple of data corresponding to measurable variables and $\mathcal K$  the set of case indices.

Let $K_i^C=\Pi_C(K_i)$ be the projection of $K_i$  on the context space, and respectively $K_i^A=\Pi_A(K_i)$, $K_i^E=\Pi_E(K_i)$, and $K_i^{CA}=\Pi_{CA}(K_i)$ the projection of $K_i$ on the action, the effect space and the context/action space satisfying $\mathbb K \subset\Pi_C(\mathbb K)\times\Pi_A(\mathbb K)\times\Pi_E(\mathbb K)$  and $\Pi_{CA}(\mathbb K)\subset\Pi_C(\mathbb K)\times\Pi_A(\mathbb K)$.  The dimension of $K^C_i$, $K^A_i$ and $K^E_i$ are respectively $n_C$, $n_A$ and $n_E$. 

Let $p(Ki)\in [0,\infty[$ be the performance of $K_i$, where $\infty$​ stands for the best performance.

Shortly, the problem to solve consists in seeking, for a given context $K^C$, actions $K^A$ maximizing the performance $max_{A_i}\ p((K^C, K^A, E(K^C, K^A)))$ where $E(K^C, K^A)$ is the effect of $C_i, A_i$ on the system but $E(.)$ is not known and must be estimated: $E(K^C, K^A)\approx \hat E(K^C, K^A)$  using the case base $\mathbb K$.

$K^C_i$ is a vector of context data related to case $K_i$ as well as $K^A_i$ for the actions done and $K^E_i$​​ for the measured effect. A vector can contain values collected at different times but related to the same phenomenon (for instance, a room temperature over the hours in a day).

## About distance between cases and sub-cases

Because context and action data are both causes, sometimes it is useful to compute the distance between 2 contexts, between actions, and/or between combined context and action data. For the sake of simplicity, let's choose a separable distance $d(.)$ such as:
$$
d_{CA}(K^{CA}_i,K^{CA}_j)=d_C(K^{C}_i,K^{C}_j)+d_A(K^{A}_i,K^{A}_j)
$$
The weighted normalized Manhattan distance is satisfying the separability property: 
$$
d_{CA,w_k}(K^{CA}_i,K^{CA}_j)=\sum_k w_k \left|\frac{(K_i^{CA})_k-(\check K^{CA})_k}{(\hat K^{CA})_k - (\check K^{CA})_k}-\frac{(K_j^{CA})_k-(\check K^{CA})_k}{(\hat K^{CA})_k - (\check K^{CA})_k}\right|
$$
 where $\sum_k w_k = 1$; $(\check K^{CA})_k$ and $(\hat K^{CA})_k$ stand respectively for the lower and the upper bounds of the $k^{th}$ value of the tuple for all recorded cases i.e. $\forall i$. Variables are normalized to avoid the scale effects (non-zero minimal value like about 400 for $CO_2$ concentration and different nominal magnitudes, for instance, 20°C for ambiant temperature compared to few hundred ppm for $CO_2$​).

Regarding the effects E, we are going to use a sensitive distance $d_s$  defined by:
$$
d_{E,\sigma_k}(K^{E}_i,K^{E}_j)=\sum_k\frac{\left| (K^E_i)_k - (K^E_j)_k \right|[\sigma_k]}{\sigma_k}
$$
where $k$ stands for the $k^{th}$ variable of the effect space and $\sigma_k$ for the maximum variation of the $k^{th}$  variable that cannot be perceived by humans.

 The weights have to be determined knowing that for all cases $K_i$ and $K_j$ from the case base, we want:
$$
\forall i, \forall j>i, K_i=(K^{CA}_i,K^E_i)\in \mathbb K\text{ and }K_j=(K^{CA}_j,K^E_j)\in \mathbb K\\
\left(d_{CA,w_k}(K^{CA}_i,K^{CA}_j)\rightarrow 0\right) \rightarrow \left(d_{E,\sigma_k}(K^E_i,K^E_j)\rightarrow 0\right)
$$

It can be solved by selecting close CAs, $\Pi_{CA}(K_i)$ and $\Pi_{CA}(K_j)$, and then checking (4)​.

Some $K_i$​ might yield important distances because of failing sensors: removing these cases might yield important improvement in terms of accuracy for the solution.

## Problem statement

The overall aim is to find a local model $\hat E(K^C, K^A)$ in the neighborhood of a current context $C$:
$$
\mathcal V_\mathbb{K}(C)=\{K_i/d_w(K^C,\Pi_C(K_i))\le\delta_C;\forall K_i\in \mathbb K\}
$$
 valid for all $K^A$ belonging to:
$$
\mathcal A_C=\{K^A/d_{CA,w_k}(K^{CA},\Pi_{CA}(K_i))\le\delta_{CA};K_i\in \mathcal V_{\mathbb K}(C)\}
$$
It can be rewritten as:
$$
\mathcal A_C=\{K^A/d_{A,w_k}(K^A,\Pi_{A}(K_i))\le\delta_{CA}-d_{C,w_k}(K^C,\Pi_{C}(K_i));K_i\in \mathcal V_{\mathbb K}(C)\}
$$
If $\mathcal A_C(\delta_{CA})$ is not empty, the optimal actions maximize the performance:
$$
A^*=argmax_{K^A\in\mathcal A_C(\delta_{CA})}\ p(K^C, K^A, \hat E(K^{C},K^A))
$$
## Completeness assumption

To be able to infer effects from causes (context $C$ and actions $A$) in a base of cases $\mathbb K$, cases where causes are close must lead the close effects. 

### Global completeness

Let $K_i, K_j\in \mathbb K^2$. $\mathbb K$ is globally complete if: $\forall (K_i,K_j)\in\mathbb K^2 \left(d_{CA,w}(\Pi_{CA}(K_i),\Pi_{CA}(K_j))\rightarrow 0\right) \rightarrow \left(d_{E,\sigma}(\Pi_{E}(K_i),\Pi_{E}(K_j))\rightarrow 0\right)$​​

The problem can be solved by finding a bounding function:
$$
d_{E,\sigma}(\Pi_{E}(K_i),\Pi_{E}(K_j))\le \alpha (d_{CA,w}(\Pi_{CA}(K_i),\Pi_{CA}(K_j))+\epsilon)
$$
$\alpha$ can be found by searching for:
$$
\alpha(w) = argmax_{(K_i,K_j)\in\mathbb K^2}\left(
\frac{d_{E,\sigma}(\Pi_{E}(K_i),\Pi_{E}(K_j))}{d_{CA,w}(\Pi_{CA}(K_i),\Pi_{CA}(K_j))+\epsilon}
\right)
$$

The smaller $α(w)$, the complete is the base of cases. Note that $\alpha$ depends on the weighted CA-distance.

### Local completeness

If $\alpha$ is too large, it's possible to define a maximum acceptable value for $\alpha$ in a neighborhood $\mathcal V_\mathbb{K}(C)$  of the current context $C$ defined by a distance $d_w$ such as:
$$
\forall K_i, K_j / \left(\Pi_{C}(K_i),\Pi_{C}(K_j)\right)\in\mathcal V_\mathbb{K}(C)^2\\
\frac{d_{E,\sigma}(\Pi_{E}(K_i),\Pi_{E}(K_j))}{d_{CA,w}(\Pi_{CA}(K_i),\Pi_{CA}(K_j))+\epsilon}\le \alpha(w)
$$

### Computing a weighted distance

The weights of the CA distance must minimize the incompleteness i.e. 
$$
w^* = argmin_w \alpha(w)
$$

subject to $\sum_k w_k = 1$​ 

### calculate the barycentre of several action data for adaptation to current context

Let $K_i;i\in N$  be the cases located in the neighborhood of a context and $\left(\Pi_A(K_i), d_C(C,\Pi_C(K_i)), p(K_i)\right); i\in N$ be respectively, the action data, the distance with the current context, and $p(K_i)\in [0,\infty[$ the performance of the case $K_i$. i.e. the bigger the best.

A neighbor case is "all the more interesting when is distance $d_C(C,\Pi_C(K_i))$ is as small as possible and the performance $p(K_i)$  is as small as possible". Let's denote $\hat d_C = max_{i\in N}(d_C(C,\Pi_C(K_i)))$, the biggest distance to the current context. If  $\hat d_C\neq 0$, the weight $\omega_i$ for each case actions of the barycentric approach is directly drawn from the intentions in between double quotations:
$$
\omega_i = \frac{\hat d_C-d_C(C,\Pi_C(K_i))}{\hat d_C}\times p(K_i)
$$
Then, the suggested action is given by:
$$
(A)_i=\sum_i\frac{\omega_i}{\Omega}\left(\Pi_A(K_i)\right)_i
$$
If $\hat d_C=0$,  it means all  the neighbors have exactly the same context, so the most performant case ix selected.



