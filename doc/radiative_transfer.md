---
marp: true
---

# Radiative transfer

## Solid angle

<img src="radiative_transfer.assets/langfr-1920px-Steradian.svg.png" alt="Représentation d'un angle solide valant 1 stéradian." style="zoom:25%;" />

A solid angle is the apparent surface of remote  surface: for instance, a $9m^2$ surface  distant from 3m leads to the same solid angle than a $1m^2$​ surface located at 1m.

<img src="radiative_transfer.assets/image-20240428022746488.png" alt="image-20240428022746488" style="zoom:150%;" />

More formally, a solid angle $\Omega$ is the apparent surface directed by $\vec n$ at a unary distance of a surface $dxdy$ located at a distance $d$ along the direction $\vec u$:
$$
d\Omega=\frac{dxdy}{d^2}\vec u.\vec n=\frac{dxdy}{d^2}cos(angle(\vec u,\vec n))
$$
![image-20240402075236078](radiative_transfer.assets/image-20240402075236078.png)

A solid angle $\Omega\in[0,4\pi]$​​ is expressed in steradians (it is equal to $2\pi$ for an canonical hemisphere with a radius equal to 1 because the surface of a sphere is given by $4\pi r^2$). For a cone starting from the origine of $\vec u$, whose half angle at the vertex if $\alpha$, it leads to: $\Omega = 2\pi(1-\cos(\alpha))$​

## Wavelengths

![Electromagnetic Spectrum](radiative_transfer.assets/EMSpectrum.jpg)

Electro-magnetic wave length  perceived by humans:

- through eyes: $\lambda \in [380nm, 780nm]$
- through skin:  $\lambda \in [780nm, 0.314mm]$​​​​ 

Note: Some exposure to ultraviolet radiation is essential for good health, as it stimulates the production of vitamin D in the body but too much exposure might lead to skin cancer. Unfortunately, UVs are not perceived by humans.

Color temperature is a parameter describing the color of a visible light source by comparing it to the color of light emitted by an idealized opaque, non-reflective body called black body. The temperature of the ideal emitter that matches the color most closely is defined as the color temperature of the original visible light source. Color temperature is usually measured in Kelvins. The color temperature scale describes only the color of light emitted by a light source, which may actually be at a different (and often much lower) temperature.

![img](radiative_transfer.assets/PlanckianLocus.png)

The aim of photometry is to evaluate light radiation as perceived by human vision, and to study the transmission of this radiation from a quantitative point of view. 

Radiometry measures all electromagnetic waves (from γ-rays to radio waves), while photometry studies only human perception of waves, enabling visual perception.

**The intensity of radiation corresponds to the quantity of energy emitted by a surface per unit of solid angle directed by a direction $\vec u$ and per unit of emitting surface perpendicular to $\vec u$​​.**

![image-20240518113558656](radiative_transfer.assets/image-20240518113558656.png)



| phenomena (photometry / radiometry)                    | photometry                              | radiometry    | comment                                                      | symbols (radiometry) |
| ------------------------------------------------------ | --------------------------------------- | ------------- | ------------------------------------------------------------ | -------------------- |
| luminous flux / radiant flux                           | lumen (lm) or candela.steradian (cd.sr) | Watt          | total power emitted by a radiative source in all directions  | $\varphi$ or $\Phi$  |
| luminous / radiant intensity (in a specific direction) | candela (cd)                            | Watt/sr       | power intensity of a radiative source emitted in a  solid angle directed by $\vec u$ | $I$ or $\Omega$      |
| luminance / radiance                                   | candela/$m^2$ <br />(cd/$m^2$)          | Watt/sr/$m^2$ | power intensity in a specific direction $\vec u$ emitted by a unit surface | L                    |
| exitance / emittance                                   | $W.m^2/Hz$                              | Watt/$m^2$    | light radiated per unit of surface in all directions         | M (or E)             |
| illuminance / irradiance                               | lux (1lm/$m^2$)                         | Watt/$m^2$    | light received by a surface from all directions              | E                    |

The  difference between radiometry and photometry is that radiometry includes the entire optical radiation spectrum, while photometry is limited to the visible spectrum as defined by the response of the eye.

The next variables denoted like $X$ are radiometric variables while the photometric ones would be denoted $X_\lambda$ if used, when there are mono-chromatic, i.e. related to the specific wave length $\lambda$, and $X_\nu$ when they are related to the whole visible light spectrum. It satisfied: $X=\int X_\lambda d\lambda$ and $X_\nu=\int_\nu X_\lambda d\lambda$.

Additionally, irradiance expressed in $W/m^2$ becomes irradiations corresponding to $Wh/m^2$ when it deals with energy.

In photometry, the total power emitted by a source of surface $S$ in all directions is denoted $\varphi$ in Watt.

<img src="radiative_transfer.assets/mceclip0.png" alt="mceclip0" style="zoom:70%;" />

The different concepts related to radiometry are represented below:

![image-20240518095243144](radiative_transfer.assets/image-20240518095243144.png)

### Luminous (lumen) or power flux (Watt)

1 lumen (lm) corresponds to the luminous flux emitted within a solid angle of 1 steradian (sr) by an isotropic spot light source located at the summit of a solid angle, with a luminous intensity of 1 candela. We have 1 ln = 1 cd.sr.

In radiometry, it corresponds to the total power emitted by a surface .

<img src="radiative_transfer.assets/image-20240406021903031.png" alt="image-20240406021903031" style="zoom:33%;" />

### Luminous (Candela) or power intensity  (Watt/steradian)

The candela (cd) is the light intensity, in a given direction, of a source emitting monochromatic radiation of frequency $540.10^{12}Hz$  (555nm[^1 $\lambda=c/f$ where $c=299792458m/s$]) and whose energy intensity in this direction is 1⁄683 Watt/steradian.

The frequency chosen corresponds to a wavelength of 555 nm, in the green-yellow part of the visible spectrum, which is close to the human eye's maximum sensitivity to daylight.

The power emitted by a surface $S$ in the direction of a solid angle $d\Omega$ directed by $\vec u$ is denoted $d\varphi_\vec u$​.

The power intensity in a direction $\vec u$ is defined as the power emitted by a surface $S$ through a solid angle directed by $\vec u$ . It is denoted: $L_\vec u=\frac{d\varphi_{\vec u}}{d\Omega}$. We have $\varphi=\int_S d \varphi=\int_\Omega d\varphi_\vec u$.

### Luminance or radiance (Candela /$m^2$ or Watt/steradians/$m^2$)

Luminance or radiance (symbol $L$)  is the flux emitted, reflected, transmitted or received by a given surface, per unit solid angle per unit projected area. It is a quantity corresponding to the visual sensation of brightness of a surface. Luminance is the power of visible light passing through or being emitted from a surface element in a given direction, per unit area and per unit solid angle. 

The total power emitted by a surface $dS$ directed by $\vec n$ through a solid angle $d\Omega$  directed by $\vec u$ is denoted $d^2\varphi$ or $d^2\varphi_{\vec n,\vec u}$​

The power emitted by a surface $dS$ in all directions is denoted $d\varphi=\iint_\Omega d^2\varphi dS$​.

The power radiance is defined as the power intensity emitted by the apparente surface $dS_1$ in the direction $\vec u$ : 
$$
L_\vec u = \frac{d L_\vec u}{dS_\vec u}=\frac{d L_\vec u}{dS \cos(\alpha)}=\frac{d^2\varphi_\vec u}{d\Omega dS \cos(\alpha)}
$$
A source is isotrope if its luminance is independent of the direction: $L_{\vec u}=L$​​.

### Illuminance or irradiance (lux or Watt/$m^2$)

Illuminance (symbol $I$) is the total flux incident received by a surface, per unit area. It is a measure of how much the incident light illuminates the surface, wavelength-weighted by the luminosity function to correlate with human brightness perception in photometry. Irradiance is also named *Intensity of light* or *Power density* at a specific surface element $dS$. It is given by:
$$
I = \int_{2\pi}L_\vec u cos(\theta)
$$


<img src="radiative_transfer.assets/image-20240406045425557.png" alt="image-20240406045425557" style="zoom:50%;" /> 

The power illuminance or irradiance[^1 "éclairement" in French] is the symmetric of the emittance: it's the power received by a surface $dS_2$​  from all directions.

### Luminous radiant exitance / emittance (Watt/$m^2$)

The areal density of luminous flux emitted by a surface per unit area (in $W/m^2$) is named power exitance or emittance (symbol $E$). It is the power emitted in all directions by per unit of emitting surface : $E=\frac{d\varphi}{dS}$. It corresponds to the emission of radiated electro-magnetic power. To designate the radiated energy striking a surface, one speaks about illuminance (photometry or radiometry). However, there is no physical difference between these quantities, only the observer's point of view.

The radiant exitance of a black surface, according to the Stefan–Boltzmann law, is equal to: $E=\sigma T^4\approx 5.67 \left(\frac{T}{100} \right)^4$ where $T$ is expressed in Kelvins and $\sigma=5.670374419\ 10^{-8}W/m^2/K^4$​ is the  Stefan–Boltzmann constant.

<img src="radiative_transfer.assets/image-20240518082525468.png" alt="image-20240518082525468" style="zoom: 25%;" />

 For a real surface, the radiant exitance is equal to: $E=\epsilon \sigma T^4$ where $\epsilon$​​ is the emissivity of the emitting surface. 

Here are different emissivity values (most other values $\approx$ 0.9).

<img src="radiative_transfer.assets/image-20240428005608289.png" alt="image-20240428005608289" style="zoom: 33%;" />

## Radiation transfers between surfaces

![image-20240405065206008](radiative_transfer.assets/radiative.png)

We also have: $\frac{dI_\vec u}{dS}=L\cos(\alpha)$. 

Because the solid angle $\Omega$ corresponding to the cone defined by (O,$\vec u,\alpha$) is equal to $\Omega=2\pi(1-cos(\alpha))=\int_0^{\alpha}d\Omega(\alpha)=2\pi\int_0^{\alpha}sin(\alpha)d\alpha$  and $d\Omega(\alpha)=2\pi\sin(\alpha)d\alpha$, for an isotropic source, the emittance of $dS$ is:
$$
E=\iint \frac{dI_{\vec u}}{dS}d\Omega=2\pi\int_0^{\pi/2} L\cos(\alpha)\sin(\alpha)d\alpha\\
\rightarrow E=\pi L
$$


 The power $d^2\varphi_{\vec u}$ transmitted  by $dS_1$ with a luminance $L_\vec u$  to a surface $dS_2$ is given by:
$$
d^2\varphi_{\vec u}=I_\vec ud\Omega=L_\vec u\cos(\alpha_1)dS_1d\Omega=L_\vec u\frac{cos(\alpha_1)cos(\alpha_2)}{d^2}dS_1dS_2
$$
It' s the Bougouer formula.

![image-20240403070325237](radiative_transfer.assets/image-20240403070325237.png)

The radiations emitted by $dS_1$ on $dS_2$, and by $dS_2$ to $dS_1$, are then given by:
$$
d\varphi_{1\rightarrow 2}=L_{1\rightarrow 2}^{\vec u}\frac{cos(\alpha_1)cos(\alpha_2)}{d^2}dS_1dS_2\\
d\varphi_{2\rightarrow 1}=L_{2\rightarrow 1}^{\vec u}\frac{cos(\alpha_1)cos(\alpha_2)}{d^2}dS_1dS_2
$$
Because the sources are surfaces, we have: $L_{1\rightarrow 2}^{\vec u}=\frac{E_{1}}{\pi}=\frac{\sigma \epsilon_1 T_1^4}{\pi}$ and  $L_{2\rightarrow 1}^{\vec u}=\frac{E_{2}}{\pi}=\frac{\sigma \epsilon_2 T_2^4}{\pi}$​

Therefore (with $\sigma$, the Stefan-Boltzmann constant),
$$
\varphi_{1\rightarrow 2}=\frac{\sigma \epsilon_1 T_1^4}{\pi} \iint_{S_1,S_2}\frac{cos(\alpha_1)cos(\alpha_2)}{d^2}dS_1dS_2\\

\varphi_{2\rightarrow 1}=\frac{\sigma \epsilon_2 T_2^4}{\pi} \iint_{S_1,S_2}\frac{cos(\alpha_1)cos(\alpha_2)}{d^2}dS_1dS_2
$$
Let's introduce the total power emitted by the 2 surfaces: $E_1=\sigma \epsilon_1 S_1T_1^4$ and $E_2=\sigma \epsilon_2 S_2T_2^4$, 2 shape factors appears: $F_{1\rightarrow 2}$ and  $F_{2\rightarrow 1}$
$$
F_{1\rightarrow 2}=\frac{\varphi_{1\rightarrow 2}}{E_1}=\frac{1}{S_1\pi} \iint_{S_1,S_2}\frac{cos(\alpha_1)cos(\alpha_2)}{d^2}dS_1dS_2\\

F_{1\rightarrow 2}=\frac{\varphi_{2\rightarrow 1}}{E_2}=\frac{1}{S_2\pi} \iint_{S_1,S_2}\frac{cos(\alpha_1)cos(\alpha_2)}{d^2}dS_1dS_2
$$
with $S_1F_{1\rightarrow 2} =S_2F_{2\rightarrow 1}$​.

Shape factors can be computed for different configurations: they just depends on the geometry.

## Long wave transfer between a surface (a wall) and the ground or the sky (the celestial vault)

For the sky and the equivalent ground (see schema), we consider solid angles of one steradian .

Let's denote $h$, $l$, $s_{wall}$ and $\beta$ are respectively the height, the width, the surface and the slope of the wall .

![image-20240518213407769](radiative_transfer.assets/image-20240518213407769.png)
$$
d\varphi_{wall,sky}=(L_{wall}-L_{sky})\cos(\alpha) d\alpha d\omega dx dy
$$
with the bounds:
$$
x\in[0,h], y\in[0,l]\\

\alpha\in\left[\beta-\pi/2, \pi/2\right]\\

\omega = [0,\pi]
$$
Therefore,
$$
\varphi_{wall,sky}=(L_{wall}-L_{sky})\pi l\int_0^h\int_{\beta-\pi/2}^{\pi/2} cos(\alpha)d\alpha dx\\
\rightarrow
\varphi_{wall,sky}=(E_{wall}-E_{sky})\frac{1+\cos(\beta)}{2} s_{wall}
$$
Let's calculate the transfer between the wall and the ground:
$$
d\varphi_{wall,ground}=(L_{wall}-L_{ground})\cos(\alpha) d\alpha d\omega dx dy
$$
with the bounds:
$$
x\in[0,h], y\in[0,l]\\

\alpha\in\left[-\pi/2, \beta-\pi/2\right]\\

\omega = [0,\pi]
$$
Therefore,
$$
\varphi_{wall,ground}=(L_{wall}-L_{ground})\pi l\int_0^h\int_{-\pi/2}^{\beta-\pi/2} cos(\alpha)d\alpha dx\\
\rightarrow
\varphi_{w,g}=(E_{wall}-E_{ground})\frac{1-\cos(\beta)}{2} s_{wall}\\
$$
![image-20240519144321031](radiative_transfer.assets/image-20240519144321031.png)

Regarding the celestial vault, we know (Bertin and Martin, 1984):
$$
E_{clear}= 0.711+0.56\frac{T_{dp}}{100}+0.73\left(\frac{T_{dp}}{100}\right)^2 \\
E_{cloud}=\epsilon_{cloud} \sigma T_{cloud}^4 \\
E_{sky}=(1-\zeta)E_{clear} + \zeta E_{cloud}\\
\epsilon_{cloud}=0.96\\
T_{cloud}=T_{weather}-5
$$
where $\zeta\in [0, 100\%]$ is the nebulosity (or the cloud cover) and $T_{dp}$​ the dew pressure temperature at ground level.

Regarding the ground and the wall, we have:
$$
E_{wall}=\epsilon_{wall}\sigma T_{wall}^4\\
E_{ground}=\epsilon_{ground}\sigma T_{ground}^4
$$

- Particular case: wall facing the sky ($\beta = 0$)
    $$
    \varphi_{wall,sky}=(E_{wall}-E_{sky}) s_{wall}\\
    \varphi_{wall,ground}=0\\
    $$

- Particular case : vertical wall ($\beta=\pi/2$​)
    $$
    \varphi_{wall,sky}=\frac{E_{wall}-E_{sky}}{2}s_{wall}\\
    \varphi_{wall,ground}=\frac{E_{wall}-E_{ground}}{2}s_{wall}
    $$

## Radiative cooling during summer clear sky

The aim is to develop a radiative cooling system for the Grenette plazza in Grenoble: a day configuration and a night one to improve the comfort during summer periods.

- Day configuration

![image-20240421043242858](radiative_transfer.assets/image-20240421043242858.png)

The radiative long-wave radiations  exchanged between to sky and the PV panels during the day is neglected because (1) it is a small quantity of energy is comparing to the direct radiations and (2) the PV panel is made of glass... It means there is almost no reflected radiation at any angle. 

- night configuration



![image-20240421043610480](radiative_transfer.assets/image-20240421043610480.png)

- Explain the radiative cooling principle

### Emittence for a flat surface directed to the sky zenith

Accordingly to the previous section, the radiative power exchange between a surface   and the sky is:
$$
\varphi_{surface,sky}=S(E_{surface}-E_{sky})\\
E_{surface}=\epsilon_{surface}\sigma T_{surface}^4\\

E_{cloud}=\epsilon_{cloud} \sigma T_{cloud}^4 \\
E_{sky}=(1-\zeta)E_{clear} + \zeta E_{cloud}\\
\epsilon_{cloud}=0.96\\
T_{cloud}=T_{out}-5
$$
The aggregated formula is given by:
$$
\varphi_{surface,sky} =\epsilon_{surface}S\sigma T_{surface}^4+(\zeta-1)SE_{clear}-\zeta \epsilon_{cloud}S\sigma (T_{out}-5)^4
$$

Let's linearize $T_{surface}$ in the neighborhoods of $\bar T_{out}=\bar T$  leads to:
$$
\varphi_{surface\rightarrow sky}=

4\epsilon_{surface}S\sigma \bar T^3T_{surface} -3\epsilon_{surface}S\sigma \bar T^4\\

\dots+(\zeta-1)SE_{clear} -  \zeta S\epsilon_{cloud}\sigma (T_{out}-5)^4
$$
Let's isolate $T_{surface}$ in a thermal transmission term:
$$
\varphi_{surface\rightarrow sky}=
4\epsilon_{surface}S\sigma \bar T^3(T_{surface}-T_{out})\\ 
+4\epsilon_{surface}S\sigma \bar T^3T_{out}
-3\epsilon_{surface}S\sigma \bar T^4\\
-  \zeta S\epsilon_{cloud}\sigma (T_{out}-5)^4
+(\zeta-1)SE_{clear}(T_{out}) \\
$$


Moreover, there is a resistor modeling the convection between the surface of the  ground and the $T_{out}$. According to the official Th-Bat calculation method, for a wind speed $v$, the convective transfer between a surface and the outdoor temperature is equal to (in France, the average wind speed is 2.4m/s.):
$$
\varphi_{conv} = (11.4+5.7v)S(T_{ground}-T_{out})
$$

Here is a equivalent electric circuit:

![image-20240520185521252](radiative_transfer.assets/image-20240520185521252.png)
$$
T_{ground}-T_{out}=R_{rad}\Phi_{00}\\
T_{ground}-T_{out}=R_{conv}\Phi_{01}\\
\Phi+\Phi_1=\Phi_{00}+\Phi_{01}\\
\rightarrow \Phi =\Phi_1+\left(\frac{1}{R_{rad}}+\frac{1}{R_{conv}}\right)(T_{ground}-T_{out})
$$
Identifying the values lead to:

- $R_{rad}=\frac{1}{4s_{ground}\epsilon_{ground}\sigma \bar T^3}$

- $R_{conv}=\frac{1}{(11.4+5.7v)s_{ground}}$​

- $\Phi_1:$ it must be consider as negative because buildingenergy can just inject power
    $$
    \Phi_1=\zeta s_{ground}\epsilon_{cloud}\sigma (\bar T-5)^3(3\bar T+5)
    -3s_{ground}\epsilon_{ground}\sigma \bar T^4\dots \\
    
    \dots+(\zeta-1)s_{ground}\left(0.711+0.56\frac{T_{dp}}{100}+0.73\left(\frac{T_{dp}}{100}\right)^2\right) \\ 
    \dots+4s_{ground}\sigma (\epsilon_{ground}\bar T^3-\epsilon_{cloud}\zeta (\bar T-5)^3)T_{out}
    $$

The ground is modeled by:

<img src="radiative_transfer.assets/image-20240519191841917.png" alt="image-20240519191841917" style="zoom:30%;" />

### Underground temperature

At around 10m deep, the underground temperature is worth about the average air outdoor temperature ($\approx 13°$). For the sake of simplicity, we will assume that the other side of the slab is at the average air outdoor temperature.


## Model of the solar short wave radiations

![t](radiative_transfer.assets/Sonne_Strahlungsintensitaet.svg.png)

Mean sun-earth distance: 149,600,000 km

Sun equatorial radius: 696,000 km

Temperature of the photosphere: 5,772 K

Solar luminous power is about $7.10^{27}$​ lumens

Mean solar radiance: $2.009.10^7 W/m^2/sr$​

Mean total solar irradiance reaching earth atmosphere: $1367W/m^2$

Optical air mass $AMx = \frac{1}{cos(90°-altitude°)}$ with $AM_0$, the solar radiation at the surface of the atmosphere, $AM_1$, the solar radiation at elevation 0m (sea level) when the sun is at the vertical of the considered position and $AM_{1.5}$, the solar radiation when the sun is at an altitude of 41.8°, an average altitude considered as a reference for photovoltaic panel characterization[^1](A photovoltaic panel is characterized by its peak power for an irradiance of 1000W/m2 under AM1.5 condition. If its efficiency is 27% and its surface is 1.7mx1m, its peak power under AM1.5 is 459W)

Different irradiances use to be defined:

The **total solar irradiance** (TSI) is a measure of the solar power over all wavelengths per unit area incident on the Earth's upper atmosphere. It is measured perpendicular to the incoming sunlight.

The **direct normal irradiance** (DNI), or beam radiation, is measured at the surface of the Earth at a given location with a surface element perpendicular to the Sun direction. It excludes diffuse solar radiation (radiation that is scattered or reflected by atmospheric components). Direct irradiance is equal to the extraterrestrial irradiance above the atmosphere minus the atmospheric losses due to absorption and scattering. Losses depend on time of day (length of light's path through the atmosphere depending on the solar elevation angle), cloud cover, moisture content and other contents. The irradiance above the atmosphere also varies with time of year because the distance to the Sun varies, although this effect is generally less significant compared to the effect of losses on DNI.

The **diffuse horizontal irradiance** (DHI), or diffuse sky radiation is the radiation at the Earth's surface from light scattered by the atmosphere. It is measured on a horizontal surface with radiation coming from all points in the sky excluding circumsolar radiation i.e. radiation coming from the sun disk. There would be almost no DHI in the absence of atmosphere.

The **global horizontal irradiance** (GHI) is the total irradiance from the Sun on a horizontal surface on Earth. It is the sum of direct irradiance (after accounting for the solar zenith angle of the Sun z) and diffuse horizontal irradiance: GHI=DHI+DNI×cos(z)

The **global tilted irradiance** (GTI) is the total radiation received on a surface with defined tilt and azimuth, fixed or sun-tracking. GTI can be measured or modeled from GHI, DNI and DHI. It is often a reference for photovoltaic power plants, while photovoltaic modules are mounted on the fixed or tracking constructions.

The **global normal irradiance** (GNI) is the total irradiance from the Sun at the surface of Earth at a given location with a surface element perpendicular to the Sun.

The position of the sun is represented by 2 angles: the azimuth, the angle formed by a vertical plan directed to the south and the vertical plan where the sun is i.e. the azimuth angle, and the altitude (or elevation sometimes) of the sun formed by the horizontal plan tangent to earth and the horizontal where the sun is with 0° means: directed to the south (for azimuth, east is negative and west positive, and altitude 0° and 90° stand respectively for horizontal and vertical positions). 

![image-20240406071636102](radiative_transfer.assets/image-20240406071636102.png)

Every day, the earth receives a large amount of solar energy. The strength of this radiation reaching Earth depends on a number of factors, including weather conditions and atmospheric scattering (dispersion, reflection and absorption). At the sun's average distance from the earth, a surface normal to solar radiation (perpendicular to it) outside the atmosphere receives around $TSI=1367 W/m²$.  This irradiance is called the solar constant: the solar constant expresses the amount of solar energy that a surface of $1 m^2$​​​ at the average distance from the Earth to the Sun, exposed perpendicular to the sun beams, would receive in the absence of an atmosphere. For the Earth, this is the energy flux density at the top of the atmosphere. 

### Solar time

Administrative time differs from solar time: let's pass from one to another.

Let's first convert administrative time in the day to the universal time coordinated (UTC) expressed in seconds i.e. the world reference time at Greenwich meridian.

```python
utc_datetime = administrative_datetime.astimezone(utc)
utc_timetuple = utc_datetime.timetuple()
day_in_year = utc_timetuple.tm_yday

UTC_time_in_seconds = utc_timetuple.tm_hour * 3600 + utc_timetuple.tm_min * 60 + utc_timetuple.tm_sec
```

Then, the local solar time is deduced :

```python
local_solar_time_seconds = UTC_time_in_seconds + longitude_rad / (2*pi) * 24 * 3600
```

Time correction are applied to take into account seasonal variation of sun speed, eccentricity of sun trajectory and the inclination of the earth axis. 

The true solar day is the time that elapses between two consecutive passages of the center of the solar disk in the plane of the same terrestrial meridian.
The corresponding rotation of the Earth is called a synodic revolution. These true solar days are unequal for several reasons: mainly because of the variation of the Earth's speed on its orbit (2nd Kepler's Law) and because of the inclination of the of the ecliptic on the equator. The average solar day has a duration equal to the annual average of the duration of the true solar day. This duration has been fixed at 24 hours and is used to define the duration of the second: 1 second = 1/86400 mean solar day.

```python
time_correction_equation_seconds = 229.2 * (0.000075
+ 0.001868*cos(2*pi*day_in_year/365) 
- 0.032077*sin(2*pi*day_in_year/365) 
- 0.014615*cos(4*pi*day_in_year/365) 
- 0.04089*sin(4*pi*day_in_year/365))
        
actual_solartime_in_secondes = local_solar_time_seconds - time_correction_equation_seconds
```

![image-20240406121316116](radiative_transfer.assets/image-20240406121316116.png)  

### Solar angles

Given the earth's elliptical trajectory around the sun, with the greatest distance occurring on July 3rd at around $153.10^6km$ [^1  Do not mix up with solstice. The December solstice marked the sun’s southernmost path across our sky. At this solstice, Earth’s Southern Hemisphere is tilted most toward the sun. And so it’s summer now in that hemisphere. Meanwhile, the northern half of the globe is tilted most away from the sun at the December solstice. We typically say that winter begins at this solstice on our half of the globe.] and the smallest distance occurring on January 3rd at around $147.10^6km$, this constant varies by $\pm 3.4\%$ with a maximum in January at around 1413 W/m² and a minimum in June at around 1321 W/m². 

![undefined](radiative_transfer.assets/2880px-Seasons1.svg.png)

The total solar irradiance at the entrance of the atmosphere is given by:
$$
TSI = \bar L_{sun} \left(1 + 0.033 \cos\left(\frac{2\pi (1+\text{day\ in\ year)}}{365.25} \right)\right)\\
$$
![image-20240406131146870](radiative_transfer.assets/image-20240406131146870.png)

where $\bar L_{sun}$ stands for the average yearly radiance of the sun at the atmosphere external surface :
$$
\bar L_{sun}=\sigma T^4 \left(\frac{4\pi R^2}{4\pi d}\right)^2=1367W/m^2
$$
where $\sigma=5.67\ 10^{-8}W/m^2/K^4$ is the Stefan-Boltzmann constant, $R=696.10^6m$, the sun radius and $d=149.6\ 10^9m$​​ the average distance between the sun and Earth.

The axis of earth rotation about itself is inclined by a declination equal to 23.45° to the azimuthal axis. Azimuthal axis is the axis perpendicular to the plane intersecting the centers of sun and earth, called the azimuthal plane. This inclination causes the occurrence of seasons on earth.

![image-20240406074505940](radiative_transfer.assets/image-20240406074505940.png)

![image-20240406135609252](radiative_transfer.assets/image-20240406135609252.png)

![image-20240406135930089](radiative_transfer.assets/image-20240406135930089.png)

```
declination_rad = 23.45/180*pi*sin(2*pi*(day_in_year+284) / 365.25)
```

![image-20240406135336350](radiative_transfer.assets/image-20240406135336350.png)

The hour angle describes the seeming movement of the Sun around Earth. Its value shows the seeming place of the Sun towards east or west compared to the local meridian. Its value is negative in the morning and positive in the afternoon and 0 when the Sun is right on the meridian:

```python
solar_hour_angle_rad = 2*pi * (solartime_secondes / 3600 - 12) / 24
```

The height of the Sun above the horizon is given by the solar altitude. Solar altitude changes continuously, its value is 0 at sunrise and sunset while its maximum is reached at culminate. Culmination height is also different in every season:

```python
altitude_rad = max(0, asin(sin(declination_rad) * sin(latitude_rad) + cos(declination_rad) * cos(latitude_rad) * cos(solar_hour_angle_rad)))
```

The azimuth angle of the Sun is the angle between solar rays and the N–S direction of the earth. It is calculated based on the following equation for which we have to know geographical latitude declination angle, hour angle and solar altitude:

```python
cos_azimuth = (cos(declination_rad) * cos(solar_hour_angle_rad) * sin(latitude_rad) - sin(declination_rad) * cos(latitude_rad)) / cos(altitude_rad)
sin_azimuth = cos(declination_rad) * sin(solar_hour_angle_rad) / cos(altitude_rad)
azimuth_rad = atan2(sin_azimuth, cos_azimuth)
```



Plotting azimuth and altitude angles, at local position (latitude and longitude), the 21st of each month leads to the heliodon with the azimuth angles on the x-axis and the altitude angle on the y-axis:

![image-20240406071833301](radiative_transfer.assets/image-20240406071833301.png)

### Transmission of solar radiation

If the sun is not hidden by Earth, i.e. its altitude belongs to [0,180°], the total solar irradiance is affected by how solar beams spread out as it travels through the atmosphere. Thermosphere temperatures pass 2000°C due to solar radiation... it burns spatial dusts and meteors. The high temperature in the thermosphere is due to the absorption of high-energy solar radiation, primarily extreme ultraviolet. Unlike the lower atmosphere, the thermosphere is thin enough for extreme ultraviolet radiation from the Sun to pass through without being completely blocked. This high-energy radiation gets absorbed by the thermosphere's molecules, causing them to vibrate more intensely, translating to a significant rise in temperature. The thermosphere is the least dense layer of the atmosphere. While the temperature reading might seem very high, it's important to remember that temperature is a measure of the average kinetic energy of gas molecules. With so few molecules in the thermosphere, even a small amount of energy absorbed can result in a large increase in temperature per molecule. The temperature in the thermosphere isn't constant. It's heavily influenced by solar activity. During periods of high solar activity, with more extreme ultraviolet radiation being emitted, the thermosphere can become significantly hotter. This can cause the layer to expand upwards. In essence, the thermosphere acts like a super-sparse gas that gets energized by absorbing the Sun's high-energy rays.  This unique combination leads to the seemingly counterintuitive phenomenon of the hottest layer being at the very top of the atmosphere. Stratosphere includes the ozone layer (19-30 km): the temperature increases due to UV-absorption. Ozone acts like a shield, absorbing most of the sun's harmful ultraviolet B and ultraviolet C radiation.  Molecules and particles in the atmosphere, primarily in the stratosphere and mesosphere, can scatter sunlight.  This scattering is what causes the sky to appear blue.  Blue wavelengths are more easily scattered by these particles than other colors, which is why the sky appears predominantly blue.

![std-atmos-temperature-color](radiative_transfer.assets/std-atmos-temperature-color.png)

Passing through the atmosphere, ~30% solar radiation is reflected, ~17% is absorbed. Only 53% reaches the earth surface as direct radiation: ~31% and diffuse radiation: ~22%.

![image-20240409080837668](radiative_transfer.assets/image-20240409080837668.png)

The Rayleigh length is a distance used to characterize this phenomenon. It's the distance a beam travels from its narrowest point (beam waist) to the point where the beam's cross-sectional area has doubled. It is given by:

```python
rayleigh_length = 1 / (0.9 * relative_optical_air_mass + 9.4)
```

where the relative optical air mass is defined as the ratio of the air mass actually flown through to the air mass at vertical incidence: it depends on the solar altitude and on the ground pressure. If not available, the ground atmospheric pressure can be estimated with: $P_{atm}=101325(1-2.26\ 10^{-5}h)^{5.26}$  where $h$ stands for the local ground elevation. The relative optical air mass is given by:

```python
relative_optical_air_mass = ground_atmospheric_pressure / (101325 * sin(altitude) + 15198.75 * (3.885 + 57.296 * altitude)**(-1.253))
```

The transmitivity coefficient represents the fraction of the incident radiation  at the surface of the atmosphere that reaches the ground along a vertical path i.e.  that is not scattered.  

```python
transmitivity_coefficient = 0.6**Mh
```

where $M_h$ reflects the variation in atmospheric thickness as a function of the time of day. The Piedallu & Gégout formula is used:

```python
M0 = sqrt(1229 + (614*sin(altitude))**2) - 614*sin(altitude_rad)
Mh: float = (1-0.0065/288*elevation)**5.256 * M0
```

Mh is a correction factor linked to atmospheric pressure, which depends on elevation of the ground (particularly useful in mountainous areas).

The Linke factor models the disturbances due to steam, fog or dust. 

```python
l_Linke = 2.4 + 14.6 * pollution + 0.4 * (1 + 2 * pollution) * log(partial_steam_pressure)
```

where Pollution = 0.002 in mountains, 0.05 in countryside, 0.1 in urban area and 0.2 in highly polluted area. 

The fog is deduced from the partial steam pressure:

```python
partial_steam_pressure = 2.165 * (1.098 + temperature/100)**8.02 * humidity
```

where humidity and temperature can be obtained from the local meteorology.

Eventually, the direct normal irradiance is given by:

```python
direct_normal_irradiance = transmitivity_coefficient * normal_atmospheric_irradiance_with_clouds * exp(-relative_optical_air_mass * rayleigh_length * l_Linke)
```

which is affected by clouds:

```python
normal_atmospheric_irradiance_with_clouds = (1 - 0.75 * (nebulosity_percent/100) ** 3.4) * total_solar_irradiance
```

Nebulosity is the percentage of the sky covered by clouds. 

![image-20240409051306844](radiative_transfer.assets/image-20240409051306844.png)

### Incident surface

Let's introduce 2 new angles to model the direction of a flat solar collector lying on Earth: the exposure and the tilt angle.

![image-20240406071935707](radiative_transfer.assets/image-20240406071935707.png)

The incidence angle between the solar angle and an inclined surface, defined by an exposure angle and a tilt angle,  can be calculated with:

```python
incidence_rad = acos(cos(pi/2-altitude_rad) * cos(slope_rad) + sin(pi/2-altitude_rad) *  sin(slope_rad) * cos(azimuth_rad - exposure_rad))
```

###

In the direction of the sun, we get:

```python
direct_tilt_irradiance = cos(incidence_rad-pi) * direct_normal_irradiance
```

![img](radiative_transfer.assets/schema-EN_2400x1160.jpg)

Clouds diffuse a part of the solar beams in all directions:

```python
diffuse_tilt_irradiance = max(0, normal_atmospheric_irradiance_with_clouds * (0.271 - 0.294 * transmitivity_coefficient**Mh) * sin(altitude)) 
```

![image-20240409063919994](radiative_transfer.assets/image-20240409063919994.png)

```python
reflected_tilt_irradiance = max(0, albedo * normal_atmospheric_irradiance_with_clouds * (0.271 + 0.706 * transmitivity_coefficient**Mh) * sin(altitude) * sin(slope_rad / 2) ** 2)
```

### Reflection and absorption

Depending on the material, the wave length and the incident angle, a light beam can be transmitted, reflected or absorbed according respectively to transmission, reflection and absorption coefficients: $\alpha_t$, $\alpha_r$, $\alpha_a$  with $\alpha_t+\alpha_r+\alpha_a=1$ due to energy conservation.

A black body absorbs 100% of the incident beams. It emits more than any other body i.e. no transmission and no reflection.

A white body reflects all the incident radiations falling on it.

The radiance of a black body  depends on its temperature (see Plansk's law and Wien displacement law).

<img src="radiative_transfer.assets/energy-unit-area-second-wavelength-interval-temperatures.jpg" alt="img" style="zoom:50%;" />

A grey body absorbs a part of the incident beams, which depends on the temperature but not of the wave length

Most materials are considered as piece-wise grey bodies where pieces depends of wave length intervals.(for example, in the infra-red range

## Short wave and long wave radiation

The transmitivity of lights through glasses depends on the wave length.

![t](radiative_transfer.assets/glass_filtering.png)

Therefore, short wave radiations are converted into long wave radiations:

<img src="radiative_transfer.assets/image-20240428005320459.png" alt="image-20240428005320459" style="zoom:50%;" />

The transmitivity and the reflectivity of solar radiations through glass depends on the incident angle of the solar beam.

![image-20240428002147510](radiative_transfer.assets/image-20240428002147510.png)

The solar factor (SF) $\tau$ is defined by:

![image-20240428002615883](radiative_transfer.assets/image-20240428002615883.png)

The reflectance $\rho$ of the materials is given below (it comes from energy+).

| painting colors | reflectance |
| --------------- | ----------- |
| white           | 0.7 to 0.8  |
| yellow          | 0.5 to 0.7  |
| green           | 0.3 to 0.6  |
| grey            | 0.35 to 0.6 |
| brown           | 0.25 to 0.5 |
| blue            | 0.2 to 0.5  |
| red             | 0.3 to 0.35 |
| black           | 0.04        |

| woods               | reflectance  |
| ------------------- | ------------ |
| light birch, maple  | 0.55 to 0.65 |
| light varnished oak | 0.4 to 0.5   |
| dark varnished oak  | 0.15 to 0.4  |
| mahogany, walnut    | 0.15 to 0.4  |

| wallpapers                                     | reflectance  |
| ---------------------------------------------- | ------------ |
| very light (white, cream)                      | 0.65 to 0.75 |
| light (grey, yellow, blue)                     | 0.45 to 0.6  |
| dark (black, dark blue, dark grey, green, red) | 0.05 to 0.36 |

| construction materials   | reflectance  |
| ------------------------ | ------------ |
| white plaster            | 0.7 to 0.8   |
| clean white marble       | 0.8 to 0.85  |
| clean white brick        | 0.62         |
| red brick                | 0.1 to 0.2   |
| old red brick            | 0.05 to 0.15 |
| polished aluminium       | 0.6 to 0.75  |
| mate aluminium           | 0.55 to 0.6  |
| white enamel             | 0.65 to 0.75 |
| glazing                  | 0.08 to 0.4  |
| new white render (crépi) | 0.7 to 0.8   |
| old white render (crépi) | 0.3 to 0.6   |
| new concrete             | 0.4 to 0.5   |
| old concrete             | 0.05 to 0.15 |


## Let's now follow the Jupyter notebook07_photovoltaic.ipynb at the root of the buildingenergy project

Download the buildingenergy project and unzip it, or clone the project with:
    
### Getting the code

git clone https://gricad-gitlab.univ-grenoble-alpes.fr/ploixs/buildingenergy.git 

### Getting the required libraries

Launch a terminal in "Visual Studio code", the "Powershell" (Windows) or the "Terminal" (MacOS, Linux) application on your Operating System

Go inside the buildingenergy folder:

cd <PATH_TO_buildingenergy>

pip install -r requirements.txt

### some libraries might to install properly using the `requirements.txt` file

### Getting data

- If you are at ENSE3, modify the `setup.ini` file at code root folder by commenting (with a ';' in front) as follows:

[folders]
;data = ./../data/
data = //FILE-V04.e3.local/Pedagogie/buildingenergy/data/
results = ./results/
linreg = linreg/
sliding = sliding/
figures = figures/ 
[sizes]
width = 8
height = 8
[databases]
irise = irise38.sqlite3

- Otherwise, keep it as it is:

[folders]
data = ./../data/
;data = //FILE-V04.e3.local/Pedagogie/buildingenergy/data/
results = ./results/
linreg = linreg/
sliding = sliding/
figures = figures/ 
[sizes]
width = 8
height = 8
[databases]
irise = irise38.sqlite3

but download the data from https://cloud.univ-grenoble-alpes.fr/s/XzJnkg3NQLYskg5

Unzip and move the data at the same level than the root folder for code

- wherever you are, your file system should look like:

/your_containing_folder
    /data
    /buildingenergy
        /buildingenergy
        /doc
        /ecommunity
        /img
        /results
        /sites
        /slides
        notebook...
        ...
        requirements.txt
        setup.ini

        ________________________

________________________
Let's remind the buildingenergy's main features:
- automatic retrieval of historical weather data from anywhere in the world, as well as skyline-type solar masks
- calculation of solar gain on any surface, taking into account near, medium and far masks + PV production calculation
- parametric analysis of a typical house (lambda) immersed in a given environment, to get a quick idea of the design axes to be favored (but this is by no means a validation, as the inertia is fixed on an average case, as the following point shows)
- multi-zone dynamic energy simulation, taking into account air quality (CO2 concentration), with control and calculation of resulting performance, and taking into account the presence + design of curves (temperature setpoints, occupancy profiles). Compared with Pléiades, the descriptions are faster and it's more control/command-oriented, but although the results are very close in fine, Pléiades takes more elements into account: solar radiation on external walls and shading, illuminance (but not air quality). BuildingEnergy can be seen as a relatively early-stage tool, but does not dispense with validation via Pléiades, which is certified.
- model calibration for energy audits
- and many other tools
________________________
The buildingenergy installation procedure is as follows:

install Python + Visual Studio Code:

	https://youtu.be/cUAK4x_7thA?si=j6MN5pyvFkL94mQz

install Git:

	https://youtu.be/JgOs70Y7jew?si=36_H_6icnVIIGrU8

then go to  https://gricad-gitlab.univ-grenoble-alpes.fr/ploixs/buildingenergy and click on code / Open in IDE / Visual Studio Code (HTTPS)  

### Get the BuildingEnergy required libraries

Launch a terminal in "Visual Studio code" (Terminal toolbar), or "Powershell" on Windows / "Terminal" on MacOS, Linux

Go to the buildingenergy root directory, which contains another buildingenergy folder, the site directory, and more:
    cd <PATH_TO_buildingenergy>
    pip install -r requirements.txt

Some libraries may not install correctly using the `requirements.txt` file: you need to install them manually with:
pip install <missing_python library>

- If you are at ENSE3, modify the `setup.ini` file at code root folder by commenting (with a ';' in front) as follows:

[folders]
;data = ./../data/
data = //FILE-V04.e3.local/Pedagogie/buildingenergy/data/
results = ./results/
linreg = linreg/
sliding = sliding/
figures = figures/ 
properties = ./
[sizes]
width = 8
height = 8
[databases]
irise = irise38.sqlite3

- Otherwise, keep it as it is:

[folders]
data = ./../data/
;data = //FILE-V04.e3.local/Pedagogie/buildingenergy/data/
results = ./results/
linreg = linreg/
sliding = sliding/
figures = figures/ 
properties = ./
[sizes]
width = 8
height = 8
[databases]
irise = irise38.sqlite3

- your file system should look like this :

    /your_root_folder
        /buildingenergy
        /doc
        /ecommunity
        /img
        /results
        /sites
        /slides
        notebook...
        ...
        requirements.txt
        setup.ini

If you can't run a code under windows because the system doesn't see the Python files even though they exist, create a PYTHONPATH environment variable with the value "." (without quotes) from Control Panel / System
________________________
The code is constantly evolving. To update it, type in the Terminal window :
    git pull
If you've made changes to certain files, it won't want to update the code. In this case, you need to copy the modified files and then return to the repository state with :
    git reset --hard 
________________________
The basic concepts of thermic are summarized in:
	https://cghiaus.github.io/dm4bem_book/intro.html
You'll also find a wealth of documents in the project's doc/ directory.You'll also find a wealth of documents in the project's doc/ directory.

A user manual of the buildingenergy project used for simulation with control can be found in the doc folder of the project: it is named control_building.md, written in markdown format (use the All-In-One Markdown extension in Visual Studio Code, or an external markdown editor).
