# Thermal design

A thermal network is composed of temperature (with a pre-existing "Tref" node) and heatflow nodes connected by edges modeling either thermal resistances or a thermal capacitance. 
Each node has a causality. CAUSALITY can be "IN", meaning a value will be specified, or "OUT" when it must be simulated. Implicitly, if no causality is specified, CAUSALITY.UNDEFINED is considered meaning the causality with be defined during th e solving step. 

Create an thermal network:

```python
tn = thermal.ThermalNetwork()
```

Creating a temperature node, named "Tname" is done by:

```python
T = tn.T(name='name', causality=CAUSALITY.IN)  # or CAUSALITY.OUT, or CAUSALITY.UNDEF
```
If no name is specified, a name will be generated and returned: ```T### ``` where ### stands for a unique number. Implicitly, a temperature node is related to a ```Tref``` reference temperature.

Creating a heat node representing a heat flow injected at a temperature node T, with a name "Pname" is done by:
```python
P = tn.HEAT(T: str, name='name')  # or CAUSALITY.OUT, or CAUSALITY.UNDEF
```
A created heat node has always a causality ```CAUSALITY.IN```. If no name is specified, a name like "P###" will be generated and returned,  where ### stands for a unique number.

Thermal resistor can be created between 2 temperature nodes:
```python
resistor_name, heatflow_name = th.R(fromT: str, toT: str, name: str, heat_flow_name: str, value: float|str)  
````
where fromT and toT are the temperature names at resistor extremities. If one of these  names is omitted, it implicitly means it's worth "Tref". "name" can be used to name the resistor. If omitted, a unique name like "R###" is generated. A heatflow name can also be provided, if not, it is automatically generated like "P###". The resistance value can also be specified or alternatively a parameter name that will be set latter.

Thermal capacitor can be created between 2 temperature nodes:
```python
capacitor_name, heatflow_name = th.R(fromT: str, toT: str, name: str, heat_flow_name: str, value: float|str)  
````
where fromT and toT are the temperature names at resistor extremities. If one of these  names is omitted, it implicitly means it's worth "Tref". "name" can be used to name the capacitor. If omitted, a unique name like "C###" is generated. A heatflow name can also be provided, if not, it is automatically generated like "P###". The capacitance value can also be specified or alternatively a name or alternatively a parameter name that will be set latter.

Plotting the thermal network:
```python
tn.draw()
tn.show()
```

A static or dynamic state space model can be generated using:

```python
state_model = net.state_model()
print(state_model)
````

See for instance "thermal_h358.py" in the folder "./site".