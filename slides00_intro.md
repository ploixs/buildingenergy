---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **Energy and Buildings**
stephane.ploix@grenoble-inp.fr

---

<br/><br/><br/>
# **anthropocene**
## a (short) history of Earth

---

![bg fit](figs/history.png)

---

![bg fit](figs/evolution.png)

---

![bg fit](figs/density.png)

---

![bg fit](figs/crunch.png)

---

![bg fit](figs/world_data.png)

---

![bg fit](figs/flow_energy.png)

---

![bg fit](figs/thin_layer.png)

---

![bg fit](figs/CO2_in_air.png)

---

![bg fit](figs/correlation.png)

---

![bg fit](figs/social_index.png)

---

![bg fit](figs/europe.png)

---

![bg fit](figs/consumption-source-1990-2022.png)

---

![bg fit](figs/france-greenhouse-gas-emissions-1990-2021-sector.png)

---

![bg fit](figs/china_consumption.png)

---

![bg fit](figs/CO2_china.png)

---

![bg fit](figs/sankey.png)

---

![bg fit](figs/primary_final.png)

---

![bg fit](figs/CO2_year.png)

---

![bg fit](figs/infraday.png)

---

![bg fit](figs/elec_france.png)

---

![bg fit](figs/conso_elec.jpg)

---

![bg fit](figs/grid.png)

---

<br/><br/><br/>

# **Phenomena**
## Reminder (or not)

---

## Temperature
It's the __kinetic energie__ of particles:
![bg 50% right:30%](figs/Translational_motion.gif)
$$
T = \frac{1}{3k_B}\int (\vec{v}-\vec{U})^2p(\vec{v})d\vec{v}
$$
with $k_B$ the Boltzmann constant, $\vec{v}$ the speed of a particle, $p(\vec{v})$ the density of probability of particle speeds and $\vec{U}$ the average speed (meaningful only if particle density enough)
$$T_\text{Kelvin}=273.15+T_\text{Celsius} \qquad \left(T_\text{Celsius} = 32 + \frac{9}{5} T_\text{Fahrenheit}\right)$$

---

## Energy

Energy is the __quantitative property__ that is __transferred__ __during a given period of time__ to a physical system, recognizable in the __performance of work__ and in the form of __heat and light__.

The unit of energy is the __Joule__ in the International system of units, but in buildings, __kW.h__ is preferred (and not $kW/h$).

1 kWh is worth 3,600,000J (1 joule = 1 Watt/s)

1 Joule is equal to the __amount of work done__ when __a force of one newton displaces a mass through a distance of one metre__ in the direction of that force. 

---

Adjectvivs for diferent magnitudes:
![width:30cm](figs/magnitude.png)

---

With 1kWh, you can:

- lift 1 ton by 367m
- increase the temperature of 10l of water by 86°C.
- increase the air temperature of $72m^2\times 2.5m$ by 16°C.

A battery for a bike: up to 2kWh.
A battery for a car (Zoé/Tesla SPlaid): about 20kWh/100kWh
A vehicle fed with gasoline: 9l/100km, ie about 85kWh/100km
A same vehicle fed with diesel : 7,7l/100km, ie about 73kWh/100km
A same vehicle fed with electricity : about 24kWh/100km

---

![bg fit](figs/modes.png)

---

  Energy used for transportation

![bg fit](figs/transports.png)

---

Reference consumption RT2012 : 50kWh/m2/year (primary energy: 1kWh final electricity=2.58kWh primary energy)

![](figs/compteur.png)

---

![](figs/electrodomestiques.png)

---

![bg fit](figs/finale_sector1.png)

---

![bg fit](figs/finale_2021.png)

---

#### Power

Power is the amount of energy transferred or converted per unit time. 
$$
E_{t_1\rightarrow t_2} = \int_{t_1}^{t_2} P(t)dt \quad/\quad \forall t, \frac{dE(t)}{dt} = P(t)
$$
The unit of power is the Watt, equal to one joule per second.

An energy is a cumulated power during a given period.

---

1 biker: 200W 
1 desktop computer: 40W-120W + monitor: 20W
1 laptop: 15W/50W/80W
1 induction hot plate: 2000-3000W
1 oven: 2000-4000W
1 microwave: 1000W
1 fridge: 150-200W but 500kWh/year
1 freezer: 100-300W
1 dish washer: 150-200W
1 washing machine: 2000-3000W
1 clothe drier: 2500-3000W
1 TV: 40-100W
1 Internet box: 8-20W

---

1$m^2$ PV panel: 100W
1 household electricity subcription: 3-18kW
1 car: 80kW ie 400 bikers or 800$m^2$ PV
1 large windmill: 1MW
1 nuclear reactor: 1000MW

---

![bg fit](figs/power_vehicle.png)

---

A __thermal power flow transfers an agitation from a hot source (higher agitation) to a cold one (lower agitation)__ either by __contact__ or by __radiation__. 

Heat pump is the only way to collect heat from cold source and to heat a hot one (we will see it later)
