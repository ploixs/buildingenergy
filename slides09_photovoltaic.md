---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **The photovoltaic power**
stephane.ploix@grenoble-inp.fr

---

<img src="figs/energies_primaires.png" width="80%"/>

---

<img src="figs/enr.png" width="80%"/>

---

Hydro-power in France is distributed like:
- gravity dams: 55%
- Pumped-storage power stations (STEP): 18%
- run-of-river dams: 27%

Energy that is lost when it is not consumed is called "fatal energy".

---

**Question: What is the ratio of fatal energy in France? What will this number become with 100% of primary energy coming from renewables?**

Fatal energies: 
- run-of-river dams: 13.1% x 19,3% x 27% / 19,3% x 27%
- windmills: 13.1% x 12,7% / 12,7%
- photovoltaic: 13.1% x 4.2% / 4.2%
- thermal solar: 13.1% x 0.7% / 0.7%
- total: 3% / 22.8%

---
#### Production and consumption must be balanced (anytime)

<img src="figs/grid.png" width="80%"/>

---
### From primary to final energy 

1 unit of final electricity = 2.58 units of primary energy
<img src="figs/produc-conso.png" width="80%"/>
(c) datalab, 2018

---

![bg fit](figs/chiffres.png)
