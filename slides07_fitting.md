---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **Model fitting**
matching actual measurements

stephane.ploix@grenoble-inp.fr

---

## Introduction

A model of the H358 office has been developed from knowledge based on physics but it appears that it does not match perfectly the measurements. It can be explained by different facts:

- some phenomena, like the contribution of the boundary layers, are approximated or even neglected, like radiation exchange with the celestial vault
- some parameters, like conductivity, are not known with precision
- the model structure has been simplified, like dimensions of the office.

---

A first idea is to adjust the parameters of the model to better match the measurements. However, the optimization procedure minimizing the error between the model and the measurements is not easy because there are numerous parameters and the objective function is generally not convex and parameters might be impossible to identify. Moreover, the knowledge-based model is nonlinear in the parameters. This approach will be discussed at the end of the tutorial.

Alternatively, general more regular models, like ARX models, can be used. They are linear in the parameters and the optimization problem is easy to solve. Nonlinear extensions can also be considered like recurrent artificial neural networks, which relatively easy to tune their parameters.

---

We are going to compare linear ARX models, easy to tune and then, we will use knowledge-based models (the one we have elaborated in the previous tutorials), that can match more complex behaviors but because of their singularity, they are more difficult to tune.

A regular model requires a large dataset from which the model structure can be deduced but little expertise of the domain.

A knowledge-based model requires expertise of the domain but little dataset.

---

## ARX model

The main issue with regular models is to find a good model structure. Indeed, these models have little connection with the physics of the system: their best structure can't be deduced from the physics. If the complexity is not complex enough, the model will not be able to match the measurements. If it is too complex, the number of parameters will be so big that phenomena that can't be explained theoretically will be fitted. It is named __over-fitting__ because the none explainable phenomena, like random noise are fitted but they will be different for another set of measurements. Therefore, estimating the parameter values and determining the best structure is compromise between the highest precision and reproducibility.

---

An ARX model with an output $y$ ($y$ means measured value and $\hat y$ the related simulated value) and the following inputs $u_1,\dots, u_{\pi}$ can be written:

---

$$
\begin{align}
   \hat{y}(k) &= -\alpha_1 \hat{y}(k-1)-\dots-\alpha_n \hat{y}(k-n)\\
   &\dots + \beta_{1,\Delta_1} u_{1}(k-\Delta_1)+\dots+\beta_{1,m_1} u_{1}(k-m_1)\\
   &\dots + \dots \\
   &\dots + \beta_{\pi,\Delta_\pi} u_{\pi}(k-\Delta_\pi)+\dots+\beta_{\pi,m_\pi} u_{\pi}(k-m_\pi)+ \text{offset}
\end{align}
$$
where:
- $n$ is the maximum output delay
- $\Delta$ is the minimum input delay common to all inputs (it's modeling a time delay in the response)
- $m_j$ are the maximum input delay for each input
- $\pi$ is the number of inputs

---

The general form of an ARX model is:

$$
y(k) + a_1 y(k-1) + a_2 y(k-2) + \dots + a_n y(k-n) = b_0 u(k) + b_1 u(k-1) + \dots + b_m u(k-m)
$$

It is basically a multi-inputs and a single output (MISO) model. It can be rewritten as:

$$
y(k) = -a_1 y(k-1) - a_2 y(k-2) - \dots - a_n y(k-n) + b_0 u(k) + b_1 u(k-1) + \dots + b_m u(k-m)
$$

where: $y(k)$: output at time $k$, $u(k)$: input, at time $k$, with $a_i$, $b_i$: coefficients.

---

An ARX (Auto-Regressive with eXogenous input) filter can be transformed into a state-space representation by rewriting its difference equation into a state-space format. 

As seen before, a state-space model consists of:

$$
x(k+1) = A x(k) + B u(k)
$$

$$
y(k) = C x(k) + D u(k)
$$

Let's choose the state vector. The state vector $x(k)$ typically contains the delayed outputs $y(k-1), y(k-2), \dots, y(k-n)$ and potentially delayed inputs $u(k-1), u(k-2), \dots, u(k-m)$.

---

For simplicity, let:
$$
x(k) = \begin{bmatrix}
y(k-1) \\
y(k-2) \\
\vdots \\
y(k-n) \\
u(k-1) \\
\vdots \\
u(k-m)
\end{bmatrix}
$$

---

Using the ARX model structure, the next state $X(k+1)$ is a linear combination of the current states $X(k)$ and the current inputs $U(k)$.
$$
X(k+1) = A X(k) + B U(k)
$$
The matrix $A$ will capture the shift dynamics and the AR coefficients ($-a_i$), while $B$ will include the exogenous input dynamics ($b_i$).

The output $Y(k)$ depends on the state vector $X(k)$ and the current input $U(k)$:
$$
Y(k) = C X(k) + D U(k)
$$

- $C$ extracts the current output from the state vector and $D$ accounts for direct input-output relationship ($b_0$).

---

Let's exemplify this by an example. For an ARX model of order $n=2$, $m=1$:

$$
y(k) = -a_1 y(k-1) - a_2 y(k-2) + b_0 u(k) + b_1 u(k-1)
$$

To gather terms in $k$, let $z(k) = y(k)-b_0 u(k)$. A canonical state vector is given by:

$$
X(k) = \begin{bmatrix}
z(k) \\
z(k-1)
\end{bmatrix}
$$
The related input vector is:
$$
U(k) = \begin{bmatrix}
u(k) \\
u(k-1)
\end{bmatrix}
$$

---

The recurrent state equation is:

$$
\begin{bmatrix}
z(k+1)\\
z(k)\\
\end{bmatrix}
=
\begin{bmatrix}
-a_1 & -a_2\\
1 & 0\\
\end{bmatrix}
\begin{bmatrix}
z(k)\\
z(k-1)\\
\end{bmatrix}
+
\begin{bmatrix}
b_1-a_1b_0 & a_2b_0\\
0 & 0\\
\end{bmatrix}
\begin{bmatrix}
u(k)\\
u(k-1)\\
\end{bmatrix}
$$

and the output equation is:
$$
\begin{bmatrix}
y(k)
\end{bmatrix}
=
\begin{bmatrix}
1 & 0\\
\end{bmatrix}
\begin{bmatrix}
z(k)\\
z(k-1)\\
\end{bmatrix}
+
\begin{bmatrix}
b_0 & 0
\end{bmatrix}
\begin{bmatrix}
u(k)\\
u(k-1)\\
\end{bmatrix}
$$

Note that the state model is nonlinear in the parameters: the ARX model is more interesting because the parameters can be obtained from least squares inversion.

---


This approach generalizes to higher orders by expanding the state vector and appropriately defining $A$, $B$, $C$ and $D$.

![height:12cm](figs/RLNN.png)

---

An ARX model corresponds to a particular __recurrent linear neural network (RLNN)__. Since the ARX model is fundamentally a linear system, an RLNN can represent it exactly by using a linear activation function and appropriately structuring its weights and biases to match the ARX model’s dynamics.

An RLNN corresponding to the ARX model would involve a single layer of recurrence with linear activation functions. It maps to the ARX structure as follows:

---

Let's define a state vector $x(k)$ to hold the lagged values of $y(k)$ (output) and $u(k)$ (input):
$$
x(k) = \begin{bmatrix}
y(k-1) \\
y(k-2) \\
\vdots \\
y(k-n) \\
u(k-1) \\
u(k-2) \\
\vdots \\
u(k-m)
\end{bmatrix}
$$

---

The recurrence of the RLNN is linear and can be written as:
$$
x(k+1) = A x(k) + B u(k)
$$
where $A$ and $B$ are weight matrices designed to encode the ARX dynamics.

The output $y(k)$ is obtained as a linear combination of the state:
$$
y(k) = C x(k) + D u(k)
$$
where $C$ and $D$ are weight matrices representing the coefficients of the ARX model.

---

Let's now construct the RLNN that matches the ARX model.

Matrices are weight matrices:
  - The $A$ matrix captures the auto-regressive part ($a_i$ coefficients).
  - The $B$ matrix captures the exogenous input dynamics ($b_i$ coefficients).
  - The $C$ matrix extracts the lagged outputs to produce $y(k)$.
  - The $D$ matrix maps the direct influence of the current input $u(k)$.

---

For an ARX model of order $n = 2$, $m = 1$:
$$
y(k) = -a_1 y(k-1) - a_2 y(k-2) + b_0 u(k) + b_1 u(k-1)
$$

The RLNN parameters are:

$$
   x(k) = \begin{bmatrix}
   y(k-1) \\
   y(k-2) \\
   u(k-1)
   \end{bmatrix}
$$
$$
   A = \begin{bmatrix}
   -a_1 & -a_2 & b_1 \\
   1 & 0 & 0 \\
   0 & 0 & 0
   \end{bmatrix}, \quad
   B = \begin{bmatrix}
   b_0 \\
   0 \\
   0
   \end{bmatrix}, \quad
   C = \begin{bmatrix}
   1 & 0 & 0
   \end{bmatrix}, \quad
   D = \begin{bmatrix}
   b_0
   \end{bmatrix}.
$$

---

The RLNN uses a linear activation function (identity), which ensures the system behaves like an ARX model.

The RLNN exactly replicates the ARX dynamics when its weights are set to the ARX coefficients.

The recurrence is purely linear, corresponding to the lagged variables in the ARX model.

---

**Remarks**

- Translating ARX models into neural network frameworks for compatibility with machine learning systems.
- Combining with nonlinear extensions to create hybrid models (e.g., combining ARX and nonlinear RNNs).

- The RLNN is limited to linear dynamics, like the ARX model.
- It lacks flexibility for capturing nonlinear or unknown dynamics unless extended.

---

## Parameter estimation

A parameter estimation problem based on an ARX model is solved by minimizing the error between the model and the measurements. The error is typically the mean square error (MSE) between the model output and the measurements. It's done automatically but the most difficult task is to find the best model structure i.e. the order $n$ and $m$ that leads to the best precision (small MSE) while avoiding over-fitting to ensure reproducibility.


The process follows these steps:

---

- take a dataset of measurements and split it into a training set and a validation set (50%/50% or 70%/30%)
- Choose a model structure
- Adjust the parameters of the structure by minimizing the MSE of the training data set between the model output and the measurements, thanks to a least squares algorithm
- Note down the precision and check whether the precision is about the same with the validation data set
- If the precision is not good enough, increase the model order and iterate using indicators to modify the model structure
- If the precision is good enough, select the model structure and the parameters

---

## Indicators

### The distribution of the MSE

![height:12cm](figs/output_error.png)

---

If the distribution of the MSE is not well balanced like a Gaussian distribution, it suggests that the model structure is not appropriate or that spectrum of the input data is poor in terms of frequencies.

---

### Trend analysis

![height:12cm](figs/ressemblance.png)
---

Theoretically, the residuals should be white noise. If it is not, it suggests that the model structure is not appropriate: if an input signal appears in the residuals, it suggests that this input is not modeled and should be included in the model structure, or the order of the related numerator $m_i$ should be increased (see further to know by how much).

---

### zeros-poles analysis

![height:12cm](figs/zeros_poles.png)

---

This figure represents the zeros and poles of the transfer function of the ARX model. The zeros are represented by "x" and the poles by "o". There is several diagrams because the ARX model has several inputs. The poles are similar in all diagrams, but the zeros are different. If poles are compensated by zeros in all plots, it suggests that the order of both the numerator and the denominator can be decreased at least by 1: $n\rightarrow n-1$ and $m_i\rightarrow m_i-1$ for each input $i$.

---

### Auto-correlation of the residuals

![height:12cm](figs/auto_correlation.png)

---

Theoretically, the auto-correlation of the residuals should look like the one of the white noise i.e. a peak at lag 0 and then no correlation. If it is not, it suggests that the model structure is not appropriate or that the input data is not sufficient to identify the system or, if a spike appears at lag $l$, the order of the denominator $n$ should be increased to include $l$.

---

### Cross-correlation between inputs and residuals

![height:12cm](figs/cross_correlation.png)

---

If spikes appear in the cross-correlation between inputs and residuals at lag $l$, it suggests that the model structure is not appropriate or that the input  $i$ is not sufficiently important: the order of the numerator $m_i$ should be increased to include $l$. Indeed, for a white noise MSE, the cross-correlation should be zero at each lag.

---

### Over-fitting test

![height:12cm](figs/overfitting.png)

---

It shows both the output coming from the training and the validation data sets. If the validation data set is not well matched, it suggests that the model structure is not appropriate or that the order of the model is too high. The error should be the same for both data sets.

---

### Akaike test

The **Akaike Information Criterion (AIC)** is a widely used metric in statistics and machine learning for **model selection**. It provides a way to balance the goodness-of-fit of a statistical model with its complexity. When estimating parameters in models, the AIC helps decide which model is most likely to explain the data while avoiding over-fitting.

On the one hand, it measures how well the model explains the observed data i.e the precision (MSE,...). On the other hand, the model complexity refers to the number of parameters in the model. More parameters generally lead to better fit but may cause over-fitting, reducing the model's generalizability.

---

The AIC penalizes models with more parameters to prevent over-fitting. This makes it a tool to balance complexity and fit.

The AIC is calculated as:
$$
\text{AIC} = 2k - 2\ln(L)
$$
where:

- $k$ is the number of parameters in the model.
- $L$ is the maximum likelihood of the model (the precision).

A lower AIC values indicate a better model. When comparing models, the one with the smallest AIC is preferred.

---

When estimating parameters for a family of models, you can use the AIC to determine which model (and its associated parameter set) is most appropriate. By systematically varying parameters or comparing different models, the AIC helps identify the model that achieves the optimal trade-off between fit and complexity.

---

Example:

Suppose you have several models to explain a dataset, each with different numbers of parameters:

1. Model A (linear regression with 2 parameters).
2. Model B (quadratic regression with 3 parameters).
3. Model C (cubic regression with 4 parameters).

---

After fitting these models to the data and calculating their likelihoods, you compute the AIC for each:

- Model A: $\text{AIC}_A$
- Model B: $\text{AIC}_B$
- Model C: $\text{AIC}_C$

If $\text{AIC}_B $is the smallest, it suggests that the quadratic regression is the best balance between fit and simplicity.

Note that AIC values are relative, so they are meaningful only when comparing models fitted to the same dataset.

---

An Akaike plot is a visual tool used in the context of model selection and parameter estimation to help compare the relative performance of multiple models using their Akaike Information Criterion (AIC) values. The plot helps to visually identify which model best balances precision and model complexity, as well as to compare how different models rank against each other.

---

The x-axis represents the models being compared. These are typically indexed (e.g., Model 1, Model 2, etc.) or labeled with their descriptive names.

The y-axis shows the AIC values for each model.

Each model's AIC value is plotted as a point or represented as a bar, making it easy to see the relative differences in AIC values across models.

---

## Tuning of the knowledge model

The process is similar to the one of the ARX model but it is simplified because the model structure is already known:

- take a dataset of measurements and split it into a training set and a validation set (50%/50% or 70%/30%)
- select the most impacting parameters for adjustment and determine a realistic range of variation for each parameter

---

- Adjust the parameters of the structure by minimizing the MSE of the training data set between the model output and the measurements, thanks to a least squares algorithm (a greedy algorithm searching around calculated parameter values use to operate better than an exploratory search)
- check if bounds are reached for some parameters: if yes, the parameter range should be increased or the parameter should be fixed because it is not identifiable.
- Note down the precision and check whether the precision is about the same with the validation data set
- If the precision is good enough, save the parameters

---

## Determining most important parameters with Morris method

In order to determine the most important parameters, we are going to use the global sensitivity analysis method named: the Morris method, also known as the Elementary Effects Method. It is a fast global sensitivity analysis technique (Sobol approach is a more computationally demanding approach) used to evaluate the influence of parameter on the output of a model.

---

It is a screening Method that identifies key parameters (factors) that significantly affect the output and those that can be considered negligible. It’s computationally efficient and suitable for high-dimensional models with many parameters.

It can capture both nonlinear relationships between parameters and outputs and interactions among input variables.

parameters are varied one at a time while keeping others fixed to assess their individual effects on the output.

---

The steps in the Morris Method are:

- Input Space Sampling
   - Define the parameters ($X_1, X_2, ..., X_k$) and their ranges.
   - Divide each parameter range into discrete levels (e.g., 5–10 levels).
- Generation of Trajectories
   - Create a number of random paths (or trajectories) through the input space.
   - In each trajectory, vary one input variable at a time while keeping the others fixed.

---

- Compute Elementary Effects (EE):
   - For each input variable in a trajectory, calculate its elementary effect:
     $$
     EE_i = \frac{f(X_1, ..., X_i + \Delta, ..., X_k) - f(X_1, ..., X_i, ..., X_k)}{\Delta}
     $$
     where:
     - $f(\cdot)$: the model output.
     - $\Delta$: a small step size (change in parameters).

---

- Statistical Aggregation:
   - For each input variable, compute:
     - Mean ($\mu$) of the absolute values of the EEs:
       - Indicates the overall influence of the variable on the output.
     - Standard Deviation ($\sigma$) of the EEs:
       - Captures the variability of the effect, indicating nonlinearities or interactions.
     - Mean of Signed Effects ($\mu^*$):
       - Indicates whether the effect is consistently positive or negative.

---

Interpretation of the Results:
- High $\mu$: The parameter has a strong overall effect on the model output.
- High $\sigma$: It indicates significant nonlinear effects or interactions with other variables.
- Low $mu$ and $\sigma$: The parameter has negligible influence on the output.
