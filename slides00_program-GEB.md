---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **Energy and Buildings**
stephane.ploix@grenoble-inp.fr, benoit.delinchant@grenoble-inp.fr

---

# program

- session 1: model of solar radiation and Earth atmosphere
- session 2: model of climate evolution
- session 3: model of human impact and comfort
- session 4: phenomena impacting dwellings: $\lambda$-house and reality
- session 5: modeling buildings from physics
- session 6: modeling dynamics of buildings
- session 7: fitting model to data
- session 8: strategies for energy management in buildings
- session 9 & 10: building simulation with Pleiades
