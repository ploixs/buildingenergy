import unittest
import buildingenergy.weather
import buildingenergy.solar
import matplotlib.pyplot as plt
from scipy.optimize import minimize, differential_evolution
import datetime, time
import pytz
from math import pi
from buildingenergy.library import SLOPES, DIRECTIONS_SREF
import numpy
from matplotlib import cm
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import buildingenergy
import buildingenergy.timemg
import buildingenergy.solar
import buildingenergy.weather

tz = pytz.timezone('Europe/Paris')

# parameter adjustment for estimation of solar radiations
site_weather_data: buildingenergy.weather.SiteWeatherData = buildingenergy.weather.WeatherJsonReader(location='Grenoble', from_requested_stringdate="1/01/2023", to_requested_stringdate="31/12/2023", latitude_north_deg=45.19154994547585, longitude_east_deg=5.722065312331381).site_weather_data
dts_optim = site_weather_data.datetimes
cloudiness = site_weather_data('cloudiness')
temperature = site_weather_data('temperature')
relative_humidity = site_weather_data('humidity')
dnis_measured = site_weather_data('direct_normal_irradiance')
dhis_measured = [_ for _ in site_weather_data('diffuse_radiation')]
parameters = buildingenergy.solar.SolarModel.Parameters()


class TestingSolar(unittest.TestCase):

    def test_solar_model(self):
        site_weather_data: buildingenergy.weather.SiteWeatherData = buildingenergy.weather.WeatherJsonReader(location='Grenoble', from_requested_stringdate="1/01/2023", to_requested_stringdate="1/01/2023", latitude_north_deg=45.19154994547585, longitude_east_deg=5.722065312331381).site_weather_data
        solar_model = buildingenergy.solar.SolarModel(site_weather_data)
        dt1: datetime.datetime = tz.localize(datetime.datetime(2023, 1, 1, 0, 0, 0))
        dt2: datetime.datetime = tz.localize(datetime.datetime(2022, 12, 31, 23, 0, 0))
        self.assertEqual(solar_model._day_in_year(dt1), 0)
        self.assertEqual(solar_model._day_in_year(dt2), 0)
        solar_daytime_secs1: tuple[int, int] = solar_model._daytime_sec(dt1)
        solar_daytime_secs2: tuple[int, int] = solar_model._daytime_sec(dt2)
        self.assertLessEqual(solar_daytime_secs1, 24*3600)
        self.assertLessEqual(solar_daytime_secs2, 24*3600)

        self.assertEqual(solar_model._daytime_sec(dt1) - solar_model._daytime_sec(dt2), 3600)

        self.assertAlmostEqual(solar_model._daytime_sec(dt1), solar_daytime_secs1)
        self.assertAlmostEqual(solar_model._daytime_sec(dt2), solar_daytime_secs2)
        
        dts: list[datetime.datetime] = [tz.localize(datetime.datetime(2023, 1, 1, h, m, 0)) for h in range(24) for m in range(60)]
        altitudes_deg = list()
        azimuths_deg = list()
        fig, axes = plt.subplots(2, 1)
        for dt in dts:
            angles = solar_model.instant_angles_rad(dt)
            altitudes_deg.append(angles['altitude_rad']/pi*180)
            azimuths_deg.append(angles['azimuth_rad']/pi*180)
        axes[0].plot(dts, altitudes_deg, dts, azimuths_deg)

        dts: list[datetime.datetime] = [tz.localize(datetime.datetime(2023, 1, d, h, 0, 0)) for d in range(1, 32) for h in range(24)]
        site_weather_data: buildingenergy.weather.SiteWeatherData = buildingenergy.weather.WeatherJsonReader(location='Grenoble', from_requested_stringdate="1/01/2023", to_requested_stringdate="31/01/2023", latitude_north_deg=45.19154994547585, longitude_east_deg=5.722065312331381).site_weather_data
        cloudiness_percent: list[float] = site_weather_data('cloudiness')
        temperature_celsius: list[float] = site_weather_data('temperature')
        humidity_percent: list[float] = site_weather_data('humidity')
        dnis = list()
        dhis = list()
        rhis = list()
        tsis = list()
        ghis = list()
        tilts_0 = list()
        tilts_180 = list()
        tilts_90_south = list()
        tilts_90_west = list()
        tilts_90_north = list()
        tilts_90_east = list()
        for h, dt in enumerate(dts):
            irradiances = solar_model.instant_canonic_irradiances_W(dt, cloudiness_percent[h], temperature_celsius[h], humidity_percent[h])
            dnis.append(irradiances['dni'])
            dhis.append(irradiances['dhi'])
            rhis.append(irradiances['rhi'])
            tsis.append(irradiances['tsi'])
            ghis.append(irradiances['ghi'])
            tilts_180.append(solar_model.instant_tilt_irradiances_W(dt, cloudiness_percent[h], temperature_celsius[h], humidity_percent[h], slope_deg=180, exposure_deg=0))
            tilts_0.append(solar_model.instant_tilt_irradiances_W(dt, cloudiness_percent[h], temperature_celsius[h], humidity_percent[h], slope_deg=0, exposure_deg=0))
            tilts_90_south.append(solar_model.instant_tilt_irradiances_W(dt, cloudiness_percent[h], temperature_celsius[h], humidity_percent[h], slope_deg=90, exposure_deg=0))
            tilts_90_west.append(solar_model.instant_tilt_irradiances_W(dt, cloudiness_percent[h], temperature_celsius[h], humidity_percent[h], slope_deg=90, exposure_deg=90))
            tilts_90_north.append(solar_model.instant_tilt_irradiances_W(dt, cloudiness_percent[h], temperature_celsius[h], humidity_percent[h], slope_deg=90, exposure_deg=180))
            tilts_90_east.append(solar_model.instant_tilt_irradiances_W(dt, cloudiness_percent[h], temperature_celsius[h], humidity_percent[h], slope_deg=90, exposure_deg=-90))

        axes[1].plot(dts, dnis, dts, dhis, dts, rhis, dts, ghis, dts, site_weather_data('direct_normal_irradiance'), dts, site_weather_data('diffuse_radiation'))
        axes[1].legend(('dni', 'dhi', 'rhi', 'ghi', 'direct_normal_irradiance', 'direct_radiation', 'diffuse_radiation'))

        fig, axes = plt.subplots(3, 1)
        axes[0].plot(dts, tilts_0, dts, tilts_180)
        axes[0].legend(('tilt 0', 'tilt_180'))

        axes[1].plot(dts, tilts_90_south, dts, tilts_90_west, dts, tilts_90_north, dts, tilts_90_east)
        axes[1].legend(('south', 'west', 'north', 'east'))

        axes[2].plot(dts, ghis, dts, dhis)
        axes[2].legend(('ghi', 'dhi'))
        solar_model.plot_heliodon(2023)
        solar_model.plot_angles()

        cardinal_irradiances_W = solar_model.cardinal_irradiances_W
        best_exposure_deg, best_slope_deg = solar_model.best_angles()
        print(f'Best exposure: {best_exposure_deg}°, best slope: {best_slope_deg}°')
        # for d in cardinal_irradiances_W:
        #     print(d.name, sum(cardinal_irradiances_W[d]))
        self.assertGreater(sum(solar_model.irradiances_W(best_exposure_deg, best_slope_deg)), sum(cardinal_irradiances_W[DIRECTIONS_SREF.SOUTH]))
        self.assertGreater(sum(cardinal_irradiances_W[DIRECTIONS_SREF.SOUTH]), sum(cardinal_irradiances_W[DIRECTIONS_SREF.WEST]))
        self.assertGreater(sum(cardinal_irradiances_W[DIRECTIONS_SREF.SOUTH]), sum(cardinal_irradiances_W[DIRECTIONS_SREF.EAST]))
        self.assertGreater(sum(cardinal_irradiances_W[DIRECTIONS_SREF.WEST]),  sum(cardinal_irradiances_W[DIRECTIONS_SREF.NORTH]))
        self.assertGreater(sum(cardinal_irradiances_W[DIRECTIONS_SREF.EAST]),  sum(cardinal_irradiances_W[DIRECTIONS_SREF.NORTH]))
        self.assertGreater(sum(cardinal_irradiances_W[SLOPES.HORIZONTAL_UP]),  sum(cardinal_irradiances_W[SLOPES.HORIZONTAL_DOWN]))
        print('best_slope:', best_slope_deg, 'best_exposure', best_exposure_deg, '\nyear production', sum(solar_model.irradiances_W(best_exposure_deg, best_slope_deg)))
        for d in cardinal_irradiances_W:
            print(d.name, sum(cardinal_irradiances_W[d]))
        solar_model.try_export()
        self.assertAlmostEqual(sum(solar_model.ghi), sum(solar_model.irradiances_W(DIRECTIONS_SREF.SOUTH.value, SLOPES.HORIZONTAL_UP.value)))
        solar_model = solar_model.plot_cardinal_irradiances()
        # plt.show()
        
    def test_pv_plant(self):
        site_weather_data: buildingenergy.weather.SiteWeatherData = buildingenergy.weather.WeatherJsonReader(location='Grenoble', from_requested_stringdate="1/01/2023", to_requested_stringdate="1/01/2023", latitude_north_deg=45.19154994547585, longitude_east_deg=5.722065312331381).site_weather_data
        solar_model = buildingenergy.solar.SolarModel(site_weather_data)
        pv_plant = buildingenergy.solar.PVplant(solar_model, exposure_deg=0, slope_deg=145, mount_type=buildingenergy.solar.MOUNT_TYPES.BACK2BACK, number_of_panels=37, number_of_panels_per_array=6, distance_between_arrays_m=.7)
        print(pv_plant)

    def test_matching(self):
        site_weather_data: buildingenergy.weather.SiteWeatherData = buildingenergy.weather.WeatherJsonReader(location='Grenoble', from_requested_stringdate="1/01/2023", to_requested_stringdate="1/01/2024", latitude_north_deg=45.19154994547585, longitude_east_deg=5.722065312331381).site_weather_data
        solar_model = buildingenergy.solar.SolarModel(site_weather_data)
        print("start improving parameters")
        tic = time.time()
        solar_model = solar_model.match_measurements(plot=True)
        print(f'Duration: {time.time()-tic} secondes')
        print(solar_model.parameters)


if __name__ == '__main__':
    unittest.main()
