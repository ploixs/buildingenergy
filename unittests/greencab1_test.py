import unittest
from buildingenergy.data import DataProvider
from buildingenergy.inhabitants import Preference
from sites.greencab1 import make_data_provider, make_state_model_maker, make_simulation


class TestingGreencab1Dataset(unittest.TestCase):

    def test_data_provider(self):

        dp: DataProvider = make_data_provider('15/02/2015', '15/02/2016')
        
        state_model_maker, nominal_state_model = make_state_model_maker(dp)
        print(nominal_state_model)
        print(state_model_maker)

        make_simulation(dp, state_model_maker)
        
        preference = Preference(preferred_temperatures=(19, 24), extreme_temperatures=(16, 29), preferred_CO2_concentration=(500, 1500), temperature_weight_wrt_CO2=0.5, power_weight_wrt_comfort=0.5, mode_cop={1: 2, -1: 2})

        preference.print_assessment(dp.series('datetime'), Pheater=dp.series('Pheater'), temperatures=dp.series('TZcabinet'), CO2_concentrations=dp.series('CCO2cabinet'), occupancies=dp.series('occupancy'), modes=dp.series('mode'))
        dp.plot()


if __name__ == '__main__':
    unittest.main()
