import unittest
from buildingenergy.weather import WeatherJsonReader, SiteWeatherData
from buildingenergy.solar import MOUNT_TYPES, SolarModel, PVplant


open_weather_map_json_reader = WeatherJsonReader(location='Allex', from_requested_stringdate="1/01/2024", to_requested_stringdate="31/12/2024", latitude_north_deg=44.760079, longitude_east_deg= 4.895299)
site_weather_data: SiteWeatherData = open_weather_map_json_reader.site_weather_data
solar_model = SolarModel(site_weather_data)


def panel_distribution(number_of_panels: int, mount_type: MOUNT_TYPES, distance_between_arrays_m: float, exposure_deg: float = 0, slope_deg: float = 153, pv_efficiency: float = 0.3*.95, temperature_coefficient: float = 0.0035, number_of_cell_rows: int = 2, number_of_panels_per_array: float = 3, panel_width_m: float = 1, panel_height_m: float = 1.7, peak_power_kW: float = None, surface_pv_m2: float = None) -> PVplant:
    pv_plant: PVplant = PVplant(solar_model, exposure_deg=exposure_deg, slope_deg=slope_deg, distance_between_arrays_m=distance_between_arrays_m, mount_type=mount_type, peak_power_kW=peak_power_kW, number_of_panels=number_of_panels, number_of_panels_per_array=number_of_panels_per_array, panel_width_m=panel_width_m, panel_height_m=panel_height_m, pv_efficiency=pv_efficiency, temperature_coefficient=temperature_coefficient, surface_pv_m2=surface_pv_m2, number_of_cell_rows=number_of_cell_rows)
    print(pv_plant)
    pv_powers = pv_plant.powers_W(gather_collectors=False)
    for collector in pv_powers:
        print(collector, sum(pv_powers[collector])/1000, 'kWh')
    powers = pv_plant.powers_W(gather_collectors=True)
    if powers is not None:
        print('Total power: ', sum(powers)/1000, 'kWh')
    return pv_plant


class PV_test(unittest.TestCase):

    # @unittest.skip('Skipping test_back2back_1')
    def test_back2back_1(self) -> None:
        print('\n_____facing South_____')
        pv_plant_south: PVplant = panel_distribution(number_of_panels=6, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=0)
        print(pv_plant_south)
        print('\n_____facing West_____')
        pv_plant_west: PVplant = panel_distribution(number_of_panels=6, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90)
        print(pv_plant_west)
        self.assertEqual(panel_distribution(number_of_panels=6, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=90).n_panels, {'front_clear': 3, 'rear_clear': 3, 'front_shadow': 0, 'rear_shadow': 0})
        power_south: float = sum(pv_plant_south.powers_W(gather_collectors=False)['front_clear'])
        power_north: float = sum(pv_plant_south.powers_W(gather_collectors=False)['rear_clear'])
        power_west: float = sum(pv_plant_west.powers_W(gather_collectors=False)['front_clear'])
        power_east: float = sum(pv_plant_west.powers_W(gather_collectors=False)['rear_clear'])
        self.assertGreater(power_south, power_west)
        self.assertGreater(power_south, power_east)
        self.assertGreater(power_south, power_north)
        self.assertGreater(power_west, power_north)
        self.assertGreater(power_east, power_north)

    # @unittest.skip('Skipping test_back2back_2')
    def test_back2back_2(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=7, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 3, 'front_shadow': 1, 'rear_shadow': 0})
        
    # @unittest.skip('Skipping test_back2back_3')
    def test_back2back_3(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=8, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 3, 'front_shadow': 1, 'rear_shadow': 1})

    def test_back2back_4(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=9, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 3, 'front_shadow': 2, 'rear_shadow': 1})

    def test_back2back_5(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=1, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 1, 'rear_clear': 0, 'front_shadow': 0, 'rear_shadow': 0})

    def test_back2back_6(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=2, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 1, 'rear_clear': 1, 'front_shadow': 0, 'rear_shadow': 0})

    def test_back2back_7(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=8, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 3, 'front_shadow': 1, 'rear_shadow': 1})

    def test_back2back_8(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=4, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 2, 'rear_clear': 2, 'front_shadow': 0, 'rear_shadow': 0})

    def test_back2back_9(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=19, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 3, 'front_shadow': 7, 'rear_shadow': 6})

    # @unittest.skip('Skipping test_back2back_2')
    def test_back2back_10(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=6, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1.7, exposure_deg=-90, slope_deg=None).n_panels, {'front_clear': 3, 'rear_clear': 3, 'front_shadow': 0, 'rear_shadow': 0})
    
    # @unittest.skip('Skipping test_back2back_2')
    def test_back2back_11(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=6, mount_type=MOUNT_TYPES.BACK2BACK, distance_between_arrays_m=1, exposure_deg=-90, slope_deg=None).n_panels, {'front_clear': 0, 'rear_clear': 0, 'front_shadow': 0, 'rear_shadow': 0})

    def test_plan_1(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=19, mount_type=MOUNT_TYPES.PLAN, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 19, 'rear_clear': 0, 'front_shadow': 0, 'rear_shadow': 0})

    def test_flat_1(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=6, mount_type=MOUNT_TYPES.FLAT, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 0, 'front_shadow': 3, 'rear_shadow': 0})
        
    def test_flat_2(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=5, mount_type=MOUNT_TYPES.FLAT, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 0, 'front_shadow': 2, 'rear_shadow': 0})
        
    def test_flat_3(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=6, mount_type=MOUNT_TYPES.FLAT, distance_between_arrays_m=1.7, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 0, 'front_shadow': 3, 'rear_shadow': 0})

    def test_flat_4(self) -> None:
        self.assertEqual(panel_distribution(number_of_panels=6, mount_type=MOUNT_TYPES.FLAT, distance_between_arrays_m=.1, exposure_deg=-90).n_panels, {'front_clear': 3, 'rear_clear': 0, 'front_shadow': 3, 'rear_shadow': 0})

if __name__ == '__main__':
    unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))