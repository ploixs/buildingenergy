import unittest
import configparser
from buildingenergy.lambdahouse import LambdaParametricData
from buildingenergy.climate import ProspectiveClimateDRIAS, HistoricalDatabase, Merger, MinMerger, MaxMerger, AvgMerger, SumMerger, ProspectiveClimateRefiner
from buildingenergy.weather import SiteWeatherData, WeatherJsonReader
from buildingenergy.lambdahouse import Analyzes


config = configparser.ConfigParser()
config.read('setup.ini')


class TestingBasics(unittest.TestCase):

    # @unittest.skip('skipping parametric')
    def test_parametric(self):

        target_year = 2100
        prospective_period: tuple[str, str] = ('1/1/2025', '31/12/2100')
        reference_period: tuple[str, str] = ('1/1/2006', '31/12/2023')
        # ________________________
        # location = 'Grenoble'
        # drias_filename = 'Grenoble_RACMO22E_rcp8.5.txt'
        # drias_filename = 'Grenoble_ALADIN63_rcp8.5.txt'
        # latitude_north_deg: float = 45.19154994547585
        # longitude_east_deg: float = 5.722065312331381
        # ________________________
        # location = 'Carcassonne'
        # drias_filename = 'Carcassonne_ALADIN63_rcp8.5.txt'
        # latitude_north_deg: float = 43.200491165676674
        # longitude_east_deg: float = 2.3938780833626065
        # ________________________
        location = 'Paris'
        drias_filename = 'Paris_ALADIN63_rcp8.5.txt'
        latitude_north_deg: float = 48.89348182398542
        longitude_east_deg: float = 2.289189375459356

        prospective_climate = ProspectiveClimateDRIAS(filename=drias_filename, starting_stringdate=prospective_period[0], ending_stringdate=prospective_period[1])

        feature_merger_weights: dict[Merger, float] = {
            MinMerger('temperature'): 1,
            MaxMerger('temperature'): 1,
            AvgMerger('temperature'): 1,
            SumMerger('precipitation_mass'): 1,
            AvgMerger('absolute_humidity'): 1,
            AvgMerger('direct_normal_irradiance_instant'): 1,
            AvgMerger('wind_speed_m_per_s'): 1,
            SumMerger('snowfall_mass'): 1
            }

        historical_database = HistoricalDatabase(WeatherJsonReader(location=location, latitude_north_deg=latitude_north_deg, longitude_east_deg=longitude_east_deg, from_requested_stringdate=reference_period[0], to_requested_stringdate=reference_period[1]).site_weather_data, feature_merger_weights)
        pcr = ProspectiveClimateRefiner(prospective_climate=prospective_climate, historical_database=historical_database)
        prospective_site_weather_data: SiteWeatherData = pcr.make_prospective_site_weather_data(location)

        lpd:  LambdaParametricData = LambdaParametricData(prospective_site_weather_data, year=target_year)

        analyzes: Analyzes = Analyzes(lpd, on_screen=False)
        analyzes.climate()
        analyzes.evolution()
        # analyzes.solar()
        analyzes.house()
        # analyzes.parametric()
        analyzes.neutrality()
        analyzes.close()


if __name__ == '__main__':
    unittest.main()