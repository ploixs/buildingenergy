import unittest
import configparser
from buildingenergy.lambdahouse import LambdaParametricData, Simulator, Analyzes
from buildingenergy.weather import WeatherJsonReader
import plotly.graph_objects as go
import plotly.subplots as subplots

config = configparser.ConfigParser()
config.read('setup.ini')
# weather_file_name = None

# weather_json_reader = WeatherJsonReader(location='Grenoble', latitude_north_deg=45.19154994547585, longitude_east_deg=5.722065312331381, pollution=0.1, albedo=0.1)
weather_json_reader = WeatherJsonReader(location='Paris', latitude_north_deg=48.89348182398542, longitude_east_deg=2.289189375459356, pollution=0.1, albedo=0.1)
# weather_json_reader = WeatherJsonReader(location='Carcassonne', latitude_north_deg=43.200491165676674, longitude_east_deg=2.3938780833626065, pollution=0.1, albedo=0.1)
lpd:  LambdaParametricData = LambdaParametricData(weather_json_reader.site_weather_data, 2023)


class TestingBasics(unittest.TestCase):

    @unittest.skip('skipping parametric')
    def test_parametric(self):
        initial_signature: int = lpd.signature
        lpd.given('thickness_m', 7e-3)
        self.assertEqual(lpd('thickness_m'), 7e-3)
        self.assertNotEqual(lpd('south_solar_protection_angle_deg', nominal=True), 0.2)
        self.assertEqual(lpd('south_solar_protection_angle_deg'), 0)

        self.assertNotEqual(lpd.signature, initial_signature)
        self.assertListEqual(lpd.parametric('south_solar_protection_angle_deg'), [0, 15, 30, 35, 40, 45, 50, 55, 60, 65, 70])

        lbd_copy: LambdaParametricData = lpd.copy()
        lbd_copy.given('shape_factor', 2)
        lbd_copy.given('number_of_floors', 2)

        self.assertAlmostEqual(lbd_copy('shape_factor'), lbd_copy('largest_side_length_m') / lbd_copy('smallest_side_length_m'), 4)
        print(lbd_copy)
        self.assertNotEqual(lpd.signature, lbd_copy.signature)
        lbd_copy.reset('number_of_floors')
        self.assertNotEqual(lpd.signature, lbd_copy.signature)
        lbd_copy.reset()
        self.assertNotEqual(lpd.signature, lbd_copy.signature)
        lpd.given('thickness_m', .2)
        self.assertEqual(lbd_copy.signature, initial_signature)

        lpd.select('south_solar_protection_angle_deg')
        sum_angles = sum(lpd.parametric('south_solar_protection_angle_deg'))
        for _ in lpd:
            sum_angles -= _
            print(_)
        self.assertEqual(sum_angles, 0)

        self.assertEqual(lpd('wall_composition_in_out')[-1], ('polystyrene', 0.2))
        lpd.given('thickness', 43e-2)
        self.assertEqual(lpd('wall_composition_in_out')[-1], ('polystyrene', lpd('thickness_m')))
        self.assertTrue('glazing_ratio_south' in lpd)

    # @unittest.skip('skipping report')
    def test_report(self):
        analyzes: Analyzes = Analyzes(lpd, on_screen=False)
        analyzes.climate()
        analyzes.evolution()
        # analyzes.solar()
        analyzes.house()
        # analyzes.parametric()
        analyzes.neutrality()
        analyzes.close()

    @unittest.skip('skipping weather')
    def test_weather(self) -> None:
        print(lpd('site_weather_data').__str__())

    @unittest.skip('skipping hvac')
    def test_hvac(self) -> None:
        print(Simulator.hvac_on((1, 2), 1))
        print(Simulator.hvac_on((1, 2), 3))
        print(Simulator.hvac_on((1, 2, 3, 4), 1))
        print(Simulator.hvac_on((1, 2, 3, 4), 3))
        print(Simulator.hvac_on((1, 2, 3, 4), 4))
        print(Simulator.hvac_on((1, 2, 3, 4), 5))

    @unittest.skip('skipping simulator')
    def test_simulator(self):
        lpd.reset()
        Simulator.run(lpd)
        print(lpd)

        plot_results(lpd, ('windows_solar_gains_W', 'best_PV_plant_powers_W', 'cooling_needs_W', 'heating_needs_W', 'hvac_needs_W', 'electricity_needs_W', 'electricity_grid_exchange_W'), ('outdoor_temperature_deg', 'smooth_outdoor_temperatures_for_hvac_periods_deg', 'indoor_temperatures', 'setpoint_temperatures', 'occupancy'))


def plot_results(lbd: LambdaParametricData, data_names1: list[str], data_names2: list[str] = None):
    if data_names2 is None:
        fig = go.Figure(layout=go.Layout(title='plot'))
        for data_name in data_names1:
            fig.add_trace(go.Scatter(x=lbd('datetimes'), y=lbd(data_name), mode='lines', name=data_name))
    else:
        fig = subplots.make_subplots(rows=2, cols=1, shared_xaxes=True)
        for data_name in data_names1:
            fig.add_trace(go.Scatter(x=lbd('datetimes'), y=lbd(data_name), mode='lines', name=data_name), row=1, col=1)
        for data_name in data_names2:
            fig.add_trace(go.Scatter(x=lbd('datetimes'), y=lbd(data_name), mode='lines', name=data_name), row=2, col=1)
    fig.show()


if __name__ == '__main__':
    unittest.main()

    # python -m cProfile -o pg.prof ./unittests/lambdahouse_test.py
    # snakeviz pg.prof
    # analyzes: Analyzes = Analyzes(lpd, on_screen=False)
    # analyzes.parametric()
    # analyzes.close()
