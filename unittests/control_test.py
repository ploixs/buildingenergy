import unittest
from buildingenergy.control import ModePort, Port, MultiplexPort, MultimodePort, ZoneHvacContinuousPowerPort, ZoneTemperatureSetpointPort, BinaryPort, AirflowPort, ZoneTemperatureController, VALUE_DOMAIN_TYPE, ContinuousPort, DiscretePort, BinaryPort
from buildingenergy.data import DataProvider
from buildingenergy.signal import SignalGenerator, Merger
from sites.data_h358 import make_data_provider
from sites.building_h358 import make_building_state_model_k

dp_h358: DataProvider = make_data_provider('15/02/2015', '17/02/2015')
insulation_thickness = 150e-3
surface_cabinet: float = 5.85*2.14
volume_cabinet: float = surface_cabinet*2.29
surface_toilet: float = 1.18*2.14
volume_toilet: float = surface_toilet*2.29
surface_window: float = 2.2*.9
surface_cabinet_wall: float = (2 * 6000e-3 + 2440e-3) * 2590e-3 - surface_window
air_flows = {'office-outdoor:Q_window': 5500/3600, 'office-outdoor:Q_door': 1000/3600}
body_metabolism = 100
occupant_consumption = 200
occupancy_sgen = SignalGenerator(dp_h358.series('datetime'))
occupancy_sgen.daily([0, 1, 2, 3, 4], {0: 0, 8: 3, 18: 0})  # 12: 0, 13: 3,
occupancy: list[float] = occupancy_sgen()
dp_h358.add_external_variable('occupancy', occupancy)
presence: list[int] = [int(occupancy[k] > 0) for k in range(len(dp_h358))]
dp_h358.add_external_variable('presence', presence)

temperature_sgen = SignalGenerator(dp_h358.series('datetime'), None)
temperature_sgen.daily([0, 1, 2, 3, 4], {0: None, 7: 20, 19: None}, merger=Merger(min, 'r'))
heating_period: list[float | None] = temperature_sgen.seasonal('16/11', '15/3', 1, merger=Merger(max, 'b'))
temp_sgen = SignalGenerator(dp_h358.series('datetime'), None)
temp_sgen.daily([0, 1, 2, 3, 4], {0: None, 7: 22, 19: None}, merger=Merger(min, 'r'))
cooling_period = temp_sgen.seasonal('16/3', '15/11', 1, merger=Merger(max, 'b'))
temperature_sgen.combine(temp_sgen(), merger=Merger(min, 'n'))
dp_h358.add_external_variable('TZcabinet_setpoint', temperature_sgen())

dp_h358.add_external_variable('PZcabinet', [occupancy[k] * (body_metabolism + occupant_consumption) for k in dp_h358.ks])
dp_h358.add_parameter('PZtoilet', 0)

hvac_modes_sgen = SignalGenerator(dp_h358.series('datetime'))
hvac_modes_sgen.combine(heating_period, merger=Merger(max, 'l'))
hvac_modes_sgen.combine(cooling_period, merger=Merger(lambda x, y: x - y, 'n'))
dp_h358.add_external_variable('mode', hvac_modes_sgen())

state_model_maker, nominal_state_model = make_building_state_model_k(dp_h358)
print(nominal_state_model)
print(state_model_maker)


class TestingPorts(unittest.TestCase):

    # @unittest.skip('skip')
    def test_port(self):
        control_port1 = Port('PZcabinet', (0, 2000, 5000), value_domain_type=VALUE_DOMAIN_TYPE.CONTINUOUS)
        self.assertEqual(control_port1(port_value=0), 0)
        self.assertEqual(control_port1(port_value=1000), 1000)
        self.assertEqual(control_port1(port_value=2000), 2000)
        self.assertEqual(control_port1(port_value=5000), 5000)
        self.assertEqual(control_port1(port_value=10000), 5000)
        self.assertEqual(control_port1(), (0, 5000))
        self.assertEqual(control_port1(), (0, 5000))
        print(control_port1(10000))

        control_port2 = DiscretePort('PZcabinet', (0, 2000, 5000))
        self.assertEqual(control_port2(port_value=0), 0)
        self.assertEqual(control_port2(port_value=1000), 0)
        self.assertEqual(control_port2(port_value=2000), 2000)
        self.assertEqual(control_port2(port_value=5000), 5000)
        self.assertEqual(control_port2(port_value=6000), 5000)
        self.assertEqual(control_port2(), (0, 2000, 5000))

    # @unittest.skip('skip')
    def test_mode_port(self) -> None:
        mode_port1 = ModePort('PZcabinet', 'mode', {-1: (-2000, 0), 0: None, 1: (0, 3000)}, value_domain_type=VALUE_DOMAIN_TYPE.CONTINUOUS, default_mode=0)
        self.assertEqual(mode_port1(modes_values={'mode': 0}, port_value=2000), None)
        self.assertEqual(mode_port1(modes_values={'mode': -1}, port_value=2500), 0)
        self.assertEqual(mode_port1(modes_values={'mode': -1}, port_value=-1000), -1000)
        self.assertEqual(mode_port1(modes_values={'mode': 1}, port_value=1500), 1500)
        self.assertEqual(mode_port1(modes_values={'mode': 1}, port_value=4000), 3000)
        print(mode_port1(modes_values={'mode': 1}))
        self.assertEqual(mode_port1(modes_values={'mode': 1}, port_value=3000), 3000)
        self.assertEqual(mode_port1(modes_values={'mode': 1}, port_value=4500), 3000)

        mode_port2 = ModePort('PZcabinet', 'mode', {-1: (-6, -3, -1), 0: None, 1: (0, 2, 3, 4, 8)}, value_domain_type=VALUE_DOMAIN_TYPE.DISCRETE, default_mode=0)
        # print(dp_h358.series('mode')[0:4])
        self.assertEqual(mode_port2(modes_values={'mode': 0}, port_value=-4), None)  # 0, -1, -1, 1, 1
        self.assertEqual(mode_port2(modes_values={'mode': -1}, port_value=-1), -1)
        self.assertEqual(mode_port2(modes_values={'mode': -1}, port_value=-4), -3)
        self.assertEqual(mode_port2(modes_values={'mode': 1}, port_value=5), 4)
        self.assertEqual(mode_port2(modes_values={'mode': 1}, port_value=50), 8)
        self.assertEqual(mode_port2(modes_values={'mode': -1}), (-6, -3, -1))
        self.assertEqual(mode_port2(modes_values={'mode': -1}, port_value=0), -1)
        self.assertEqual(mode_port2(modes_values={'mode': 1}), (0, 2, 3, 4, 8))

    # @unittest.skip('skip')
    def test_multimode_port(self) -> None:
        multimode_port = MultimodePort('PZcabinet', ('mode', 'presence'), {(-1, 0): (-6, -3, -1), (-1, 1): (-7, -4, -2), (0, 0): None, (1, 0): (0, 2, 3, 4, 8), (1, 1): (0, 1, 3, 6, 10)}, value_domain_type=VALUE_DOMAIN_TYPE.DISCRETE)
        self.assertEqual(multimode_port(modes_values={'mode': 0, 'presence': 0}, port_value=-5), None)
        self.assertEqual(multimode_port(modes_values={'mode': -1, 'presence': 1}, port_value=-5), -4)
        self.assertEqual(multimode_port(modes_values={'mode': -1, 'presence': 0}, port_value=-4), -3)
        self.assertEqual(multimode_port(modes_values={'mode': 1, 'presence': 1}, port_value=5), 6)
        self.assertEqual(multimode_port(modes_values={'mode': 1, 'presence': 1}, port_value=50), 10)
        self.assertEqual(multimode_port(modes_values={'mode': 1, 'presence': 1}), (0, 1, 3, 6, 10))
        self.assertEqual(multimode_port(modes_values={'mode': -1, 'presence': 1}, port_value=5), -2)
        self.assertEqual(multimode_port(modes_values={'mode': -1, 'presence': 0}), (-6, -3, -1))
        self.assertEqual(multimode_port(modes_values={'mode': 1, 'presence': 1}, port_value=7), 6)

        multimode_port2 = MultimodePort('PZcabinet', ('mode', 'presence'), {(-1, 0): (-6, -1), (-1, 1): (-7, -2), (0, 0): (0, 0), (1, 0): (0, 8), (1, 1): (0, 10)}, value_domain_type=VALUE_DOMAIN_TYPE.CONTINUOUS)
        self.assertEqual(multimode_port2(modes_values={'mode': -1, 'presence': 1}, port_value=-4.5), -4.5)
        self.assertEqual(multimode_port2(modes_values={'mode': 0, 'presence': 0}, port_value=4), 0)
        self.assertEqual(multimode_port2(modes_values={'mode': 1, 'presence': 1}, port_value=5), 5)
        self.assertEqual(multimode_port2(modes_values={'mode': 1, 'presence': 1}, port_value=50), 10)
        self.assertEqual(multimode_port2(modes_values={'mode': 1, 'presence': 1}, port_value=-7), 0)
        self.assertEqual(multimode_port2(modes_values={'mode': 1, 'presence': 0}, port_value=5), 5)
        self.assertEqual(multimode_port2(modes_values={'mode': -1, 'presence': 0}, port_value=-6), -6)
        self.assertEqual(multimode_port2(modes_values={'mode': 1, 'presence': 1}, port_value=11), 10)

    # @unittest.skip('skip')
    def test_zone_temperature_setpoint_port(self) -> None:
        port = ZoneTemperatureSetpointPort(temperature_setpoint_variable='TZcabinet_setpoint', mode_variable_name='mode')
        port(modes_values={'mode': 0}, port_value=14)
        port(modes_values={'mode': 1}, port_value=14)
        port(modes_values={'mode': 2}, port_value=19)
        port(modes_values={'mode': 3}, port_value=19)
        port(modes_values={'mode': 4}, port_value=23)

    # @unittest.skip('skip')
    def test_opening_control_port(self) -> None:
        port = BinaryPort('window_opening', 'presence')
        port(modes_values={'window_opening': 1, 'presence': 1}, port_value=0.5)
        port(modes_values={'window_opening': 1, 'presence': 0}, port_value=0.5)
        port(modes_values={'window_opening': 0, 'presence': 0}, port_value=1)
        port(modes_values={'window_opening': 0, 'presence': 1}, port_value=1)

    # @unittest.skip('skip')
    def test_airflow_port(self) -> None:
        port = AirflowPort(airflow_variable='cabinet-outdoor:Q', infiltration_rate=10/3600, ventilation_levels={'office-outdoor:Q_window': 5500/3600, 'office-outdoor:Q_door': 1000/3600})
        self.assertEqual(port(modes_values={'office-outdoor:Q_window': 1, 'office-outdoor:Q_door': 0}), (10+5500)/3600)
        self.assertAlmostEqual(port(modes_values={'office-outdoor:Q_window': 1, 'office-outdoor:Q_door': 1}), (10+5500+1000)/3600, places=5)
        self.assertAlmostEqual(port(modes_values={'office-outdoor:Q_window': 0, 'office-outdoor:Q_door': 1}), (10+1000)/3600, places=5)
        self.assertAlmostEqual(port(modes_values={'office-outdoor:Q_window': 0, 'office-outdoor:Q_door': 0}), (10)/3600, places=5)
        
    # @unittest.skip('skip')
    def test_zone_hvac_power_port(self) -> None:
        port = ZoneHvacContinuousPowerPort('PZcabinet', 'mode', max_heating_power=3000, max_cooling_power=2000, full_range=False)
        self.assertEqual(port(modes_values={'mode': 0}, port_value=4000), 0)
        self.assertEqual(port(modes_values={'mode': 1}, port_value=2500), 2500)
        self.assertEqual(port(modes_values={'mode': -1}, port_value=-4000), -2000)
        self.assertEqual(port(modes_values={'mode': 1}, port_value=4000), 3000)

    def u_without_heater(k: int) -> float:
        return {v: dp_h358(v, k) for v in ('TZcorridor', 'TZdownstairs', 'TZoutdoor', 'PZoffice', 'CCO2corridor', 'CCO2outdoor', 'PCO2office')}

    # @unittest.skip('skip')
    def test_zone_temperature_controller(self) -> None:
        mode_variable_name = 'mode'
        zone_temperature_setpoint_port = ZoneTemperatureSetpointPort(temperature_setpoint_variable='TZoffice_setpoint', mode_variable_name=mode_variable_name)
        print(zone_temperature_setpoint_port)
        zone_hvac_power_port = ZoneHvacContinuousPowerPort(hvac_power_variable='PZoffice', hvac_mode='mode', max_heating_power=3000, max_cooling_power=2000, full_range=False)
        print(zone_hvac_power_port)
        
        controller = ZoneTemperatureController(nominal_state_model, 'TZoffice', zone_temperature_setpoint_port, zone_hvac_power_port)
        
        print(controller)
        print(nominal_state_model)
        output_names = nominal_state_model.output_names
        setpoint_values = [27 for k in range(24)]
        k = 0
        x_k = nominal_state_model.initialize(**TestingPorts.u_without_heater(k))
        for k in range(24):
            mode_k = dp_h358(mode_variable_name, k)
            u_k = TestingPorts.u_without_heater(k)
            u_kp1 = TestingPorts.u_without_heater(k+1)
            delta_power_control_value_k = controller.step(setpoint_values[k], nominal_state_model, x_k, mode_k, u_k, u_kp1)
            u_k[controller.power_control_variable()] += delta_power_control_value_k
            y_k: list[float] = nominal_state_model.output(**u_k)
            [dp_h358(output_names[i], k, y_k[i]) for i in range(len(output_names))]
            x_kp1 = nominal_state_model.step(**u_k)
            nominal_state_model.set_state(x_kp1)
            x_k = x_kp1
            if k > 0:
                self.assertAlmostEqual(dp_h358('TZoffice', k), 24, places=5)
        print('TZoffice', [dp_h358('TZoffice', k) for k in range(24)])
        print('TZoffice_setpoint', setpoint_values)
        print('PZoffice', [dp_h358('PZoffice', k) for k in range(24)])


if __name__ == '__main__':
    unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
