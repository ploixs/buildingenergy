import unittest
from buildingenergy.data import DataProvider
from sites.greencab2 import make_data_provider, make_state_model_maker, make_simulation


class TestingGreencab2Dataset(unittest.TestCase):

    def test_data_provider(self):

        dp: DataProvider = make_data_provider('15/02/2015', '15/02/2016')
        # dp.plot()
        
        state_model_maker, nominal_state_model = make_state_model_maker(dp)
        print(nominal_state_model)
        print(state_model_maker)

        make_simulation(dp, state_model_maker)


if __name__ == '__main__':
    unittest.main()
