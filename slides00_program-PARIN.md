---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **Energy and Buildings**
## PARIN

---

# program

- session 1 & 2: model of solar radiation and Earth atmosphere
- session 3: model of climate evolution
- session 4: model of human impact and comfort
- session 5: $\lambda$-house and reality
- session 6 & 7: modeling buildings from physics
- session 8: modeling dynamics of buildings
- session 9 & 10: building simulation with Pleiades
