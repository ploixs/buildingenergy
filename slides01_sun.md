---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **Sun, Earth and heat transfer**

stephane.ploix@grenoble-inp.fr

---

## Electro-magnetic radiations

__Each material__, which is not at the absolute minimum temperature (0 Kelvin), __emits radiations__ corresponding to a conversion of a material's internal energy into electromagnetic energy. Thermal electro-magnetic radiations depend on the temperature and on the __surface of a material__. 

![](figs/atom_radiation.gif)

---

The following figure shows the nature of electro-magnetic radiations depending on the wave length (that implicitly depends on the surface temperature and on the type of surface):

![width:28cm](img/EMSpectrum.jpg)

---

Only few electro-magnetic wave lengths are perceived by humans:

- through eyes: $\lambda \in [380nm, 780nm]$, so called short wave lengths
- through skin:  $\lambda \in [780nm, 0.314mm]$​​​​, so called long wave lengths

Note that __ultraviolet radiations are mostly absorbed by ozone (O3)__ of the troposphere and the stratosphere but the little quantity that reaches the ground is important. Indeed __exposure to ultraviolet radiation is essential for good health__, as it stimulates the production of vitamin D in the body but too much exposure might lead to skin cancer. 

---

Shorter wave lengths like X-rays or gamma radiations are armful for human body (very short wave radiations implies high power): they contains a lot of energy: 
$$E = h\nu = \frac{hc}{\lambda}$$ 
where $E$ stands for energy, $h$ for the Planck's constant $(6.62607015 × 10^{-34} m^2 kg/s)$, $\nu$ for frequency, $c$ for the speed of light ($299792458 m/s$), and $\lambda$ for the wavelength.

---

Depending on the material, the wave length and the incident angle, a light beam can be transmitted, reflected or absorbed according respectively to transmission, reflection and absorption coefficients: $\alpha_t(\nu)$, $\alpha_r(\nu)$, $\alpha_a(\nu)$ with, due to energy conservation:

$$
\alpha_t(\nu)+\alpha_r(\nu)+\alpha_a(\nu)=1
$$

where $\nu$ is a single wave length but more frequently wave length ranges like visible short wave length (Sun [380nm, 4$\mu$m]) or long wave length (Earth [4$\mu$m, 0.314mm]).

These coefficients are wave length dependent.

---

![bg fit](img/image-20240428002615883.png)

---

The ratio $\rho$ is named reflectance, $\tau$ transmittance (or solar factor) and $\alpha$ absorptance.

When a radiation beam is hitting a surface, the absorbed energy is given to the surface as an power source.

For the visible range, the reflectance $\rho$ of the materials is given below (it comes from energy+). Because most of these materials have a null transmittance, the absorptance can be deduced.

---

| painting colors | reflectance |
| --------------- | ----------- |
| white           | 0.7 to 0.8  |
| yellow          | 0.5 to 0.7  |
| green           | 0.3 to 0.6  |
| grey            | 0.35 to 0.6 |
| brown           | 0.25 to 0.5 |
| blue            | 0.2 to 0.5  |
| red             | 0.3 to 0.35 |
| black           | 0.04        |

---

| woods               | reflectance  |
| ------------------- | ------------ |
| light birch, maple  | 0.55 to 0.65 |
| light varnished oak | 0.4 to 0.5   |
| dark varnished oak  | 0.15 to 0.4  |
| mahogany, walnut    | 0.15 to 0.4  |


| wallpapers                                     | reflectance  |
| ---------------------------------------------- | ------------ |
| very light (white, cream)                      | 0.65 to 0.75 |
| light (grey, yellow, blue)                     | 0.45 to 0.6  |
| dark (black, dark blue, dark grey, green, red) | 0.05 to 0.36 |

---

| construction materials   | reflectance  |
| ------------------------ | ------------ |
| white plaster            | 0.7 to 0.8   |
| clean white marble       | 0.8 to 0.85  |
| clean white brick        | 0.62         |
| red brick                | 0.1 to 0.2   |
| old red brick            | 0.05 to 0.15 |
| polished aluminium       | 0.6 to 0.75  |
| mate aluminium           | 0.55 to 0.6  |
| white enamel             | 0.65 to 0.75 |
| glazing                  | 0.08 to 0.4  |

---

| construction materials   | reflectance  |
| ------------------------ | ------------ |
| new white render (crépi) | 0.7 to 0.8   |
| old white render (crépi) | 0.3 to 0.6   |
| new concrete             | 0.4 to 0.5   |
| old concrete             | 0.05 to 0.15 |

---

Regarding transmittance or solar factor, a glazing is mostly (depending on the type of glass) transparent for visible radiations but opaque for infrareds. Modeling solar factor (SF) by a single coefficient is an approximation because it depends on the radiation beam angle:

<img src="img/image-20240428002147510.png" width="60%"/>

---

Repartition of solar power (short waves): near-IR:49%, visible: 43%, UV: 7%, X-ray, gamma: 1% absorbed by atmosphere

Solar irradiance reaching Earth and passing through standard glazing: 99%~100%

![width:19cm](figs/transmit.png)

---

The following figure shows how glass is behaving depending on the radiation wave length:

<img src="img/glass_filtering.png" width="80%"/>

The glass becomes opaque from about $4\mu m$: it's the limit between the so-called short wave and long-wave radiations.

---

In the next figure, in the next figure, identify the type of wave length of each beam:

<img src="img/image-20240428005320459.png" width="50%"/>

Explain why interior blinds does not protect from the heat of solar radiations.

---

### Black, grey and white bodies

A __black body__ is an __ideal emitter__ used as a reference but in reality, no object is a pure black body although most have a close behavior. Here is a spectrum comparison of a theoretical black body at 5777K and the solar radiance (sun temperature is about at 5772K):

---

<img src="img/EffectiveTemperature_300dpi_e.png" width="70%"/>

---

A black body absorbs 100% of the incident beams. It emits more than any other body i.e. no transmission and no reflection. A black body emits:

$$
 E=\sigma T_{Kelvin}^4
$$

 where $T_{Kelvin} = \sigma (T_{celsius}+273.15°C)$ and $\sigma$ is the Stefan-Boltzmann constant $\sigma=5.670374419\times 10^{-8}W/m^2/K^4$. 

---

 With usual temperatures on Earth, it can be easily linearized:

<img src="img/image-20240518082525468.png" alt="image-20240518082525468" width="65%" />

---

Other bodies emitance, or radiant exitance, are absorbing less power than black bodies i.e. only a fraction $\epsilon$ of this power named "emissivity":
$$
E=\epsilon\sigma T_\text{Kelvin}^4
$$
with the emissivity $\epsilon \in [0,1]$. 

Here are few different emissivity values. The file [propertiesDB.xlsx](../propertiesDB.xlsx) provide much more values. Note that most values are closed to $\approx$ 0.9 except for metals:

---

<img src="img/image-20240428005608289.png" alt="image-20240428005608289" width="50%" />

Infrared thermo-meters assume an emissivity of 0.9: the measured temperature is true for most materials but fake for metallic surfaces and low emissivity materials.

---

The areal density of __luminous flux emitted by a surface per unit area__ (in $W/m^2$) is named power exitance or __emittance__ (symbol $E$). It is __the power emitted in all directions by per unit of emitting surface__: 
$$E=\frac{d\varphi}{dS}$$

It corresponds to the __emission of radiated electro-magnetic power by a body__. To designate the __radiated energy striking a surface__, one speaks about __illuminance__ (photometry or radiometry). However, there is no physical difference between these quantities, only the observer's point of view.

---

As the temperature of a black body decreases, the emitted thermal radiation decreases in intensity and its maximum moves to longer wavelengths (not the energy is the integral of $E=\int_\lambda E(\lambda) d\lambda$). 

Black-body radiation has a characteristic, continuous frequency spectrum that depends only on the body's temperature:

---

<img src="img/energy-unit-area-second-wavelength-interval-temperatures.jpg" alt="img" width="70%" />

---

It's modelled by the Planck's law:
$$
    B(\lambda, T_{Kelvin})= \frac{2hc^2}{\lambda^5}\frac{1}{e^\frac{hc}{\lambda k_B T_{Kelvin}}-1}
$$
where $k_B$ is the Boltzmann constant ($k_B=1.38×10^{-23} J/K$), which is different from the Stefan-Boltzmann constant, $B(\lambda, T_{Kelvin})$ is the spectral radiance expressed in $W/m^2/sr/Hz$ and other notations have been introduced previously.

---

The color of blackbody radiation scales inversely with the temperature of the black body. These colors are often used as a color scale (in photography lighting for instance): it can be obtained by the approximate Wien displacement law that shows that the maximum spectral radiance depends only on the temperature of a black body:
$$
\lambda_{max} = \frac{hc}{5 k_B T} \approx \frac{2.898\times 10^{-3}}{T_{Kelvin}} \text{ in }\mu m
$$

---

![bg fit](img/PlanckianLocus.png)

---

Color temperature is a parameter describing the color of a visible light source by comparing it to the color of light emitted by an idealized opaque, non-reflective body called black body. 

The temperature of the ideal emitter that matches the color most closely is defined as the color temperature of the original visible light source. Color temperature is usually measured in Kelvins. The color temperature scale describes only the color of light emitted by a light source, which may actually be at a different (and often much lower) temperature.

---

A white body reflects all the incident radiations falling on it.

A __grey body absorbs a part of the incident beams, which depends on the temperature but not of the wave length__

__Most materials are considered as piece-wise grey bodies__ where pieces depends of wave length intervals.

---

The next figure shows the different radiations appearing in the Solar-Earth system. The yellow arrows stands for short wave length visible radiations and the red ones for long wave length infrared radiations. Daily average powers are given. 3 energy balances are satisfied: at earth surface, at atmospheric level and at tropopause.

---

![bg fit](img/radiative_forcing.png)

---

![bg fit](img/std-atmos-temperature-color.png)

---

The spectrum of the solar radiations reaching the tropopause is closed to a black body but when radiations are passing through the atmosphere, there are absorption and scattering mechanisms that modify the spectrum. 

__Solar radiation closely follows that of the black body at 5772K__. However, these radiations are attenuated by scattering and absorption as it passes through the Earth's atmosphere: the thicker the atmosphere, the greater the attenuation. 

---

Certain gases in the atmosphere absorb specific wavelengths, which are very strongly attenuated in the solar radiation reaching the ground: this is the case of water vapour, which absorbs several wavelengths, as well as nitrogen, oxygen and carbon dioxide. The solar spectrum at ground level is therefore narrowly restricted to wavelengths between the far infrared and the near ultraviolet.

---

We can see the effects of different greenhouse effect gas. Note that the water is the main cause of modification of the ideal black body spectrum. Most of the energy is located in the visible range of the spectrum but still there is energy in the infrared range. Most of the UVs do not pass through the atmosphere.

---

![bg fit](img/Sonne_Strahlungsintensitaet.svg.png)

---

$AM_0$ and $AM1.5$ represent the optical paths, depending on the Sun altitude and  followed by the solar beams with ($L_0$ is the atmosphere thickness):
$$
AM_x=\frac{L}{L_0}
$$

---

### Aphelion and Perihelion 

- Minimum Sun-Earth distance considering planet apexes: $\check D = 147.1\times 10^9$ m
- Mean Sun-Earth distance: $D=149.6\times 10^9$ m
- Maximum Sun-Earth distance: $\hat D=152.1\times 10^9$ m
- Sun equatorial radius: $R=696\times 10^6$ m
- Temperature of the photosphere: $T=5772$ K
- Earth radius: $r=6.38\times 10^6$ m

---

Considering Sun as a black body, the solar radiant exitance is given:

- $M_{Sun}=\sigma T^4$ in $W/m^2$ where $\sigma=5.670\times10^{-8}$ is the Stefan-Boltzmann constant

The total emitted solar power is: 

- $L_{Sun}=4\pi R^2 M_{Sun}=4 \sigma \pi R^2 T^4$ in W

---

The total solar power emitted by Sun per surface unit at the distance of Earth is given by:

- $P_D=\frac{L_{Sun}}{4\pi D^2}=\sigma \frac{R^2}{ D^2}T^4$ in $W/m^2$

The total power received by Earth is:

- $P_{Earth}=\pi r^2 P_D = \frac{\sigma \pi r^2 R^2 T^4}{D^2}$

Solar irradiance of Earth at shortest distance: $180$ petaW (1 peta Watt =$10^{15}W$)

Solar irradiance of Earth at mean distance: $174$ petaW
Solar irradiance of Earth at longest distance:  $169$ petaW​

---

It means a variation of the energy received from Sun of $\pm 3\%$. It's not perceived by humans: the longest distance corresponds to the so-called Aphelion (occurring in early July) and the shortest to the so-called Perihelion (occurring in early January). 

The declination of the Earth rotation axis has a much bigger influence and yields seasons.

---

## Radiometry

The aim of photometry is to evaluate light radiation as perceived by human vision, and to study the transmission of this radiation from a quantitative point of view. __Radiometry measures all electromagnetic waves__ (from γ-rays to radio waves). 

When working with energies, the radiometry concepts are used: the visible and infrared band-passes are focused: they correspond to the 2 ranges of temperatures met in the Sun-Earth system. 

---

Let's define some concepts regarding radiometry. Photometric concepts are nevertheless given for information (sometimes they are used instead of radiometry concepts). 

Different names are sometimes used dealing with the same concept: units are useful to clarify confusions

---

The following table summarizes the main concepts depending wether they deal with directional or non-directional radiation sources, and whether they concern unit surfaces or the radiation sources.

---

![bg fit](img/image-20240518113558656.png)

---

<style scoped>table {font-size: 20px;}</style>
| photometry / radiometry                                | photometry                              | radiometry    | comment                                                                                          | symbols (radiometry) |
|--------------------------------------------------------|-----------------------------------------|---------------|--------------------------------------------------------------------------------------------------|----------------------|
| luminous flux / radiant flux                           | lumen (lm) or candela.steradian (cd.sr) | Watt          | total power emitted by a radiative source in all directions                                      | $\varphi$ or $\Phi$  |
| luminous / radiant intensity (in a specific direction) | candela (cd)                            | Watt/sr       | power intensity of a radiative source emitted in a  solid angle directed by $\overrightarrow{u}$ | $I$ or $\Omega$      |
| luminance / radiance                                   | candela/$m^2$ <br />(cd/$m^2$)          | Watt/sr/$m^2$ | power intensity in a specific direction $\overrightarrow{u}$ emitted by a unit surface           | L                    |
| exitance / emittance                                   | $W.m^2/Hz$                              | Watt/$m^2$    | light radiated per unit of surface in all directions                                             | M (or E)             |
| illuminance / irradiance                               | lux (1lm/$m^2$)                         | Watt/$m^2$    | light received by a surface from all directions                                                  | E                    |

---

The __radiant intensity of a radiation corresponds to the quantity of energy emitted by a surface per unit of solid angle directed by a direction $\overrightarrow{u}$ and per unit of emitting surface perpendicular to $\overrightarrow{u}$__.

Software like __*Radiance*__ can model the impact of radiations in a complex scene using a __ray tracing mechanism__ lying on a forward propagation mechanism, starting from a radiation source and a backward mechanism following the enlightened object.

---

![bg fit](img/ray_tracing.png)

---

Primary sources are distinguish from secondary (reflectors), which are also receptors. Pure receptors are black bodies.

The next variables denoted like $X$ are radiometric variables while the photometric ones would be denoted $X_\lambda$ if used, when there are mono-chromatic, i.e. related to the specific wave length $\lambda$, and $X_\nu$ when they are related to the whole visible light spectrum. It satisfies: $X=\int X_\lambda d\lambda$ and $X_\nu=\int_\nu X_\lambda d\lambda$.

Irradiance expressed in $W/m^2$ becomes irradiations corresponding to $Wh/m^2$ when it deals with energy.

---

In photometry, the total power emitted by a source of surface $S$ in all directions is denoted $\varphi$ in Watt.

<img src="img/mceclip0.png" alt="mceclip0" width="70%"/>

---

- Luminous (lumen) or radiant flux (Watt)

1 lumen (lm) corresponds to the luminous flux emitted within a solid angle of 1 steradian (sr) by an isotropic spot light source located at the summit of a solid angle, with a luminous intensity of 1 candela. We have 1 ln = 1 cd.sr.

---

In radiometry, it corresponds to the total power emitted by a surface .

<img src="img/image-20240406021903031.png" width="90%"/>

---

- Luminous (Candela) or radiant intensity  (Watt/steradian)

The candela (cd) is the light intensity, in a given direction, of a source emitting monochromatic radiation of frequency $540.10^{12}Hz$  (555nm, $\lambda=c/f$ where $c=299792458m/s$) and whose energy intensity in this direction is 1⁄683 Watt/steradian.

The frequency chosen corresponds to a wavelength of 555 nm, in the green-yellow part of the visible spectrum, which is close to the human eye's maximum sensitivity to daylight.

The power emitted by a surface $S$ in the direction of a solid angle $d\Omega$ directed by $\overrightarrow{u}$ is denoted $d\varphi_{\overrightarrow{u}}$.

---

The power intensity in a direction $\overrightarrow{u}$ is defined as the power emitted by a surface $S$ through a solid angle directed by $\overrightarrow{u}$. It is denoted: $L_{\overrightarrow{u}}=\frac{d\varphi_{\overrightarrow{u}}}{d\Omega}$. We have $\varphi=\int_S d \varphi=\int_\Omega d\varphi_{\overrightarrow{u}}$.

---

- Luminance or radiance (Candela /$m^2$ or Watt/steradians/$m^2$)

Luminance or radiance (symbol $L$) is the flux emitted, reflected, transmitted or received by a given surface, per unit solid angle per unit projected area. It is a quantity corresponding to the visual sensation of brightness of a surface. Luminance is the power of visible light passing through or being emitted from a surface element in a given direction, per unit area and per unit solid angle. 

---

The total power emitted by a surface $dS$ directed by $\overrightarrow n$ through a solid angle $d\Omega$  directed by $\overrightarrow{u}$ is denoted $d^2\varphi$ or $d^2\varphi_{\overrightarrow {n},\overrightarrow{u}}$.

The power emitted by a surface $dS$ in all directions is denoted $d\varphi=\iint_\Omega d^2\varphi dS$.

---

The power radiance is defined as the power intensity emitted by the apparent surface $dS_1$ in the direction $\overrightarrow{u}$ : 
$$
L_{\overrightarrow{u}} = \frac{d L_{\overrightarrow{u}}}{dS_{\overrightarrow{u}}}=\frac{d L_{\overrightarrow{u}}}{dS \cos(\alpha)}=\frac{d^2\varphi_:{\overrightarrow{u}}}{d\Omega dS \cos(\alpha)}
$$
A source is isotrope if its luminance is independent of the direction: $L_{\overrightarrow{u}}=L$.

---

- Illuminance or irradiance (lux or Watt/$m^2$)

Illuminance (symbol $I$) is the total flux incident received by a surface, per unit area. It is a measure of how much the incident light illuminates the surface, wavelength-weighted by the luminosity function to correlate with human brightness perception in photometry. Irradiance is also named *Intensity of light* or *Power density* at a specific surface element $dS$. It is given by:
$$
I = \int_{2\pi}L_{\overrightarrow{u}} cos(\theta)
$$

---

<img src="img/image-20240406045425557.png" alt="image-20240406045425557" width="50%"/> 

---

The power illuminance or irradiance ("éclairement" in French) is the symmetric of the emittance: it's the power received by a surface $dS_2$ from all directions.

- Luminous radiant exitance / emittance (Watt/$m^2$)

---

### I.4 Solid angle

Radiations propagate along 3D angles.  As shown on the next figure, the quantity of radiation passing through the surface $S$ at distance $d$, is the same than the quantity of radiation passing through the surface $4S$ at distance $2d$ and similarly the same than the quantity of radiations passing through a surface $9S$ at distance $3d$

<img src="img/image-20240428022746488.png" alt="image-20240428022746488" width="70%" />

---

Angles matter but in a 3D space. Radians are used to measure angles in the plan, steradians are defined by extension in a 3D space:

<img src="img/steradians.png" alt="" width="100%" />

---

A solid angle can be seen as the apparent surface of remote. Radiations rather propagates according to a cone:

<img src="img/cone.png" alt="" width="40%" />

---

The surface of the cap cut in the sphere by the cone is given by: $S=2\pi d h$ and the resulting solid angle is $\Omega=\frac{2\pi h}{R}$. Introducing the angle $\theta$ thanks to $cos(\theta)=\frac{d-h}{d}$, which is equivalent to $\frac{h}{d}=1-cos(\theta)$. It leads to:
$$
\Omega=2 \pi (1-cos(\theta))
$$

---

Consider now that radiations hit a surface, which is not perpendicular to the propagation direction:

<img src="img/cone2.png" alt="" width="50%" />

---

The solid angle is given by:
$$
\Omega = \iint_S sin(\alpha) d\theta d\alpha
$$

---

More formally, a solid angle $\Omega$ is the apparent surface directed by $\overrightarrow n$ at a unary distance of a surface $dxdy$ located at a distance $d$ along the direction $\overrightarrow{u}$:
$$
d\Omega=\frac{dxdy}{d^2}\overrightarrow{u}.\overrightarrow n=\frac{dxdy}{d^2}cos(angle(\overrightarrow{u},\overrightarrow n))
$$

<img src="img/image-20240402075236078.png" width="70%"/>

---

A solid angle $\Omega\in[0,4\pi]$​​ is expressed in steradians (it is equal to $2\pi$ for an canonical hemisphere with a radius equal to 1 because the surface of a sphere is given by $4\pi r^2$). For a cone starting from the origine of $\overrightarrow{u}$, whose half angle at the vertex if $\alpha$, it leads to: $\Omega = 2\pi(1-\cos(\alpha))$​

Let's compute the variation of energy received by Earth from the Sun.

---

<br/><br/><br/>

# **Model of the Sun Earth system**
Sun is our only power source

---

Different irradiances use to be defined:

The **total solar irradiance** (TSI) is a measure of the solar power over all wavelengths per unit area incident on the Earth's upper atmosphere. It is measured perpendicular to the incoming sunlight. In section (I.2) named Aphelion and Perihelion, the solar radiation reaching Earth is: $1367W/m^2$.

---

The **direct normal irradiance** (DNI), or beam radiation, is measured at the surface of the Earth at a given location with a surface element perpendicular to the Sun direction. It excludes diffuse solar radiation (radiation that is scattered or reflected by atmospheric components). Direct irradiance is equal to the extraterrestrial irradiance above the atmosphere minus the atmospheric losses due to absorption and scattering. Losses depend on time of day (length of light's path through the atmosphere depending on the solar elevation angle), cloud cover, moisture content and other contents. The irradiance above the atmosphere also varies with time of year because the distance to the Sun varies, although this effect is generally less significant compared to the effect of losses on DNI.

---

The **diffuse horizontal irradiance** (DHI), or diffuse sky radiation is the radiation at the Earth's surface from light scattered by the atmosphere. It is measured on a horizontal surface with radiation coming from all points in the sky excluding circumsolar radiation i.e. radiation coming from the sun disk. There would be almost no DHI in the absence of atmosphere.

---

The **global horizontal irradiance** (GHI) is the total irradiance from the Sun on a horizontal surface on Earth. It is the sum of direct irradiance (after accounting for the solar zenith angle of the Sun z) and diffuse horizontal irradiance: GHI=DHI+DNI×cos(z)

The **global tilted irradiance** (GTI) is the total radiation received on a surface with defined tilt and azimuth, fixed or sun-tracking. GTI can be measured or modeled from GHI, DNI and DHI. It is often a reference for photovoltaic power plants, while photovoltaic modules are mounted on the fixed or tracking constructions.

---

The **global normal irradiance** (GNI) is the total irradiance from the Sun at the surface of Earth at a given location with a surface element perpendicular to the Sun.

![height:12cm](figs/gni.png)

---

The position of the sun is represented by 2 angles: the azimuth, the angle formed by a vertical plan directed to the south and the vertical plan where the sun is i.e. the azimuth angle, and the altitude (or elevation sometimes) of the sun formed by the horizontal plan tangent to earth and the horizontal where the sun is with __0° means directed to the south, according to the buildingenergy convention. Regarding azimuth, east is negative and west positive. Altitudes 0° and 90° stand respectively for horizontal and vertical positions.__. 

---

![](img/image-20240406071636102.png)

---

Every day, the earth receives a large amount of solar energy. The strength of this radiation reaching Earth depends on a number of factors, including weather conditions and atmospheric scattering (dispersion, reflection and absorption). At the sun's average distance from the earth, a surface normal to solar radiation (perpendicular to it) outside the atmosphere receives around $TSI=1367 W/m²$.  This irradiance is called the solar constant: the solar constant expresses the amount of solar energy that a surface of $1 m^2$​​​ at the average distance from the Earth to the Sun, exposed perpendicular to the sun beams, would receive in the absence of an atmosphere. For the Earth, this is the energy flux density at the top of the atmosphere. 

---

### Solar time

Administrative time differs from solar time: let's pass from one to another.

Let's first convert __administrative time in the day to the universal time coordinated (UTC) expressed in seconds___ i.e. the world reference time at Greenwich meridian.

```python
utc_datetime = administrative_datetime.astimezone(utc)
utc_timetuple = utc_datetime.timetuple()
day_in_year = utc_timetuple.tm_yday

UTC_time_in_seconds = utc_timetuple.tm_hour * 3600 + utc_timetuple.tm_min * 60 + utc_timetuple.tm_sec
```

---

Then, the ___local solar time__ is deduced :

```python
local_solar_time_seconds = UTC_time_in_seconds + longitude_rad / (2*pi) * 24 * 3600
```

---

Time correction are applied to take into account __seasonal variation of sun speed__, __eccentricity of sun trajectory__ and the __inclination of the earth axis__. 

The true solar day is the time that elapses between two consecutive passages of the center of the solar disk in the plane of the same terrestrial meridian.

The corresponding rotation of the Earth is called a synodic revolution. These __true solar days are unequal__ for several reasons: mainly because of the __variation of the Earth's speed on its orbit__ (2nd Kepler's Law) and because of the __inclination of the ecliptic on the equator__. 

---

The average solar day has a duration equal to the annual average of the duration of the true solar day. This duration has been fixed at 24 hours and is used to define the duration of the second: 1 second = 1/86400 mean solar day.

```python
time_correction_equation_seconds = 229.2 * (0.000075
+ 0.001868*cos(2*pi*day_in_year/365) 
- 0.032077*sin(2*pi*day_in_year/365) 
- 0.014615*cos(4*pi*day_in_year/365) 
- 0.04089*sin(4*pi*day_in_year/365))
```

---

actual_solartime_in_secondes = local_solar_time_seconds - time_correction_equation_seconds

<img src="img/image-20240406121316116.png" width="60%">

---

### Solar angles

Given the earth's elliptical trajectory around the sun, with the greatest distance occurring on July 3rd at around $153.10^6km$ and the smallest distance occurring on January 3rd at around $147.10^6km$, this constant varies by $\pm 3.4\%$ with a maximum in January at around 1413 W/m² and a minimum in June at around 1321 W/m². 

---

![bg fit](img/2880px-Seasons1.svg.png)

---

The total solar irradiance at the entrance of the atmosphere is given by:
$$
TSI = \bar L_{sun} \left(1 + 0.033 \cos\left(\frac{2\pi (1+\text{day\ in\ year)}}{365.25} \right)\right)\\
$$
<img src="img/image-20240406131146870.png" width="50%"/>

---

$\bar L_{sun}$ stands for the average yearly radiance of the sun at the atmosphere external surface :
$$
\bar L_{sun}=\sigma T^4 \left(\frac{4\pi R^2}{4\pi d}\right)^2=1367W/m^2
$$
where $\sigma=5.67\ 10^{-8}W/m^2/K^4$ is the Stefan-Boltzmann constant, $R=696.10^6m$, the sun radius and $d=149.6\ 10^9m$​​ the average distance between the sun and Earth.

---

The axis of earth rotation about itself is inclined by a declination equal to 23.45° to the azimuthal axis. Azimuthal axis is the axis perpendicular to the plane intersecting the centers of sun and earth, called the azimuthal plane. This inclination causes the occurrence of seasons on earth.

---

<img src="img/image-20240406074505940.png" width="60%"/>

<img src="img/image-20240406135930089.png" width="70%"/>

---

<img src="img/image-20240406135609252.png" width="70%"/>

---

```
declination_rad = 23.45/180*pi*sin(2*pi*(day_in_year+284) / 365.25)
```

<img src="img/image-20240406135336350.png" width="70%"/>

---

The hour angle describes the seeming movement of the Sun around Earth. Its value shows the seeming place of the Sun towards east or west compared to the local meridian. Its value is negative in the morning and positive in the afternoon and 0 when the Sun is right on the meridian:

```python
solar_hour_angle_rad = 2*pi * (solartime_secondes / 3600 - 12) / 24
```

---

The height of the Sun above the horizon is given by the solar altitude. Solar altitude changes continuously, its value is 0 at sunrise and sunset while its maximum is reached at culminate. Culmination height is also different in every season:

```python
altitude_rad = max(0, asin(sin(declination_rad) * sin(latitude_rad) 
    + cos(declination_rad) * cos(latitude_rad) * cos(solar_hour_angle_rad)))
```

---

The azimuth angle of the Sun is the angle between solar rays and the North–South direction of the earth. It is calculated based on the following equation for which we have to know geographical latitude, declination angle, hour angle and solar altitude:

```python
cos_azimuth = (cos(declination_rad) * cos(solar_hour_angle_rad) * sin(latitude_rad) - sin(declination_rad) * cos(latitude_rad))
     / cos(altitude_rad)
sin_azimuth = cos(declination_rad) * sin(solar_hour_angle_rad) / cos(altitude_rad)
azimuth_rad = atan2(sin_azimuth, cos_azimuth)
```

---

Plotting azimuth and altitude angles, at local position (latitude and longitude), the 21st of each month leads to the heliodon with the azimuth angles on the x-axis and the altitude angle on the y-axis:

---

![bg fit](img/image-20240406071833301.png)

---

### I.8 Transmission of solar radiation

If the sun were not hidden by Earth, its altitude would belong to [0,180°]. The total solar irradiance is affected by how solar beams spread out as it travels through the atmosphere. Thermosphere temperatures pass 2000°C due to solar radiation... it burns spatial dusts and meteors. The high temperature in the thermosphere is due to the absorption of high-energy solar radiation, primarily extreme ultraviolet. Unlike the lower atmosphere, the thermosphere is thin enough for extreme ultraviolet radiation from the Sun to pass through without being completely blocked. 

---

This high-energy radiation gets absorbed by the thermosphere's molecules, causing them to vibrate more intensely, translating to a significant rise in temperature. The thermosphere is the least dense layer of the atmosphere. While the temperature reading might seem very high, it's important to remember that temperature is a measure of the average kinetic energy of gas molecules. With so few molecules in the thermosphere, even a small amount of energy absorbed can result in a large increase in temperature per molecule. 

---

The temperature in the __thermosphere__ isn't constant. It's heavily influenced by solar activity (see Milankovitch cycles). During periods of high solar activity, with more extreme ultraviolet radiation being emitted, the thermosphere can become significantly hotter. This can cause the layer to expand upwards. In essence, the __thermosphere acts like a super-sparse gas that gets energized by absorbing the Sun's high-energy rays__.  This unique combination leads to the seemingly counterintuitive phenomenon of the hottest layer being at the very top of the atmosphere. 

---

__Stratosphere__ includes __the ozone layer__ (19-30 km): the temperature increases due to __UV-absorption__. 

Ozone acts like __a shield, absorbing most of the sun's harmful ultraviolet B and ultraviolet C radiation__.  Molecules and particles in the atmosphere, primarily in the stratosphere and mesosphere, can __scatter sunlight__.  This scattering is what __causes the sky to appear blue__.  Blue wavelengths are more easily scattered by these particles than other colors, which is why the sky appears predominantly blue.

---

![bg fit](img/std-atmos-temperature-color.png)

---

Passing through the atmosphere, ~30% solar radiation is reflected, ~17% is absorbed. Only 53% reaches the earth surface as direct radiation: ~31% and diffuse radiation: ~22%.

<img src="img/image-20240409080837668.png" width="70%"/>

---

The __Rayleigh length__ is a distance used to characterize this phenomenon. It's __the distance a beam travels__ from its narrowest point (beam waist) to the point where the beam's cross-sectional area has doubled. It is given by:

```python
rayleigh_length = 1 / (0.9 * relative_optical_air_mass + 9.4)
```

where the __relative optical air mass__ is defined as __the ratio of the air mass actually flown through to the air mass at vertical incidence__: it depends on the solar altitude and on the ground pressure. If not available, the ground atmospheric pressure can be estimated with: $P_{atm}=101325(1-2.26\ 10^{-5}h)^{5.26}$  where $h$ stands for the local ground elevation. 

---

The relative optical air mass is given by:

```python
relative_optical_air_mass = ground_atmospheric_pressure 
    / (101325 * sin(altitude) + 15198.75 * (3.885 + 57.296 * altitude)**(-1.253))
```

---

The __transmitivity coefficient__ represents __the fraction of the incident radiation  at the surface of the atmosphere that reaches the ground along a vertical path i.e.  that is not scattered__.  

```python
transmitivity_coefficient = 0.6**Mh
```

where $M_h$ reflects the variation in atmospheric thickness as a function of the time of day. 

---

The Piedallu & Gégout formula is used:

```python
M0 = sqrt(1229 + (614*sin(altitude))**2) - 614*sin(altitude_rad)
Mh: float = (1-0.0065/288*elevation)**5.256 * M0
```

Mh is a correction factor linked to atmospheric pressure, which depends on elevation of the ground (particularly useful in mountainous areas).

---

The Linke factor models the disturbances due to steam, fog or dust. 

```python
l_Linke = 2.4 + 14.6 * pollution + 0.4 * (1 + 2 * pollution) * log(partial_steam_pressure)
```

where Pollution = 0.002 in mountains, 0.05 in countryside, 0.1 in urban area and 0.2 in highly polluted area. 

The fog is deduced from the partial steam pressure:

```python
partial_steam_pressure = 2.165 * (1.098 + temperature/100)**8.02 * humidity
```

where humidity and temperature can be obtained from the local meteorology.

---

Eventually, the direct normal irradiance is given by:

```python
direct_normal_irradiance = transmitivity_coefficient * normal_atmospheric_irradiance_with_clouds * 
    exp(-relative_optical_air_mass * rayleigh_length * l_Linke)
```

which is affected by clouds:

```python
normal_atmospheric_irradiance_with_clouds = (1 - 0.75 * (nebulosity_percent/100) ** 3.4) 
    * total_solar_irradiance
```

Nebulosity is the percentage of the sky covered by clouds. 

---

![bg fit](img/image-20240409051306844.png)

---

### Incident surface

Let's introduce 2 new angles to model the direction of a flat solar collector lying on Earth: the exposure and the tilt angle.

![image-20240406071935707](img/image-20240406071935707.png)

---

The incidence angle between the solar angle and an inclined surface, defined by an exposure angle and a tilt angle,  can be calculated with:

```python
incidence_rad = acos(cos(pi/2-altitude_rad) * cos(slope_rad) +
     sin(pi/2-altitude_rad) *  sin(slope_rad) * cos(azimuth_rad - exposure_rad))
```

---

In the direction of the sun, we get:

```python
direct_tilt_irradiance = cos(incidence_rad-pi) * direct_normal_irradiance
```

<img src="img/schema-EN_2400x1160.jpg" width="80%"/>

---

Clouds diffuse a part of the solar beams in all directions:

```python
diffuse_tilt_irradiance = max(0, normal_atmospheric_irradiance_with_clouds *
     (0.271 - 0.294 * transmitivity_coefficient**Mh) * sin(altitude)) 
```

---

<img src="img/image-20240409063919994.png" width="60%"/>

```python
reflected_tilt_irradiance = max(0, albedo * normal_atmospheric_irradiance_with_clouds * (0.271 + 0.706 * transmitivity_coefficient**Mh) * sin(altitude) * sin(slope_rad / 2) ** 2)
```

---

## Long wave radiation due to celestial vault (atmosphere)

For the sky and the equivalent ground (see schema), the corresponding solid angles are calculated.

Let's denote $h$, $l$, $s_{wall}$ and $\beta$ are respectively the height, the width, the surface and the slope of the wall.

---

![bg fit](figs/image-20240518213407769.png)

---

$$
d\varphi_{wall,sky}=(L_{wall}-L_{sky})\cos(\alpha) d\alpha d\omega dx dy
$$
with the bounds:
$$
x\in[0,h], y\in[0,l]\\

\alpha\in\left[\beta-\pi/2, \pi/2\right]\\

\omega = [0,\pi]
$$
Therefore,
$$
\varphi_{wall,sky}=(L_{wall}-L_{sky})\pi l\int_0^h\int_{\beta-\pi/2}^{\pi/2} cos(\alpha)d\alpha dx\\
\rightarrow
\varphi_{wall,sky}=(E_{wall}-E_{sky})\frac{1+\cos(\beta)}{2} s_{wall}
$$

---

Let's calculate the transfer between the wall and the ground:
$$
d\varphi_{wall,ground}=(L_{wall}-L_{ground})\cos(\alpha) d\alpha d\omega dx dy
$$
with the bounds:
$$
x\in[0,h], y\in[0,l]\\

\alpha\in\left[-\pi/2, \beta-\pi/2\right]\\

\omega = [0,\pi]
$$
Therefore,
$$
\varphi_{wall,ground}=(L_{wall}-L_{ground})\pi l\int_0^h\int_{-\pi/2}^{\beta-\pi/2} cos(\alpha)d\alpha dx\\
\rightarrow
\varphi_{w,g}=(E_{wall}-E_{ground})\frac{1-\cos(\beta)}{2} s_{wall}\\
$$

---

![bg fit](figs/image-20240519144321031.png)

---

Regarding the celestial vault, we know (Bertin and Martin, 1984):
$$
E_{clear}= 0.711+0.56\frac{T_{dp}}{100}+0.73\left(\frac{T_{dp}}{100}\right)^2
$$
$$
E_{cloud}=\epsilon_{cloud} \sigma T_{cloud}^4
$$
$$
E_{sky}=(1-\zeta)E_{clear} + \zeta E_{cloud}
$$
$$
\epsilon_{cloud}=0.96
$$
$$
T_{cloud}=T_{weather}-5
$$
where $\zeta\in [0, 100\%]$ is the nebulosity (or the cloud cover) and $T_{dp}$​ the dew pressure temperature at ground level.

---

Regarding the ground and the wall, we have:
$$
E_{wall}=\epsilon_{wall}\sigma T_{wall}^4
$$

$$
E_{ground}=\epsilon_{ground}\sigma T_{ground}^4
$$

Let's consider that $\epsilon_{wall}=\epsilon_{ground}$ and $T_{wall}=T_{ground}$ i.e. $\varphi_{wall,ground}=0$, it yields:

---

$$\varphi_{wall→sky}=(E_{wall}-E_{sky})\frac{1+\cos(\beta)}{2} s_{wall}$$
with:
$$E_{wall}=\epsilon_{wall}\sigma T_{wall}^4$$
$$E_{sky}=(1-\zeta)\left(0.711+0.56\frac{T_{dp}}{100}+0.73\left(\frac{T_{dp}}{100}\right)^2\right) + 0.96 \zeta \sigma (T_{weather}-5)^4$$
---

- Particular case: wall facing the sky ($\beta = 0$)
    $$
    \varphi_{wall,sky}=(E_{wall}-E_{sky}) s_{wall}
    $$

    $$
    \varphi_{wall,ground}=0\\
    $$

- Particular case : vertical wall ($\beta=\pi/2$​)
    $$
    \varphi_{wall,sky}=\frac{E_{wall}-E_{sky}}{2}s_{wall}
    $$

    $$
    \varphi_{wall,ground}=\frac{E_{wall}-E_{ground}}{2}s_{wall}
    $$


---

### Underground temperature

At around 10m deep, the underground temperature is worth about the average air outdoor temperature ($\approx 13°$). For the sake of simplicity, we will assume that the other side of the slab is at the average air outdoor temperature.

[Marchio et Reboux 2003] proposed a simple model based on year temperature profile (other approaches can be found in Stephane Thiers's PhD thesis, 2008):

---

$$
T_\text{ground}(t, z)=\bar T - \frac{\Delta_T}{2} e^{-\frac{z}{\delta}}\cos\left(2\pi\frac{t-\hat t}{D_\text{year}}-\frac{z}{\delta}\right)
$$

where time and durations are in seconds:

- $\delta=\sqrt{\frac{\lambda_{ground}D_\text{year}}{\rho_{ground}c_{ground}\pi}}$, the depth of penetration of the surface year wave temperature with $\lambda_{ground}$, the thermal conductivity of the ground, $\rho_{ground}$, the ground density and $c_{ground}$, the specific heat of the ground
- $\bar T$, the average year temperature, and $\Delta_T$, the maximum temperature amplitude when the year temperature is assimilated to a wave

---

- $D_{year}=3600\times24\times365$​ secondes (31,536,000 secondes)
- $t$, the time in seconds starting at January 1st
- $\hat t$, the time in seconds of the surface wave temperature peak time

Consider for instance a ground characterized by: $\rho_\text{gound}=1200kg/m^3$, $c_\text{ground}=1000J/kg.K$ and $\lambda=0.6W/m.K$ located around Grenoble.

Let's first fit a surface year wave temperature centered on the average year temperature (+14°C) and with the minimum and maximum temperature respectively at February 1st and August first (6 months must seperate these dates).

---

![bg fit](figs/ground_surf.png)

---

![bg fit](figs/depth.png)

---

<br/><br/><br/>
# Open and explore the workshop01_sun.ipynb
