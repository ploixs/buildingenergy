---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

![bg left:40% 80%](https://ense3.grenoble-inp.fr/uas/alias23/LOGO/Grenoble+INP+-+Ense3+%28couleur%2C+RVB%2C+120px%29.png)

# **Modeling buildings from physics**
stephane.ploix@grenoble-inp.fr

---

![bg fit](figs/heat_fluxes_walls.png)

---

## Conduction within solids

<img src=figs/conduction.gif width=70%/>  

---

To make it easy, a simplified approach is proposed by the ThBAT (official RT2012 and RE2020 method for thermal calculation), with typical resistance values for convection and radiation valid in most situations.

Nevertheless, the physics from which it comes from will be developed:
1. to figure out the underlying assumptions
2. to be able to extend to special cases

---

The general isotropic heat conduction equation is given by:
$$
\text{div}( \overrightarrow{\text{grad}(T(X, t))}) + \frac{1}{\lambda}Q(X,t) = \frac{\rho c}{\lambda} \frac{\partial T(X,t)}{\partial t} 
$$

with $\rho$, the  density (kg/m$^2$), $\lambda$ the conductivity (W/m.K), c the material specific heat capacity (J/kg.K), T(X,t) the temperature (K) at position $X$ and time $t$ and $Q(X,t)$ the heat power. 

$\frac{\rho c}{\lambda}$ is the __thermal diffusivity__: the larger, the faster the heat will diffuse. The __volumetric heat storage capacity__ is given by: $\rho c$

---

<style scoped>
table {
  font-size: 20px;
}
</style>

|material|conductivity (W/m.K)|	specific heat capacity (J/kg.K)|	density (kg/m3)|	emmisivity coefficient|	thermal diffusivity (mm$^2$/s)	|volumetric heat capacity (J/m3/K)|
|--------|--------------------|---------------------------------|----------------|-----------------------------------------|---------------------------------|---|
|*immobile CO2*	|0.01	|1,005	|1.56	|0	|8.92	|1570|
|*immobile air*	|0.02	|1,005	|1.26	|0	|19.03	|1261|
|polyurethane foam	|0.03	|1,000	|30	|0.82	|0.97	|30000|
|dry glass wool	|0.05	|840	|25	|0.97	|2.38	|21000|
|asbestos (amiante)	|0.15	|816	|2,400	|0.93	|0.08	|1958400|
|lightweight wood	|0.04	|1,300	|400	|0.95	|0.08	|520000|
|heavyweight wood	|0.12	|2,400	|890	|0.95	|0.06	|2136000|
|plaster	|0.35	|1,090	|2,300	|0.98	|0.14	|2507000|
|water	|0.60	|4,180|	997|	0.95|	0.14	|4167460|
|glass	|1	|753	|2,600	|0.94	|0.51	|1957800|
|full brick	|1	|840	|2,000	|0.93	|0.60	|1680000|
|concrete	|0.5	|880	|2,000	|0.85|	0.28	|1760000|

---

<style scoped>
table {
  font-size: 20px;
}
</style>

|material|conductivity (W/m.K)|	specific heat capacity (J/kg.K)|	density (kg/m3)|	emmisivity coefficient |	thermal diffusivity (mm$^2$/s)	|volumetric heat capacity (J/m3/K)|
|--------|--------------------|---------------------------------|----------------|-----------------------------------------|---------------------------------|---|
|graphite	|5	|780	|2,500|	0.96	|2.56	|1950000|
|stainless steel	|15	|440	|7,820|	0.07|	4.36|	3440800|
|lead	|35	|129|	11,350	|0.43	|23.90	|1464150|
|iron	|57	|449|	7,200|	0.44	|17.63	|3232800|
|aluminium	|203	|897	|2,700	|0.07	|83.82	|2421900|
|copper	|380	|385|	8,790|	0.03	|112.29|	3384150|
|silver	|410	|235|	10,500	|0.03	|166.16|	2467500|
|wood	|0.20	|1,300|	700|	0.95	|0.22	|910000|
|PVC	|0.25	|1,046.00	|1,395.00|	0.92	|0.17	|1459170|

For more materials, see ```/buildingenergy/data/propertiesDB.xslx```

---

The Fourier empirical law provides a solution:
$$
\overrightarrow{\varphi(X,t)} = -\lambda\overrightarrow{ grad(T(X,t))}
$$

Introducing the Fourier law into the general isotropic heat conduction equation, gives:
$$
\rho c \frac{\partial T(X,t)}{\partial t}  = -\text{div}( \overrightarrow{\varphi(X, t)}) + Q(X,t) 
$$

In steady state ($t=t_{\infty}$) without internal heat, it yields:
$$
\text{div}( \overrightarrow{\varphi(X, t)})=0 \rightarrow \overrightarrow{\varphi(X, t)} = \Phi
$$

---
For a very small element:$\Phi = -\lambda \begin{bmatrix} \frac{\partial T}{\partial x} \\ \frac{\partial T}{\partial x} \\ \frac{\partial T}{\partial x} \end{bmatrix}$

For a single layer wall (sum up over a close area) of surface $S$ and thickness $e$:

$$\phi = \frac{\lambda S}{e} (T_1 - T_2)$$

---

![width:10cm](figs/single_layer_wall.png)

The transmitivity coefficient is: $U=\frac{\lambda}{e}$ in $W/m^2.K$
 The thermal resistance is $R=\frac{e}{\lambda}$ in $m^2.K/W$ or $R=\frac{e}{\lambda S}$ in $K/W$

---

 A single layer can be modeled by a electrical resistance where temperatures are equivalent to voltages and currents to heat flows:

 ![width:15cm](figs/resistor.png)
 __Remark: Because of the minus sign, the first term of the temperature subtraction is at the opposite side of the heat flow direction.__
 It's named a nodal model: each node = a temperature.

---

Let's consider a multi-layer wall:

![width:13cm](figs/multilayer.png)

---

Sometimes, it could be discontinuity in the temperature variation: it's modelled by contact resistances:

![width:4cm](figs/contact.png)

---
### Modeling radiative exchanges between walls

As seen before, every surface made of a given material at a given temperature (in Kelvin for radiative exchanges) is behaving more or less like a black body i.e. a wall is emitting and exchanging power with the other walls. 

<img src=figs/photon-emission-absorption.jpg  width=50%/>

---

Different approaches more or less accurate but also, more or less complex, can be adopted. The most complex and accurate is to compute shape factor of each surface relatively to the other i.e. how a surface is visible by another but this leads to complex calculations (some tables gathering common configurations can be found in the scientific literature).


An easier way is to consider each wall is exchanging with a virtual wall facing it at indoor air temperature, or with a virtual wall at outdoor air temperature (or at sky temperature), having the same emissivity.

---

![bg fit](figs/radiative_principle.png)

---

Instead of calculated the radiative heat directly exchange between wall 1 and wall 3 (for example), one computes the  heat exchange between wall 1 and the virtual wall $wall_1^*$ at a indoor air temperature $T_{in}$ and similarly, between wall 3 and the virtual wall $wall_3^*$ at a indoor air temperature $T_{in}$.

$$\varphi_1 = \sigma \epsilon_1 T_1^4-\sigma \epsilon_1 T_{in}^4 \approx 4\sigma \epsilon_1\bar T_K^3(T_1-T_{in})= h_{r,1}(T_1-T_{in})$$

with:
- $\bar T_K$, an average temperature in Kelvin ($T_K=T_C + 273.15°C$)
- $h_{r,1}=4\sigma \epsilon_1\bar T_K^3$, the radiative heat transfer coefficient for wall 1
- $\sigma$, the Stefan-Boltzmann constant: $5.670374419\times 10^{-8}W/m^2.K^4$
  
---
![bg fit](figs/approx.png)

---

__As a summary__, we have, for each wall with a surface temperature $T_1$:

$$\varphi_1 = h_{r,1}(T_1-T_{in})$$

with:
- $\bar T_K$, an average temperature in Kelvin 
  ($\bar T_K=293K$ for indoor, $\bar T_K=286K$ for outdoor)
- $h_{r,1}=4\sigma \epsilon_1\bar T_K^3$, the radiative heat transfer coefficient for wall 1
- $\sigma$, the Stefan-Boltzmann constant: $5.670374419\times 10^{-8}W/m^2.K^4$

---
__Reminder from "slides01_sun.md"__:

Few emissivity values are given below. The file [propertiesDB.xlsx](../propertiesDB.xlsx) provide much more values. 


<img src="img/image-20240428005608289.png" alt="image-20240428005608289" width="50%" />

---

### Modeling convection in air boundary layers and cavities

When a surface is in contact with a gas, in particular air, at a  temperature higher or lower than that of the surface, a  layer of gas adjacent to the surface gets heated or  cooled. A density difference is created between this layer and the surface surrounding it. The density  difference introduces a buoyant (poussée d'Archimède) force causing flow  of gas near the surface. Heat transfer under such  conditions is known as __free or natural convection__.  Thus “Free or natural convection is the process  of heat transfer which occurs due to movement of  the fluid particles by density changes associated  with temperature differential in a fluid.”

---
<img src=figs/boiling-atoms-gif.gif width=80%/>

---
![bg fit](figs/radiative_phenomena.png)

---

In building heat transfer, convection is the process by which heat is transferred through the movement of air or fluid within the building. Convection occurs in two main forms: natural (or free) convection and forced convection. 

1. Natural Convection relies on temperature differences within the air. When air is heated, it becomes less dense and rises, while cooler, denser air descends. This creates a circulation pattern.

Example: In a heated room, air near a radiator or heating source warms up, rises to the ceiling, and spreads across the room. As it cools, it falls, allowing warmer air to take its place, creating a loop.

---

2. Forced Convection occurs when fans, blowers, pumps or simply wind actively move air across surfaces, improving heat transfer by continuously replacing air near surfaces with air at a different temperature.

Example: HVAC systems use fans to push warm or cool air through ducts, delivering it to different areas within a building. This forced circulation accelerates the heat transfer process and maintains consistent indoor temperatures.

---

## Convection along surfaces

Immobile air does not exist because of convection i.e. conduction only applies to solid: the conductivity value ($\lambda$) can not be used directly as for solids. Insulation is a material made of lots of small alveoli to immobilize air. Therefore, all the insulation materials have a $\lambda$ close but higher than the $\lambda$ of the immobile air ($\lambda_\text{air}$=0.024W/mK @0°C, $\lambda_\text{air}$=0.025W/mK @20°C).

The model use is named Newton's law:
$$
\varphi_\text{conv}=h_c S(T_\text{surf} - T_{air})
$$
where $h_c$ is named convective heat transfer coefficient.

---
For natural convection,
$$
h_c=\frac{\lambda N_u}{L}
$$
with 
- $N_u=CR_a^n$, the Nusselt dimensionless number, whose constant $C$ and $n$ depends on the problem to solve (for instance, $C=0.59$ and $n=0.25$),
- $\lambda$, the thermal conductivity of air in W/m.K
- $R_a$, the Rayleigh dimensionless number for air
- $L$, a typical length, for instance, the height of a wall

---

The Rayleigh number for air in a typical indoor environment can vary widely depending on factors such as the temperature difference between the wall and the air, the height of the surface, and the properties of the air.

It is calculated like this:

$$
R_a= \frac{g \beta (T_{ref}-T_{other}) L^3}{\nu \alpha}
$$
where:
...

___
- $T_{ref}$ and $T_{other}$ are 2 temperatures of interest depending on the problem to solve (wall, cavity,...)
- $g$ is the acceleration due to gravity (9.81 m/s²)
- $\beta$ is the thermal expansion coefficient of air ($\approx 1/\bar T_K$) with $\bar T_K$ an average temperature in Kelvin.
- $L$ is a typical height in the problem to solve
- $\nu$ is the kinematic viscosity of air
- $\alpha$ is the thermal diffusivity of air
---
Kinematic viscosity (in $m^2/s$) of air depends on temperature. It can be deduced from:
<p style="font-size: 16px;">

| T(°C) | -25 | -15 | -10 | -5 | 0 | 5 | 10 | 15 | 20 | 25 | 30 | 40 |
|---|---|---|---|---|---|---|---|---|---|---|---|---|
$\nu$| 11.18e-6 | 12.01e-6 | 12.43e-6 | 12.85e-6 | 13.28e-6 | 13.72e-6 | 14.16e-6 | 14.61e-6 | 15.06e-6 | 15.52e-6 | 15.98e-6 | 16.92e-6 |
</p>

Thermal diffusivity (in $m^2/s$ too) of air depends also on temperature:
<p style="font-size: 16px;">

| T(°C) | -53.2 | -33.2 | -13.2 | 0 | 6.9 | 15.6 | 26.9 | 46.9 |
|---|---|---|---|---|---|---|---|---|
| $\alpha$ | 12.6e-6 | 14.9e-6 | 17.3e-6 | 19e-6 | 19.9e-6 | 21e-6 | 22.6e-6 | 25.4e-6 |
</p>

Thermal conductivity (in $W/m.K$) of air also depends on temperature:
<p style="font-size: 14px;">

| T(°C) |-50 | -25 | -15 | -10 | -5 | 0 | 5 | 10 | 15 | 20 | 25 | 30 | 40 | 50 |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---
| $\lambda$ | 20.41e-3 | 22.41e-3 | 23.20e-3 | 23.59e-3 | 23.97e-3 | 24.36e-3 | 24.74e-3 | 25.12e-3 | 25.50e-3 | 25.87e-3 | 26.24e-3 | 26.62e-3 | 27.35e-3 | 28.08e-3 |
</p>

---
![bg fit](figs/laminar_turbulent_flow.jpg)

---
The Rayleigh number is the ratio of thermal transport via diffusion wrt thermal wrt thermal transport via convection:
- $R_a<10^3$, air is not moving and the conduction is dominating
- $10^3\le R_a \le 10^9$, air is behaving mainly as a laminar flow
- $R_a > 10^9$, air is behaving mainly as a turbulent flow

---
The Nusselt number is the ratio of total heat transfer to conductive heat transfer at a boundary. It determines whether a flow condition is rather immobile, laminar or turbulent:
- $N_u\le 1$, conduction mainly (air is immobile)
- $1<N_u< 100$, laminar flow mainly
- $N_u > 100$, turbulent flow mainly

---
#### Free convection along a vertical (indoor) wall

$$R_c=\frac{1}{h_c S}$$
with $S$ is the wall surface, and
$$
h_c=\frac{\lambda N_u}{L}
$$
or, with radiative transfer, the thermal resistance of the whole boundary air layer:
$$R_L=\frac{1}{(h_c+h_r) S}$$

---
The Nusselt number is given by:
- $R_a \le 10^4$, $N_u=1$, conduction is dominating
- $10^4 < R_a < 10^9$, $N_u=0.68+\frac{0.67R_a^{0.25}}{\left(1+\frac{0.492}{P_r}\right)^{0.25}}$, laminar flow mainly
- $R_a \ge 10^9, N_u=0.1 R_a^{0.33}$, turbulent flow mainly

where $P_r\approx 0.71$ is the Prandtl number.

The thickness $\delta$ of the resulting air layer is given by:

$$
\delta = \frac{L}{R_a^{0.25}}
$$

---

Radiative transfer included (emissivity=0.9)
![bg  width:30cm](figs/indoor_vertical_surface1.png)

---
#### Free convection along a horizontal (indoor) floor

$$R_c=\frac{1}{h_c S}$$
with $S$ is the floor surface, and $h_c=\frac{\lambda N_u}{L}$

With radiative transfer, the thermal resistance of the whole boundary air layer:
$$R_L=\frac{1}{(h_c+h_r) S}$$

---
For an ascending flow, the Nusselt number is given by:
- $R_a \le 10^4$, $N_u=1$, conduction is dominating
- $10^4 < R_a < 10^7$, $0.54 R_a^{0.25}$, laminar flow mainly
- $R_a \ge 10^7, N_u=0.15 R_a^{0.33}$, turbulent flow mainly

For descending flow, $N_u=1$

---
Radiative transfer included (emissivity=0.9)
![bg width:30cm](figs/indoor_horizontal_surface.png)

---
#### Free convection inside a vertical cavity

$$R_c=\frac{1}{h_c S}$$
with $S$ is the cavity surface, $h_c=\frac{\lambda N_u}{L}$ and $N_u=C {R_a}^n$

With radiative transfer, the thermal resistance of the whole boundary air layer:
$$R_L=\frac{1}{(h_c+h_r) S}$$

---
The coefficient $C$ and $n$ involved in the Nusselt number is given by:
- $R_a \le 10^3$, $N_u=1$, conduction is dominating
__laminar flow mainly__
- $10^3 < R_a < 10^4$, $C=1.18, n=0.125$
- $10^4 < R_a < 10^7$, $C=0.54, n=0.25$
- $10^7 < R_a < 10^9$, $C=0.15, n=0.33$
__turbulent flow mainly__
- $R_a \ge 10^9$, $C=0.10, n=0.33$

---
Radiative transfer included (emissivity=0.9)
![bg  width:30cm](figs/vertical_cavity1.png)

---
Radiative transfer included (emissivity=0.9)
![bg  width:30cm](figs/vertical_cavity2.png)

---
#### Free convection inside a horizontal cavity

$$R_c=\frac{1}{h_c S}$$
with $S$ is the cavity surface, $h_c=\frac{\lambda N_u}{L}$ and $N_u=C {R_a}^n$

With radiative transfer, the thermal resistance of the whole boundary air layer:
$$R_L=\frac{1}{(h_c+h_r) S}$$

---
The Nusselt number is given by:
__hot floor on top, conduction is dominating__
- $R_a \le 10^3$, $N_u=1$
__laminar flow mainly__
- $10^3 < R_a < 10^7$, $N_u=0.54 {R_a}^{0.25}$
__turbulent flow mainly__
- $R_a \ge 10^7$, $N_u=0.15 {R_a}^{0.33}$

---
Radiative transfer included (emissivity=0.9)
![bg width:30cm](figs/horizontal_cavity1.png)

---
Radiative transfer included (emissivity=0.9)
![bg  width:30cm](figs/horizontal_cavity2.png)

---
Outdoor, in the presence of wind, we satisfy the __forced convection__ conditions. The Reynolds number ($R_e$) is used to predict the transition to laminar forces. It is a ratio between inertial and viscous forces:
- $R_e<2300$, mainly laminar regime
- $2300 \le R_e \le 4000$, transition regime
- $R_e>4000$, turbulent regime

---
#### Wind against a vertical wall

$$R_c=\frac{1}{h_c S}$$
with $S$ is the wall surface, $h_c=\frac{\lambda N_u}{L}$ and $N_u=0.037 R_e^{0.8} P_r^{0.33}$ ($P_r=0.71$, the Prantl number)

With radiative transfer, the thermal resistance of the whole boundary air layer:
$$R_L=\frac{1}{(h_c+h_r) S}$$

---
The Reynolds number is given by:
$$
R_e = \frac{VL}{\nu}
$$
where:
- $V$ is the wind speed
- $L$ is the height of the wall
- $\nu$ is the kinematic viscosity of air

---
Radiative transfer included (emissivity=0.9), wind speed=8.6km/h
![bg  width:30cm](figs/outdoor_vertical_surface1.png)

---
Radiative transfer included (emissivity=0.9)
![bg  width:30cm](figs/outdoor_vertical_surface2.png)

---
#### Wind against a horizontal roof

$$R_c=\frac{1}{h_c S}$$
with $S$ is the roof surface, $h_c=\frac{\lambda N_u}{L}$ and $N_u=0.036 R_e^{0.8} P_r^{0.33}$ ($P_r=0.71$, the Prantl number)

With radiative transfer, the thermal resistance of the whole boundary air layer:
$$R_L=\frac{1}{(h_c+h_r) S}$$

---
The Reynolds number is given by:
$$
R_e = \frac{WL}{\nu}
$$
where:
- $W$ is the wind speed
- $L$ is the height of the wall
- $\nu$ is the kinematic viscosity of air

---
Radiative transfer included (emissivity=0.9), wind speed W=8.6km/h
![bg width:30cm](figs/outdoor_horizontal_surface1.png)

---
Radiative transfer included (emissivity=0.9)
![bg width:30cm](figs/outdoor_horizontal_surface2.png)

---
## Standard Values to use for RT2012 and RE2020 (emissivities of 0.9 included.)

| where | position | $h_L$ | $R_L$ |
|---|---|---|---|
| indoor | vertical | $7.69W/m^2K$ | $0.13m^2K/W$ |
| indoor | horizontal ascending flow | $10W/m^2K$ | $0.10m^2K/W$ |
| indoor | horizontal descending flow | $5.88W/m^2K$ | $0.17m^2K/W$ |
| outdoor | vertical or horizontal | $5.7V+11.4$ or $25W/m^2.K$ | $1/h_L$ or $0.04m^2K/W$ |


---
Regarding cavities, if $L \le 300mm$), according to the ThBat (Th-U 2.2.1.2), an air gap can be assimilated to variable resistance $R=R(L)$:
| $L(mm)$ | $R (m^2.K/W)$ |
|-------------|------------------|
| 0  | 0 | 0.024 |
| 5  | 0.11 | 9.1 |
| 7  | 0.13 | 7.69 |
| 10 | 0.15 | 6.67 |
| 15 | 0.17 | 5.88 |
| $25 \le L \le 300$ | 0.18 | 5.56 |

---

Applying this to double glazing leads to (without considering the boundary air layers):

![width:25cm](figs/glazing.png)

---
## weak component

Re-consider the previous configuration but instead of a double glass, a single one is used ($R_g=0.004m^2.K/W$ or $U_g=250W/m^2K$ for $1m^2$). Additionnaly, the insulated wall is reinforced with $R_w=4m^2.K/W$ or $U_{w}=0.25W/m^2.K$ for $14m^2$.

The overall system satisfied: $US=250+3.5=253.5W/m^2.K \approx 250W/m^2.K$. Whatever the insulation thickness over the walls, the weak resistance will dominates: during a retrofitting, weak component must be avoided because they determine the thermal losses.

---

![bg fit](figs/weakness.png)

---

In case of complex assembly, it's easier:
- to compute the thermal resistance $R_i$ of each layer of a wall
- to sum them up to get the equivalent resistance of each wall component
- to inverse resistance to get transmitivity coefficient $U_i$
- to sum up the $U_iS_i$ of the components of the same wall (same temperature on both sides)

---
Walls are easy to model because there is only 1 dimension: the thickness but junctions between walls might have to be modeled in 2D, and possibly in 3D:

![bg left width:10cm](figs/bridges.png)

$$
\text{div}(\overrightarrow{\text{grad(T)}})=0
$$
$$
\phi = - \lambda \text{grad(T)}
$$

The 2D and 3D components are named __thermal bridges__.

___

2D thermal bridges must be multiplied by length whereas 1D are punctual (and actually not common).

Using finite element method, thermal bridges are computed as:

![width:20cm](figs/thermal_bridge.png)

--- 

The volume or surface is decomposed into many small elements, that are gathered into big matrices satisfying:
$[M][T]=[N]$.
Re-organizing the vector temperatures at nodes like $T'=\begin{bmatrix}
T_u\\T_k
\end{bmatrix}$, where "u" means "unknown temperature" and "k" stands for "known temperatures". We get:
$$
\begin{bmatrix}M_{u,u},M_{u,k}\\M_{k,u},M_{k,k}\end{bmatrix}\begin{bmatrix}
T_u\\T_k
\end{bmatrix}=\begin{bmatrix}
N_u\\N_k
\end{bmatrix}
$$
The temperatures at unknown are obtained by solving $T_u=M_{u,u}^{-1}\left(N_u-M_{u,k}T_k\right)$. Heat flows (in Watt) are obtained with $\varphi=-\lambda grad(T)$

---

The shape is (1) defined (2) meshed and (3) solved.
![height:16cm](figs/steps.png)

---
Solutions to avoid the heat leaks through thermal bridges are for instance:

![](figs/thermal-bridges.jpg)

---

## Advection (heat transportation)

Consider a room with a quantity of air $Q(t)$ exchanged from indoor at temperature $T_{in}$ with outdoor at temperature $T_{out}$. The heat transported by air $\varphi(t)$ is given by: $\varphi(t)=\rho c_p Q(t) (T_{in}-T_{out})$ It corresponds to a thermal resistance $R_v(t)=\frac{1}{\rho c_p Q(t)}$.
![height:7cm](figs/ventilation.png)

---

Let's consider there is a heat exchanger that recovers $\zeta$ of the heat transported by air. The recovered heat is given by $\zeta \varphi(t)$. Therefore, the thermal resistance of the heat exchanger is $R_{v}(t)=\frac{1}{(1-\zeta) \rho c_p Q(t)}$.

![height:7cm](figs/vnetilation_exchanger.png)

---

## Modeling a whole system

Before starting calculations, the interfaces with heat exchanges have to be identified:

![width:25cm](figs/interfaces.png)

---

Example: 

![bg fit right](figs/flat.png)

- determine the interfaces of thermal losses
- compute the transmission coefficient for each surface (neglecting surface phenomena)
- compute the Ubat in W/m2.K

---
Solution considering only interfaces with thermal losses
  - 2 walls without window: 3mx3m+4mx3m-2mx1m
thickness:0.1m[concrete]+.04m[glass]+0.01m[plaster]
$
R_{wall} = \frac{0.1}{1}+\frac{0.04}{0.05}+\frac{0.01}{0.35}=0.93K.m^2/W: U_{wall}=1.08W/m^2.K
$
- 1 window 1mx2m, thickness:0.004m[glass]+0.01m[air]+0.004m[glass]
$
R_{glazing} = 2\frac{0.004}{1}+0.15 =  0.158K.m^2/W: U_{glazing}=6.33W/m^2.K
$
$$
U_{b}=\frac{U_{wall}S_{wall}+U_{glazing}S_{glazing}}{S_{wall}+S_{glazing}} = 1W/m^2.K
$$
---

Because reality is too complex and too detailed, simplified modeling is needed.

![width:12cm](figs/complexity.png)

More for equity, the simplified modeling must be defined by the law.

---

### RT2012/RE2020 regulatory calculation method: the ThBAT (CSTB)

- [RT2012](doc/ThBAT/RT2012_law.pdf)

- [Th-U](doc/ThBAT/RT2012_Th-U.pdf): Transmission coefficients
  - booklet 1 (p1): Generalities
  - booklet 2 (p25): Materials
  - booklet 3 (p58): Glazed walls
  - booklet 4 (p136): Opaque walls
  - booklet 5 (p240): Thermal bridges

---

- [Th-L](doc/ThBAT/RT2012_Th-L.pdf): light transmission on walls
- [Th-S](doc/ThBAT/RT2012_Th-S.pdf): solar transmittance of walls
- [Th-I](doc/ThBAT/RT2012_Th-I.pdf): thermal inertia
- [Th-BCE](doc/ThBAT/RT2012_Th-BCE.pdf): conventional data or thresholds

- [RE2020 guide](doc/ThBAT/guide_RE2020.pdf): the guide to RE2020

<!---

### Advection

--->

---

## Calculating complex structures

Let's now consider a multi-component wall:

![width:20cm](figs/multicomponent.png)

---
1. Identify the non-adiabatic interfaces (and neglect the other ones)
2. Add the boundary air layers
3. Identify the nodes of temperatures (nodal model): each temperature is related to an unique node
4. Identify the same sequence of interface components by identifying the temperatures on both sides: same temperatures = same interface with different components
5. Identify components of the same interface by identifying heat flow passing through
6. Sum up the thermal resistances (R) of each interface component corresponding to a unique heat flow

---
7. Invert thermal resistance of interface components to get transmitivity coefficients $U$
8. Sum up the transmitivity coefficients of components to get a single value per heat flow (i.e. per interface)
9. Add thermal bridges
10. Add air renewal
11. Model your thermal systems with power sources injected at some nodes, place thermal resistance following the "recipient convention":
---

## Thermal and electricity analogy

![length=26cm](figs/electric_eq.png)
---

---
## Going further

RK Rajput. A Textbook of Heat and Mass Transfer [Concise Edition] (p. 443). S Chand & Co Ltd. Kindle Edition. 