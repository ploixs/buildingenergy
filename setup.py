from setuptools import find_packages, setup

setup(
    name='buildingenergy',
    description='A toolbox dedicated to energy management in buildings',
    long_description='A toolbox for teaching purpose',
    long_description_content_type='text/plain',
    author='Stephane Ploix',
    author_email='stephane.ploix@grenoble-inp.fr',
    version='1.0',
    url='https://gricad-gitlab.univ-grenoble-alpes.fr/ploixs/buildingenergy',
    install_requires=[
        'joblib', 'matplotlib', 'networkx', 'numpy', 'openpyxl', 'pandas', 'plotly', 'prettytable', 'PyExcelerate', 'pymor', 'pytz', 'Requests', 'SALib', 'scipy', 'SQLAlchemy', 'tomlkit', 'windrose',
        'importlib-metadata; python_version>"3.9"'],
    #package_data={'': ["setup.ini", "propertiesDB.xlsx"]},
    packages=find_packages(where='.', include=['buildingenergy', 'ecommunity'], exclude=['data', 'doc', 'img', 'results', 'sites', 'slides'])
)
# include_package_data=True,
#     package_data={'': ["setup.ini"]},